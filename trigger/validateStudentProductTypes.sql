DROP TRIGGER IF EXISTS triggerForValidationOfProducts;
DROP TRIGGER IF EXISTS triggerIsErrorInStudentRollNoRecords;

DROP TABLE IF EXISTS verifyStudentProducts;
DROP TABLE IF EXISTS g;

CREATE TABLE verifyStudentProducts (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(255) NOT NULL,
  Constraint pk_verifyStudentProducts Primary Key(rollNo,productTypeId),
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId) REFERENCES productTypes(productTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE g(
  a text,
  b text,
  c text
);

CREATE TRIGGER triggerIsErrorInStudentRollNoRecords after insert on studentRollNoRecords
when (select count(subProductTypeId) from productTypeHierarchy where rollNo = NEW.rollNo) < (select leafCount from leafCountForStudentAssignedProductType)
begin
  delete from rejectedStudents where rollNo = NEW.rollNo and msgId = 1;
  insert into rejectedStudents(rollNo,msgId,msg) values(NEW.rollNo,1,(select msg from ErrorMsg where msgId = 1));
  insert into g(a,b,c) values(NEW.rollNo,1,1);
end;

CREATE TRIGGER triggerForValidationOfProducts after insert on studentRollNoRecords
when (select count(subProductTypeId) from productTypeHierarchy where rollNo = NEW.rollNo) > (select leafCount-1 from leafCountForStudentAssignedProductType)
begin
  insert into verifyStudentProducts(rollNo,productTypeId) select distinct rollNo,productTypeId from productTypes where rollNo = NEW.rollNo;
end;
