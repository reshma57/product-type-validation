DROP TRIGGER IF EXISTS triggerForValidateFloatAndListValue;
DROP TRIGGER IF EXISTS triggerForValidateIntegerAndListValue;
DROP TRIGGER IF EXISTS triggerForValidateStringAndListValue;
DROP TRIGGER IF EXISTS triggerForIsValidateFloatAndListValue;
DROP TRIGGER IF EXISTS triggerForIsValidateIntegerAndListValue;
DROP TRIGGER IF EXISTS triggerForIsValidateStringAndListValue;


CREATE TRIGGER triggerForIsValidateFloatAndListValue after insert on checkForListAndFloat
when (select NEW.productTypePropertyDefaultValue in (select productTypePropertyTypeValidationValue from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId))) = 0
begin
    delete from rejectedStudents where rollNo = NEW.rollNo and msgId = 8;
    insert into rejectedStudents(rollNo,msgId,msg) values(NEW.rollNo,8,(select msg from ErrorMsg where msgId =8));
    insert into g(a,b,c) values(8,NEW.productTypeId,NEW.productTypePropertyTypeId);
end;

CREATE TRIGGER triggerForValidateFloatAndListValue after insert on checkForListAndFloat
when (select NEW.productTypePropertyDefaultValue in (select productTypePropertyTypeValidationValue from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId))) = 1
begin
  insert into validateProductTypePropertyTypeDefaultValue(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId,NEW.productTypePropertyDefaultValue);
end;


CREATE TRIGGER triggerForIsValidateIntegerAndListValue after insert on checkForListAndInteger
when (select NEW.productTypePropertyDefaultValue in (select productTypePropertyTypeValidationValue from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId))) = 0
begin
    delete from rejectedStudents where rollNo = NEW.rollNo and msgId = 8;
    insert into rejectedStudents(rollNo,msgId,msg) values(NEW.rollNo,8,(select msg from ErrorMsg where msgId =8));
    insert into g(a,b,c) values(8,NEW.productTypeId,NEW.productTypePropertyTypeId);
end;

CREATE TRIGGER triggerForValidateIntegerAndListValue after insert on checkForListAndInteger
when (select NEW.productTypePropertyDefaultValue in (select productTypePropertyTypeValidationValue from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId))) = 1
begin
  insert into validateProductTypePropertyTypeDefaultValue(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId,NEW.productTypePropertyDefaultValue);
end;

CREATE TRIGGER triggerForIsValidateStringAndListValue after insert on checkForListAndString
when (select NEW.productTypePropertyDefaultValue in (select productTypePropertyTypeValidationValue from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId))) = 0
begin
    delete from rejectedStudents where rollNo = NEW.rollNo and msgId = 8;
    insert into rejectedStudents(rollNo,msgId,msg) values(NEW.rollNo,8,(select msg from ErrorMsg where msgId =8));
    insert into g(a,b,c) values(8,NEW.productTypeId,NEW.productTypePropertyTypeId);
end;

CREATE TRIGGER triggerForValidateStringAndListValue after insert on checkForListAndString
when (select NEW.productTypePropertyDefaultValue in (select productTypePropertyTypeValidationValue from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId))) = 1
begin
  insert into validateProductTypePropertyTypeDefaultValue(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId,NEW.productTypePropertyDefaultValue);
end;
