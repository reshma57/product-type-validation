DROP TRIGGER IF EXISTS triggerForRejectedStudentRollNo;

CREATE TRIGGER triggerForRejectedStudentRollNo after insert on rejectedStudentRollNo
begin
delete from rejectedStudentRollNo where rollNo = NEW.rollNo;
delete from checkForContainerAndFloat where rollNo = NEW.rollNo;
delete from checkForContainerAndInteger where rollNo = NEW.rollNo;
delete from checkForContainerAndString where rollNo = NEW.rollNo;
delete from checkForListAndFloat where rollNo = NEW.rollNo;
delete from checkForListAndInteger where rollNo = NEW.rollNo;
delete from checkForListAndString where rollNo = NEW.rollNo;
delete from checkForRangeAndFloat where rollNo = NEW.rollNo;
delete from checkForRangeAndInteger where rollNo = NEW.rollNo;
delete from checkForRangeAndString where rollNo = NEW.rollNo;
delete from checkPoductTypePropertyTypeSequenceNumber where rollNo = NEW.rollNo;
delete from checkProductTypeForFloat where rollNo = NEW.rollNo;
delete from checkProductTypeForInteger where rollNo = NEW.rollNo;
delete from checkProductTypeForString where rollNo = NEW.rollNo;
delete from checkPropertyTypeAndPropertyValues where rollNo = NEW.rollNo;
delete from checkValueOfProductTypePropertyTypeValues where rollNo = NEW.rollNo;
delete from definedProductTypePropertyTypes where rollNo = NEW.rollNo;
delete from depthOfProductTypePropertyType where rollNo = NEW.rollNo;
delete from findDepthOfProductTypePropertyTypeContainer where rollNo = NEW.rollNo;
delete from generateSequenceNumberByProductTypeValidationId where rollNo = NEW.rollNo;
delete from leafOfTree where rollNo = NEW.rollNo;
delete from parentInProductTypes where rollNo = NEW.rollNo;
delete from parentInTree where rollNo = NEW.rollNo;
delete from productTypeHierarchy where rollNo = NEW.rollNo;
delete from productTypePropertyTypeContainer where rollNo = NEW.rollNo;
delete from productTypePropertyTypeValidationTypes where rollNo = NEW.rollNo;
delete from productTypePropertyTypeValidationValues where rollNo = NEW.rollNo;
delete from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo;
delete from productTypePropertyTypes where rollNo = NEW.rollNo;
delete from productTypeRoot where rollNo = NEW.rollNo;
delete from productTypeUrls where rollNo = NEW.rollNo;
delete from productTypes where rollNo = NEW.rollNo;
delete from rejectedStudents where rollNo = NEW.rollNo;
delete from requiredProductTypeProperties where rollNo = NEW.rollNo;
delete from siblingInTree where rollNo = NEW.rollNo;
delete from studentRollNoRecords where rollNo = NEW.rollNo;
delete from validateForFloatValues where rollNo = NEW.rollNo;
delete from validateForIntegerValues where rollNo = NEW.rollNo;
delete from validateForStringValues where rollNo = NEW.rollNo;
delete from validateProductTypePropertyTypeDefaultValue where rollNo = NEW.rollNo;
delete from validationOfDepthAndElem where rollNo = NEW.rollNo;
delete from validationOfHierarchyAndElem where rollNo = NEW.rollNo;
delete from validationOfPoductTypePropertyTypeSequenceNumber where rollNo = NEW.rollNo;
delete from verifyStudentProducts where rollNo = NEW.rollNo;
end;
