DROP TABLE IF EXISTS validationOfHierarchyAndElem;
DROP TABLE IF EXISTS findDepthOfProductTypePropertyTypeContainer;
DROP TABLE IF EXISTS countOfElemOfProductTypePropertyTypeOfContainer;
DROP TABLE IF EXISTS validationOfDepthAndElem;
DROP TABLE IF EXISTS checkValueOfProductTypePropertyTypeValues;

DROP TRIGGER IF EXISTS triggerValidationOfHierarchy;
DROP TRIGGER IF EXISTS triggerValidateCountOfElemInTree;
DROP TRIGGER IF EXISTS triggerValidateCountOfDefinedElemInTree;
DROP TRIGGER IF EXISTS triggerIsValidHierarchyForInsertion;
DROP TRIGGER IF EXISTS triggerIsValidateCountOfElemInTreeForInsert;


CREATE TABLE findDepthOfProductTypePropertyTypeContainer (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(255) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  /*Constraint pk_findDepthOfProductTypePropertyTypeContainer Primary Key(rollNo,productTypeId,productTypePropertyTypeId),*/
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId) REFERENCES productTypes(productTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypePropertyTypeId) REFERENCES requiredProductTypeProperties(productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE validationOfHierarchyAndElem (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(255) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  countOfElem integer NOT NULL,
  Constraint pk_validationOfHierarchyAndElem Primary Key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId) REFERENCES productTypes(productTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypePropertyTypeId) REFERENCES requiredProductTypeProperties(productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE countOfElemOfProductTypePropertyTypeOfContainer (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(255) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  countOfElem integer default 1,
  Constraint pk_countOfElemOfProductTypePropertyTypeOfContainer Primary Key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId) REFERENCES productTypes(productTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypePropertyTypeId) REFERENCES requiredProductTypeProperties(productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE validationOfDepthAndElem (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(255) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  countOfElem integer default 0,
  Constraint pk_validationOfDepthAndElem Primary Key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId) REFERENCES productTypes(productTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypePropertyTypeId) REFERENCES requiredProductTypeProperties(productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE checkValueOfProductTypePropertyTypeValues (
  checkId integer NOT NULL Primary key AUTOINCREMENT,
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(255) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId) REFERENCES productTypes(productTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypePropertyTypeId) REFERENCES requiredProductTypeProperties(productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TRIGGER triggerIsValidHierarchyForInsertion after insert on depthOfProductTypePropertyType
when NEW.countOfElem < (select height from depthOfTree)
  and
(select NEW.rollNo in (select rollNo from rejectedStudents)) = 0
begin
  insert into rejectedStudents(rollNo,msgId,msg) values(NEW.rollNo,5,(select msg from ErrorMsg where msgId = 5));
  insert into g(a,b,c) values(5,NEW.productTypeId,NEW.productTypePropertyTypeId);
end;


CREATE TRIGGER triggerIsValidHierarchy after update on depthOfProductTypePropertyType
when NEW.countOfElem < (select height from depthOfTree)
  and
(select NEW.rollNo in (select rollNo from rejectedStudents)) = 0
begin
  insert into rejectedStudents(rollNo,msgId,msg) values(NEW.rollNo,5,(select msg from ErrorMsg where msgId = 5));
  insert into g(a,b,c) values(5,NEW.productTypeId,NEW.productTypePropertyTypeId);
end;

CREATE TRIGGER triggerValidationOfHierarchy after update on depthOfProductTypePropertyType
when NEW.countOfElem > (select height-1 from depthOfTree)
and
(select NEW.rollNo in (select rollNo from findDepthOfProductTypePropertyTypeContainer where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId )) = 0
and
(select NEW.rollNo in (select rollNo from countOfElemOfProductTypePropertyTypeOfContainer where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId )) = 0
begin
  delete from rejectedStudents where rollNo = NEW.rollNo and msgId = 5;
  delete from g where a = 5;
  insert into countOfElemOfProductTypePropertyTypeOfContainer(rollNo,productTypeId,productTypePropertyTypeId) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId);
  insert into findDepthOfProductTypePropertyTypeContainer(rollNo,productTypeId,productTypePropertyTypeId) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId);
end;


CREATE TRIGGER triggerFindDepth  insert on findDepthOfProductTypePropertyTypeContainer
when (select NEW.rollNo in (select rollNo from findDepthOfProductTypePropertyTypeContainer where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId )) = 0
begin
    update countOfElemOfProductTypePropertyTypeOfContainer set countOfElem = countOfElem + (select count(childProductTypePropertyTypeId) from productTypePropertyTypeContainer where parentProductTypePropertyTypeId = NEW.productTypePropertyTypeId) where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = (select productTypePropertyTypeId from parentInProductTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId);
    insert into findDepthOfProductTypePropertyTypeContainer(rollNo,productTypeId,productTypePropertyTypeId) select distinct r.rollNo,r.productTypeId,p.childProductTypePropertyTypeId from productTypePropertyTypeContainer as p,requiredProductTypeProperties as r where parentProductTypePropertyTypeId = NEW.productTypePropertyTypeId and r.rollNo = NEW.rollNo and r.productTypeId = NEW.productTypeId;
end;

CREATE TRIGGER triggerIsValidateCountOfElemInTreeForInsert after insert on countOfElemOfProductTypePropertyTypeOfContainer
when NEW.countOfElem < (select propertyContainerLimit from countOfPropertyContainer)
  and
(select NEW.rollNo in (select rollNo from rejectedStudents where rollNo = NEW.rollNo)) = 0
begin
  insert into rejectedStudents(rollNo,msgId,msg) values(NEW.rollNo,5,(select msg from ErrorMsg where msgId = 5));
  insert into g(a,b,c) values(5,NEW.productTypeId,NEW.productTypePropertyTypeId);
end;


CREATE TRIGGER triggerIsValidateCountOfElemInTree after update on countOfElemOfProductTypePropertyTypeOfContainer
when NEW.countOfElem < (select propertyContainerLimit from countOfPropertyContainer)
  and
(select NEW.rollNo in (select rollNo from rejectedStudents where rollNo = NEW.rollNo)) = 0
begin
  insert into rejectedStudents(rollNo,msgId,msg) values(NEW.rollNo,5,(select msg from ErrorMsg where msgId = 5));
  insert into g(a,b,c) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId);
end;



CREATE TRIGGER triggerValidateCountOfElemInTree after update on countOfElemOfProductTypePropertyTypeOfContainer
when NEW.countOfElem > (select propertyContainerLimit-1 from countOfPropertyContainer)
and
(select NEW.productTypePropertyTypeId in (select productTypePropertyTypeId from validationOfHierarchyAndElem where productTypeId = NEW.productTypeId))  = 0
and
(select count(*) from productTypePropertyTypes where productTypePropertyTypeId = NEW.productTypePropertyTypeId and rollNo = NEW.rollNo) > 0
  begin
    delete from rejectedStudents where rollNo = NEW.rollNo and msgId = 5;
    delete from g where a = 5;
    insert into validationOfDepthAndElem(rollNo,productTypeId,productTypePropertyTypeId,countOfElem) values (NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId,NEW.countOfElem);
    insert into validationOfHierarchyAndElem(rollNo,productTypeId,productTypePropertyTypeId,countOfElem) values (NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId,NEW.countOfElem);
  end;

CREATE TRIGGER triggerForDeletionOfStudentRecords after insert on validationOfDepthAndElem
begin
  delete from rejectedStudents where rollNo = NEW.rollNo and msgId = 5;
end;

CREATE TRIGGER triggerValidateCountOfDefinedElemInTree after update on countOfElemOfProductTypePropertyTypeOfContainer
when NEW.countOfElem > (select propertyContainerLimit-1 from countOfPropertyContainer)
and
(select NEW.productTypePropertyTypeId in (select productTypePropertyTypeId from validationOfHierarchyAndElem where productTypeId = NEW.productTypeId))  = 0
and
(select distinct rollNo from productTypePropertyTypeContainer where  parentProductTypePropertyTypeId = NEW.productTypePropertyTypeId and rollNo not in (select rollNo from productTypePropertyTypes where productTypePropertyTypeId = NEW.productTypePropertyTypeId) and rollNo = NEW.rollNo) != NEW.rollNo
 begin
    delete from rejectedStudents where rollNo = NEW.rollNo and msgId = 4;
    insert into rejectedStudents(rollNo,msgId,msg) values(NEW.rollNo,4,(select msg from ErrorMsg where msgId = 4));
    insert into g(a,b,c) values(5,NEW.productTypeId,NEW.productTypePropertyTypeId);
 end;

/* trigger for validation Of value according to productTypeValidationTypeId */
CREATE TRIGGER triggerValidate after insert on validationOfHierarchyAndElem
begin
  insert into checkValueOfProductTypePropertyTypeValues(rollNo,productTypeId,productTypePropertyTypeId)  select rollNo,productTypeId,productTypePropertyTypeId from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId;
end;
