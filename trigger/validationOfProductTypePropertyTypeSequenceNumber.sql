DROP TRIGGER IF EXISTS triggerForproductTypePropertyTypeValidationValues;
DROP TRIGGER IF EXISTS triggerRemoveDuplicateproductTypePropertyTypeValidationTypeId;
DROP TRIGGER IF EXISTS triggerForGenrateNumber;
DROP TRIGGER IF EXISTS triggerForGenratingNumberByRecursion;
DROP TRIGGER IF EXISTS triggerForValidateSeqenceNumberAndCheck;

DROP TABLE IF EXISTS validationOfPoductTypePropertyTypeSequenceNumber;
DROP TABLE IF EXISTS checkPoductTypePropertyTypeSequenceNumber;
DROP TABLE IF EXISTS generateSequenceNumberByProductTypeValidationId;

CREATE TABLE validationOfPoductTypePropertyTypeSequenceNumber (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(25) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  productTypePropertyTypeValidationTypeId varchar(255) NOT NULL,
  countOfProductTypePropertyType integer NOT NULL,
  Constraint pk_validationOfPoductTypePropertyTypeSequenceNumber primary key(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyTypeValidationTypeId),
  Foreign Key(rollNo) References students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId,productTypePropertyTypeId) References requiredProductTypeProperties(productTypeId,productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypePropertyTypeValidationTypeId) References productTypePropertyTypeValidationTypes(productTypePropertyTypeValidationTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);


CREATE TABLE checkPoductTypePropertyTypeSequenceNumber (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(25) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  productTypePropertyTypeValidationTypeId varchar(255) NOT NULL,
  countOfProductTypePropertyType integer NOT NULL,
  Constraint pk_checkPoductTypePropertyTypeSequenceNumber primary key(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyTypeValidationTypeId),
  Foreign Key(rollNo) References students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId,productTypePropertyTypeId) References requiredProductTypeProperties(productTypeId,productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypePropertyTypeValidationTypeId) References productTypePropertyTypeValidationTypes(productTypePropertyTypeValidationTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE generateSequenceNumberByProductTypeValidationId (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(25) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  productTypePropertyTypeValidationTypeId varchar(255) NOT NULL,
  limitOfproductTypePropertyType integer NOT NULL,
  sequenceNumber integer NOT NULL,
  Constraint pk_generateSequenceNumberByProductTypeValidationId primary key(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyTypeValidationTypeId,sequenceNumber),
  Foreign Key(rollNo) References students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId,productTypePropertyTypeId) References requiredProductTypeProperties(productTypeId,productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypePropertyTypeValidationTypeId) References productTypePropertyTypeValidationTypes(productTypePropertyTypeValidationTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TRIGGER triggerForproductTypePropertyTypeValidationValues after insert on validateProductTypePropertyTypeDefaultValue
begin
  insert into checkPoductTypePropertyTypeSequenceNumber(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyTypeValidationTypeId,countOfProductTypePropertyType) select distinct p.rollNo,p.productTypeId,p.productTypePropertyTypeId,p.productTypePropertyTypeValidationId,count(sequenceNumber) from productTypePropertyTypeValueTypes as p,productTypePropertyTypeValidationValues as r where p.rollNo = NEW.rollNo and p.productTypeId = NEW.productTypeId and p.productTypePropertyTypeId = NEW.productTypePropertyTypeId and r.productTypePropertyTypeValidationTypeId = p.productTypePropertyTypeValidationId and r.rollNo = NEW.rollNo;
end;



CREATE TRIGGER triggerRemoveDuplicateproductTypePropertyTypeValidationTypeId after insert on checkPoductTypePropertyTypeSequenceNumber
when (select NEW.productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationTypeId from validationOfPoductTypePropertyTypeSequenceNumber where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId)) = 0
begin
  insert into validationOfPoductTypePropertyTypeSequenceNumber(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyTypeValidationTypeId,countOfProductTypePropertyType) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId,NEW.productTypePropertyTypeValidationTypeId,NEW.countOfProductTypePropertyType);
end;


CREATE TRIGGER triggerForGenrateNumber after insert on validationOfPoductTypePropertyTypeSequenceNumber
begin
   insert into generateSequenceNumberByProductTypeValidationId(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyTypeValidationTypeId,limitOfproductTypePropertyType,sequenceNumber) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId,NEW.productTypePropertyTypeValidationTypeId,NEW.countOfProductTypePropertyType,1);
end;


CREATE TRIGGER triggerForGenratingNumberByRecursion after insert on generateSequenceNumberByProductTypeValidationId
when NEW.sequenceNumber < NEW.limitOfproductTypePropertyType
begin
   insert into generateSequenceNumberByProductTypeValidationId(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyTypeValidationTypeId,limitOfproductTypePropertyType,sequenceNumber) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId,NEW.productTypePropertyTypeValidationTypeId,NEW.limitOfproductTypePropertyType,NEW.sequenceNumber + 1);
end;

CREATE TRIGGER triggerForValidateSeqenceNumberAndCheck after insert on generateSequenceNumberByProductTypeValidationId
when (select NEW.sequenceNumber in (select sequenceNumber from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where productTypePropertyTypeValidationId = NEW.productTypePropertyTypeValidationTypeId and rollNo = NEW.rollNo and productTypeId = NEW.productTypeId))) = 0
begin
   delete from rejectedStudents where rollNo = NEW.rollNo and msgId = 9;
   insert into rejectedStudents(rollNo,msgId,msg) values(NEW.rollNo,9,(select msg from ErrorMsg where msgId =9));
   insert into g(a,b,c) values(NEW.productTypePropertyTypeValidationTypeId,NEW.productTypeId,NEW.sequenceNumber);
end;


