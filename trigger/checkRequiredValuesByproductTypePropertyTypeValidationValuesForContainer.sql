DROP TRIGGER IF EXISTS triggerForCheckContainerAndFloat;
DROP TRIGGER IF EXISTS triggerForCheckContainerAndInteger;
DROP TRIGGER IF EXISTS triggerForCheckContainerAndString;

DROP TABLE IF EXISTS checkForContainerAndFloat;
DROP TABLE IF EXISTS checkForContainerAndInteger;
DROP TABLE IF EXISTS checkForContainerAndString;

CREATE TABLE checkForContainerAndFloat (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(25) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  productTypePropertyDefaultValue text NOT NULL,
  Constraint pk_checkForContainerAndFloat primary key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) References students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId,productTypePropertyTypeId) References requiredProductTypeProperties(productTypeId,productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE checkForContainerAndInteger (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(25) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  productTypePropertyDefaultValue text NOT NULL,
  Constraint pk_checkForContainerAndInteger primary key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) References students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId,productTypePropertyTypeId) References requiredProductTypeProperties(productTypeId,productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);


CREATE TABLE checkForContainerAndString (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(25) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  productTypePropertyDefaultValue text NOT NULL,
  Constraint pk_checkForContainerAndString primary key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) References students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId,productTypePropertyTypeId) References requiredProductTypeProperties(productTypeId,productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TRIGGER triggerForCheckContainerAndFloat after insert on validateForFloatValues
when (select count(*) from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId  from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId and productTypePropertyTypeValidationId in (select productTypePropertyTypeValidationTypeId from productTypePropertyTypeValidationTypes where abstractProductTypePropertyValueTypeTypeId in (select abstractProductTypePropertyValueTypeTypeId from abstractProductTypePropertyValueTypeTypes order by abstractProductTypePropertyValueTypeTypeId asc limit 1)))) > 0
begin
 insert into checkForContainerAndFloat(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId,NEW.productTypePropertyDefaultValue);
end;


CREATE TRIGGER triggerForCheckContainerAndInteger after insert on validateForIntegerValues
when (select count(*) from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId  from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId and productTypePropertyTypeValidationId in (select productTypePropertyTypeValidationTypeId from productTypePropertyTypeValidationTypes where abstractProductTypePropertyValueTypeTypeId in (select abstractProductTypePropertyValueTypeTypeId from abstractProductTypePropertyValueTypeTypes order by abstractProductTypePropertyValueTypeTypeId asc limit 1)))) > 0
begin
 insert into checkForContainerAndInteger(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId,NEW.productTypePropertyDefaultValue);
end;

CREATE TRIGGER triggerForCheckContainerAndString after insert on validateForStringValues
when (select count(*) from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId  from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId and productTypePropertyTypeValidationId in (select productTypePropertyTypeValidationTypeId from productTypePropertyTypeValidationTypes where abstractProductTypePropertyValueTypeTypeId in (select abstractProductTypePropertyValueTypeTypeId from abstractProductTypePropertyValueTypeTypes order by abstractProductTypePropertyValueTypeTypeId asc limit 1)))) > 0
begin
 insert into checkForContainerAndString(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId,NEW.productTypePropertyDefaultValue);
end;


/*select * from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where productTypePropertyTypeValidationId in (select productTypePropertyTypeValidationTypeId from productTypePropertyTypeValidationTypes where abstractProductTypePropertyValueTypeTypeId in (select abstractProductTypePropertyValueTypeTypeId from abstractProductTypePropertyValueTypeTypes order by abstractProductTypePropertyValueTypeTypeId limit 1)) and rollNo = "00002" and productTypeId = "499927" and productTypePropertyTypeId = "instructions")*/
