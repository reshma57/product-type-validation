DROP TRIGGER IF EXISTS triggerForValidateFloatAndRangeValue;
DROP TRIGGER IF EXISTS triggerForValidateIntegerAndRangeValue;
DROP TRIGGER IF EXISTS triggerForValidateStringAndRangeValue;

CREATE TRIGGER triggerForIsValidateFloatAndRangeValue after insert on checkForRangeAndFloat
when (select (select productTypePropertyTypeValidationValue from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId) order by sequenceNumber asc limit 1)  < NEW.productTypePropertyDefaultValue - 0.1 < (select productTypePropertyTypeValidationValue from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId) order by sequenceNumber desc limit 1)) = 0
begin
    delete from rejectedStudents where rollNo = NEW.rollNo and msgId = 8;
    insert into rejectedStudents(rollNo,msgId,msg) values(NEW.rollNo,8,(select msg from ErrorMsg where msgId =8));
    insert into g(a,b,c) values("8float",NEW.productTypeId,NEW.productTypePropertyTypeId);
end;

CREATE TRIGGER triggerForValidateFloatAndRangeValue after insert on checkForRangeAndFloat
when (select (select productTypePropertyTypeValidationValue from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId) order by sequenceNumber asc limit 1)  < NEW.productTypePropertyDefaultValue - 0.1 < (select productTypePropertyTypeValidationValue from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId) order by sequenceNumber desc limit 1)) = 1
begin
  insert into validateProductTypePropertyTypeDefaultValue(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId,NEW.productTypePropertyDefaultValue);
end;


CREATE TRIGGER triggerForIsValidateIntegerAndRangeValue after insert on checkForRangeAndInteger
when (select (select productTypePropertyTypeValidationValue from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId) order by sequenceNumber asc limit 1)  < NEW.productTypePropertyDefaultValue - 1 < (select productTypePropertyTypeValidationValue from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId) order by sequenceNumber desc limit 1)) = 0
begin
    delete from rejectedStudents where rollNo = NEW.rollNo and msgId = 8;
    insert into rejectedStudents(rollNo,msgId,msg) values(NEW.rollNo,8,(select msg from ErrorMsg where msgId =8));
    insert into g(a,b,c) values("8int",NEW.productTypeId,NEW.productTypePropertyTypeId);
end;

CREATE TRIGGER triggerForValidateIntegerAndRangeValue after insert on checkForRangeAndInteger
when (select (select productTypePropertyTypeValidationValue from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId) order by sequenceNumber asc limit 1)  < NEW.productTypePropertyDefaultValue - 1 < (select productTypePropertyTypeValidationValue from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId) order by sequenceNumber desc limit 1)) = 1
begin
  insert into validateProductTypePropertyTypeDefaultValue(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId,NEW.productTypePropertyDefaultValue);
end;

