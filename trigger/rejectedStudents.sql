DROP TABLE IF EXISTS studentRollNoRecords;
DROP TABLE IF EXISTS rejectedStudents;
DROP TABLE IF EXISTS aux_rejectedStudents;
DROP TABLE IF EXISTS rejectedStudentRollNo;
DROP TABLE IF EXISTS ErrorMsg;

CREATE TABLE studentRollNoRecords (
  rollNo varchar(5) NOT NULL,
  Foreign Key(rollNo) References students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE rejectedStudentRollNo (
  rollNo varchar(5) NOT NULL,
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE
);


CREATE TABLE ErrorMsg (
  msgId integer NOT NULL,
  tableId text NOT NULL,
  msg text NOT NULL,
  Constraint pk_ErrorMsg Primary Key(msgId)
);

CREATE TABLE rejectedStudents (
  rollNo varchar(5) NOT NULL,
  msgId integer NOT NULL,
  msg text NOT NULL,
  Constraint pk_rejectedStudents Primary Key(rollNo,msgId),
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(msgId) REFERENCES ErrorMsg(msgId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE aux_rejectedStudents (
  rollNo varchar(5) NOT NULL,
  msgId integer NOT NULL,
  msg text NOT NULL,
  Constraint pk_aux_rejectedStudents Primary Key(rollNo,msgId),
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(msgId) REFERENCES ErrorMsg(msgId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);


insert into ErrorMsg(msgId,tableId,msg) values(1,"productTypeHierarchy","ERROR:productTypeHierarchy Require at least two subProductTypeId");
insert into ErrorMsg(msgId,tableId,msg) values(2,"productTypePropertyTypeContainer","ERROR:productTypePropertyTypeContainer Require Container frommandatory  productTypePropertyTypes");
insert into ErrorMsg(msgId,tableId,msg) values(3,"productTypePropertyTypeContainer","ERROR:productTypePropertyTypeContainer Require Container from productTypePropertyTypes or productTypePropertyTypeId  count should be at least 5");
insert into ErrorMsg(msgId,tableId,msg) values(4,"productTypePropertyTypeContainer","ERROR:productTypePropertyTypeContainer Require at least 1 mandatory productTypePropertyType");
insert into ErrorMsg(msgId,tableId,msg) values(5,"productTypePropertyTypeContainer","ERROR:productTypePropertyTypeContainer Require at least 1 productTypePropertyType should be of type of container with depth at least 3 and total entries under the container are at least 10");
insert into ErrorMsg(msgId,tableId,msg) values(6,"requiredProductTypeProperties","ERROR:datatype mismatch in requiredProductTypeProperties here float is Required");
insert into ErrorMsg(msgId,tableId,msg) values(7,"requiredProductTypeProperties","ERROR:datatype mismatch in requiredProductTypeProperties here integer is Required");
insert into ErrorMsg(msgId,tableId,msg) values(8,"requiredProductTypeProperties","ERROR:productTypePropertyDefaultValue should satisfy the rule of productTypePropertyTypeValidation Rule and check productTypePropertyDefaultValue from requiredProductTypeProperties and productTypePropertyTypeValidationValues");
insert into ErrorMsg(msgId,tableId,msg) values(9,"productTypePropertyTypeValidationValues","ERROR-check your sequenceNumber in productTypePropertyTypeValidationValues");
