DROP TRIGGER IF EXISTS triggerForCheckRangeAndFloat;
DROP TRIGGER IF EXISTS triggerForCheckRangeAndInteger;
DROP TRIGGER IF EXISTS triggerForCheckRangeAndstring;

DROP TABLE IF EXISTS checkForRangeAndFloat;
DROP TABLE IF EXISTS checkForRangeAndInteger;
DROP TABLE IF EXISTS checkForRangeAndString;
DROP TABLE IF EXISTS validateProductTypePropertyTypeDefaultValue;

CREATE TABLE checkForRangeAndFloat (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(25) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  productTypePropertyDefaultValue text NOT NULL,
  Constraint pk_checkForRangeAndFloat primary key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) References students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId,productTypePropertyTypeId) References requiredProductTypeProperties(productTypeId,productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE checkForRangeAndInteger (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(25) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  productTypePropertyDefaultValue text NOT NULL,
  Constraint pk_checkForRangeAndInteger primary key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) References students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId,productTypePropertyTypeId) References requiredProductTypeProperties(productTypeId,productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE checkForRangeAndString (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(25) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  productTypePropertyDefaultValue text NOT NULL,
  Constraint pk_checkForRangeAndString primary key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) References students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId,productTypePropertyTypeId) References requiredProductTypeProperties(productTypeId,productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);


CREATE TABLE validateProductTypePropertyTypeDefaultValue (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(25) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  productTypePropertyDefaultValue text NOT NULL,
  Constraint pk_validateProductTypePropertyTypeDefaultValue primary key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) References students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId,productTypePropertyTypeId) References requiredProductTypeProperties(productTypeId,productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);


/*this trigger For validating range and float values according to rollNo,productTypeId,productTypePropertyTypeId */

CREATE TRIGGER triggerForCheckRangeAndFloat after insert on validateForFloatValues
when (select count(*) from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId and productTypePropertyTypeValidationId in (select productTypePropertyTypeValidationTypeId from productTypePropertyTypeValidationTypes where rollNo = NEW.rollNo and abstractProductTypePropertyValueTypeTypeId in (select abstractProductTypePropertyValueTypeTypeId from abstractProductTypePropertyValueTypeTypes order by abstractProductTypePropertyValueTypeTypeId desc limit 1)))) = 2
begin
  insert into checkForRangeAndFloat(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId,NEW.productTypePropertyDefaultValue);
end;


/*this trigger For validating range and integer values according to rollNo,productTypeId,productTypePropertyTypeId */
CREATE TRIGGER triggerForCheckRangeAndInteger after insert on validateForIntegerValues
when (select count(*) from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId and productTypePropertyTypeValidationId in (select productTypePropertyTypeValidationTypeId from productTypePropertyTypeValidationTypes where rollNo = NEW.rollNo and abstractProductTypePropertyValueTypeTypeId in (select abstractProductTypePropertyValueTypeTypeId from abstractProductTypePropertyValueTypeTypes order by abstractProductTypePropertyValueTypeTypeId desc limit 1)))) = 2
begin
  insert into checkForRangeAndInteger(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId,NEW.productTypePropertyDefaultValue);
end;


CREATE TRIGGER triggerForCheckRangeAndString after insert on validateForstringValues
when (select count(*) from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId and productTypePropertyTypeValidationId in (select productTypePropertyTypeValidationTypeId from productTypePropertyTypeValidationTypes where rollNo = NEW.rollNo and abstractProductTypePropertyValueTypeTypeId in (select abstractProductTypePropertyValueTypeTypeId from abstractProductTypePropertyValueTypeTypes order by abstractProductTypePropertyValueTypeTypeId desc limit 1)))) = 2
begin
  insert into checkForRangeAndstring(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId,NEW.productTypePropertyDefaultValue);
end;
