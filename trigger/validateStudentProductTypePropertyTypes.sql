DROP TRIGGER IF EXISTS triggerForCountPropertyType;
DROP TRIGGER IF EXISTS triggerForCheckCountOfPropertyType;
DROP TRIGGER IF EXISTS triggerForCheckCountOfDefinedPropertType;
DROP TRIGGER IF EXISTS triggerForFindProductTypePropertyTypeContainer;
DROP TRIGGER IF EXISTS triggerForCountDefinedPropertyType;
DROP TRIGGER IF EXISTS triggerForSiblingInTree;
DROP TRIGGER IF EXISTS triggerForMaintainParent;
DROP TRIGGER IF EXISTS triggerForTemp;

DROP TABLE IF EXISTS definedProductTypePropertyTypes;
DROP TABLE IF EXISTS depthOfProductTypePropertyType;
DROP TABLE IF EXISTS parentInTree;
DROP TABLE IF EXISTS productTypeRoot;
DROP TABLE IF EXISTS parentInProductTypes;
DROP TABLE IF EXISTS leafOfTree;
DROP TABLE IF EXISTS siblingInTree;


CREATE TABLE definedProductTypePropertyTypes  (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(255) NOT NULL,
  Constraint pk_definedProductTypePropertyTypes Primary Key(rollNo,productTypeId),
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId) REFERENCES productTypes(productTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);



CREATE TABLE depthOfProductTypePropertyType  (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(255) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  countOfElem integer default 1,
  Constraint pk_depthOfProductTypePropertyType Primary Key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId) REFERENCES productTypes(productTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypePropertyTypeId) REFERENCES requiredProductTypeProperties(productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE parentInTree (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(255) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  /*Constraint pk_parentInTree Primary Key(rollNo,productTypeId,productTypePropertyTypeId),*/
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId) REFERENCES productTypes(productTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypePropertyTypeId) REFERENCES requiredProductTypeProperties(productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);


CREATE TABLE siblingInTree (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(255) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  Constraint pk_siblingInTree Primary Key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId) REFERENCES productTypes(productTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypePropertyTypeId) REFERENCES requiredProductTypeProperties(productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE productTypeRoot (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(255) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  Constraint pk_productTypeRoot Primary Key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId) REFERENCES productTypes(productTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypePropertyTypeId) REFERENCES requiredProductTypeProperties(productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE leafOfTree (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(255) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  Constraint pk_leafOfTree Primary Key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId) REFERENCES productTypes(productTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypePropertyTypeId) REFERENCES requiredProductTypeProperties(productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE parentInProductTypes (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(255) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  Constraint pk_parentInProductTypes Primary Key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId) REFERENCES productTypes(productTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypePropertyTypeId) REFERENCES requiredProductTypeProperties(productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

/* trigger for check the given properTypeId satisfy the property of defined propertyType Container or not*/

CREATE TRIGGER triggerForCheckCountOfDefinedPropertType after insert on verifyStudentProducts
when (select count(*) from productTypePropertyTypeValueTypes where productTypeId in (select superProductTypeId from productTypeHierarchy where superProductTypeId = NEW.productTypeId or subProductTypeId = NEW.productTypeId) and productTypePropertyTypeId not in (select productTypePropertyTypeId from productTypePropertyTypes where rollNo = NEW.rollNo) and productTypePropertyTypeValidationId in (select productTypePropertyTypeValidationTypeId from productTypePropertyTypeValidationTypes where abstractProductTypePropertyValueTypeTypeId = (select abstractProductTypePropertyValueTypeTypeId from abstractProductTypePropertyValueTypeTypes order by abstractProductTypePropertyValueTypeTypeId asc limit 1))) = (select propertyTypeCnt - 1 from countOfContainer)
begin
   delete from rejectedStudents where rollNo = NEW.rollNo and msgId = 2;
   insert into rejectedStudents(rollNo,msgId,msg) values(NEW.rollNo,2,(select msg from ErrorMsg where msgId = 2));
   insert into g(a,b,c) values(NEW.rollNo,NEW.productTypeId,2);
end;

CREATE TRIGGER triggerForCountOfDefinedPropertType after insert on verifyStudentProducts
when (select count(*) from productTypePropertyTypeValidationTypes where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where productTypePropertyTypeId not in (select productTypePropertyTypeId from productTypePropertyTypes where rollNo = NEW.rollNo) and productTypeId = (select superProductTypeId from productTypeHierarchy where subProductTypeId =NEW.productTypeId or superProductTypeId = NEW.productTypeId)) and abstractProductTypePropertyValueTypeTypeId = (select abstractProductTypePropertyValueTypeTypeId from abstractProductTypePropertyValueTypeTypes order by abstractProductTypePropertyValueTypeTypeId asc limit 1)) > (select propertyTypeCnt - 1 from countOfContainer)
  begin
     insert into definedProductTypePropertyTypes(rollNo,productTypeId) values(NEW.rollNo,NEW.productTypeId);
  end;


CREATE TRIGGER triggerForCheckCountOfPropertyType after insert on verifyStudentProducts
when (select count(*) from productTypePropertyTypes p,requiredProductTypeProperties as r where p.productTypePropertyTypeId = r.productTypePropertyTypeId and p.rollNo = NEW.rollNo and r.productTypeId = NEW.productTypeId) < (select propertyCnt-1 from propertyCount)
  or
  (select count(distinct productTypePropertyTypeId) from (select distinct productTypePropertyTypeId from requiredProductTypeProperties as r,productTypePropertyTypeContainer as p where p.parentProductTypePropertyTypeId = r.productTypePropertyTypeId and r.rollNo = NEW.rollNo and  r.productTypeId = NEW.productTypeId) as p,productTypePropertyTypeContainer as pc where p.productTypePropertyTypeId not in (select childProductTypePropertyTypeId from productTypePropertyTypeContainer where rollNo = NEW.rollNo)) = (select propertyTypeCnt - 1 from countOfContainer)
begin
    delete from rejectedStudents where rollNo = NEW.rollNo and msgId = 3;
    insert into rejectedStudents(rollNo,msgId,msg) values(NEW.rollNo,3,(select msg from ErrorMsg where msgId = 3));
    insert into g(a,b,c) values(NEW.rollNo,NEW.productTypeId,2);
end;

/* this trigger for check the count container for each produTypeId and also check propertyType for each productTypeId
-this trigger is insert parent of productTypePropertyTypeOfContainer in findDepthOfProductTypePropertyTypeContainer 
 */

CREATE TRIGGER triggerForCountPropertyType after insert on verifyStudentProducts
when (select count(*) from productTypePropertyTypes p,requiredProductTypeProperties as r where p.productTypePropertyTypeId = r.productTypePropertyTypeId and p.rollNo = NEW.rollNo and r.productTypeId = NEW.productTypeId) > (select propertyCnt-1 from propertyCount)
  and
  (select count(distinct productTypePropertyTypeId) from (select distinct productTypePropertyTypeId from requiredProductTypeProperties as r,productTypePropertyTypeContainer as p where p.parentProductTypePropertyTypeId = r.productTypePropertyTypeId and r.rollNo = NEW.rollNo and  r.productTypeId = NEW.productTypeId) as p,productTypePropertyTypeContainer as pc where p.productTypePropertyTypeId not in (select childProductTypePropertyTypeId from productTypePropertyTypeContainer where rollNo = NEW.rollNo)) > (select propertyTypeCnt - 1 from countOfContainer)
begin
     delete from rejectedStudents where rollNo = NEW.rollNo and msgId = 3;
     insert into depthOfProductTypePropertyType(rollNo,productTypeId,productTypePropertyTypeId) select distinct r.rollNo,r.productTypeId,r.productTypePropertyTypeId from requiredProductTypeProperties as r,productTypePropertyTypeContainer as p where r.productTypePropertyTypeId = p.parentProductTypePropertyTypeId and p.parentProductTypePropertyTypeId not in (select childProductTypePropertyTypeId from productTypePropertyTypeContainer where rollNo = NEW.rollNo) and r.productTypeId = NEW.productTypeId and p.parentProductTypePropertyTypeId in (select productTypePropertyTypeId from productTypePropertyTypes);
     insert into parentInTree(rollNo,productTypeId,productTypePropertyTypeId) select distinct r.rollNo,r.productTypeId,r.productTypePropertyTypeId from requiredProductTypeProperties as r,productTypePropertyTypeContainer as p where r.productTypePropertyTypeId = p.parentProductTypePropertyTypeId and p.parentProductTypePropertyTypeId not in (select childProductTypePropertyTypeId from productTypePropertyTypeContainer where rollNo = NEW.rollNo) and r.productTypeId = NEW.productTypeId and p.parentProductTypePropertyTypeId in (select productTypePropertyTypeId from productTypePropertyTypes);
end;


CREATE TRIGGER triggerForFindingHeight after insert on parentInTree
when (select count(*) from productTypePropertyTypeContainer as p,requiredProductTypeProperties as r where r.productTypeId = NEW.productTypeId and r.rollNo = p.rollNo and r.productTypePropertyTypeId = NEW.productTypePropertyTypeId and parentProductTypePropertyTypeId = NEW.productTypePropertyTypeId and p.rollNo = NEW.rollNo) > 0
begin
    insert into productTypeRoot(rollNo,productTypeId,productTypePropertyTypeId) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId);
    insert into leafOfTree(rollNo,productTypeId,productTypePropertyTypeId) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId);
    insert into parentInTree(rollNo,productTypeId,productTypePropertyTypeId) select distinct r.rollNo,r.productTypeId,p.childProductTypePropertyTypeId from productTypePropertyTypeContainer as p,requiredProductTypeProperties as r where parentProductTypePropertyTypeId = NEW.productTypePropertyTypeId and r.rollNo = NEW.rollNo and r.productTypeId = NEW.productTypeId;
end;

CREATE TRIGGER triggerForMaintainParent after insert on productTypeRoot
when (select NEW.productTypePropertyTypeId in (select productTypePropertyTypeId from depthOfProductTypePropertyType where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId)) = 1
begin
  delete from parentInProductTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId;
  insert into parentInProductTypes(rollNo,productTypeId,productTypePropertyTypeId) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId);
end;

CREATE TRIGGER triggerForSiblingInTree after insert on leafOfTree
when (select (select parentProductTypePropertyTypeId from productTypePropertyTypeContainer where childProductTypePropertyTypeId = NEW.productTypePropertyTypeId or parentProductTypePropertyTypeId = NEW.productTypePropertyTypeId and rollNo = NEW.rollNo) in (select productTypePropertyTypeId from siblingInTree where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId )) = 0
begin
  update depthOfProductTypePropertyType set countOfElem = countOfElem + 1 where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = (select productTypePropertyTypeId from parentInProductTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId);
  insert into siblingInTree(rollNo,productTypeId,productTypePropertyTypeId) select distinct r.rollNo,r.productTypeId,p.parentProductTypePropertyTypeId from requiredProductTypeProperties as r,productTypePropertyTypeContainer as p where r.productTypePropertyTypeId = NEW.productTypePropertyTypeId  and r.rollNo = NEW.rollNo and r.productTypeId = NEW.productTypeId and p.childProductTypePropertyTypeId = NEW.productTypePropertyTypeId;
end;








