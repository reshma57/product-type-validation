DROP TRIGGER IF EXISTS triggerForValidateFloatAndContainerValue;
DROP TRIGGER IF EXISTS triggerForValidateIntegerAndContainerValue;
DROP TRIGGER IF EXISTS triggerForValidateStringAndContainerValue;
DROP TRIGGER IF EXISTS triggerForIsValidateFloatAndContainerValue;
DROP TRIGGER IF EXISTS triggerForIsValidateIntegerAndContainerValue;
DROP TRIGGER IF EXISTS triggerForIsValidateStringAndContainerValue;


CREATE TRIGGER triggerForIsValidateFloatAndContainerValue after insert on checkForContainerAndFloat
when (select NEW.productTypePropertyDefaultValue in (select productTypePropertyTypeValidationValue from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId))) = 0
begin
    delete from rejectedStudents where rollNo = NEW.rollNo and msgId = 8;
    insert into rejectedStudents(rollNo,msgId,msg) values(NEW.rollNo,8,(select msg from ErrorMsg where msgId =8));
    insert into g(a,b,c) values("float",NEW.productTypeId,NEW.productTypePropertyTypeId);
end;

CREATE TRIGGER triggerForValidateFloatAndContainerValue after insert on checkForContainerAndFloat
when (select NEW.productTypePropertyDefaultValue in (select productTypePropertyTypeValidationValue from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId))) = 1
begin
  insert into validateProductTypePropertyTypeDefaultValue(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId,NEW.productTypePropertyDefaultValue);
end;


CREATE TRIGGER triggerForIsValidateIntegerAndContainerValue after insert on checkForContainerAndInteger
when (select NEW.productTypePropertyDefaultValue in (select productTypePropertyTypeValidationValue from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId))) = 0
begin
    delete from rejectedStudents where rollNo = NEW.rollNo and msgId = 8;
    insert into rejectedStudents(rollNo,msgId,msg) values(NEW.rollNo,8,(select msg from ErrorMsg where msgId =8));
    insert into g(a,b,c) values("int",NEW.productTypeId,NEW.productTypePropertyTypeId);
end;

CREATE TRIGGER triggerForValidateIntegerAndContainerValue after insert on checkForContainerAndInteger
when (select NEW.productTypePropertyDefaultValue in (select productTypePropertyTypeValidationValue from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId))) = 1
begin
  insert into validateProductTypePropertyTypeDefaultValue(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId,NEW.productTypePropertyDefaultValue);
end;

CREATE TRIGGER triggerForIsValidateStringAndContainerValue after insert on checkForContainerAndString
when (select NEW.productTypePropertyDefaultValue in (select productTypePropertyTypeValidationValue from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId))) = 0
begin
    delete from rejectedStudents where rollNo = NEW.rollNo and msgId = 8;
    insert into rejectedStudents(rollNo,msgId,msg) values(NEW.rollNo,8,(select msg from ErrorMsg where msgId =8));
    insert into g(a,b,c) values("string",NEW.productTypeId,NEW.productTypePropertyTypeId);
end;

CREATE TRIGGER triggerForValidateStringAndContainerValue after insert on checkForContainerAndString
when (select NEW.productTypePropertyDefaultValue in (select productTypePropertyTypeValidationValue from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId))) = 1
begin
  insert into validateProductTypePropertyTypeDefaultValue(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId,NEW.productTypePropertyDefaultValue);
end;
