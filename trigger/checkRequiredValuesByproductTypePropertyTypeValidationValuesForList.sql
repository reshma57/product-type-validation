DROP TRIGGER IF EXISTS triggerForCheckListAndFloat;
DROP TRIGGER IF EXISTS triggerForCheckListAndInteger;
DROP TRIGGER IF EXISTS triggerForCheckListAndString;

DROP TABLE IF EXISTS checkForListAndFloat;
DROP TABLE IF EXISTS checkForListAndInteger;
DROP TABLE IF EXISTS checkForListAndString;

CREATE TABLE checkForListAndFloat (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(25) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  productTypePropertyDefaultValue text NOT NULL,
  Constraint pk_checkForListAndFloat primary key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) References students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId,productTypePropertyTypeId) References requiredProductTypeProperties(productTypeId,productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE checkForListAndInteger (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(25) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  productTypePropertyDefaultValue text NOT NULL,
  Constraint pk_checkForListAndInteger primary key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) References students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId,productTypePropertyTypeId) References requiredProductTypeProperties(productTypeId,productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);


CREATE TABLE checkForListAndString (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(25) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  productTypePropertyDefaultValue text NOT NULL,
  Constraint pk_checkForListAndString primary key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) References students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId,productTypePropertyTypeId) References requiredProductTypeProperties(productTypeId,productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TRIGGER triggerForCheckListAndFloat after insert on validateForFloatValues
when (select count(*) from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId  from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId and productTypePropertyTypeValidationId in (select productTypePropertyTypeValidationTypeId from productTypePropertyTypeValidationTypes where abstractProductTypePropertyValueTypeTypeId in (select abstractProductTypePropertyValueTypeTypeId from abstractProductTypePropertyValueTypeTypes order by abstractProductTypePropertyValueTypeTypeId asc limit 1,1)))) > 0
begin
 insert into checkForListAndFloat(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId,NEW.productTypePropertyDefaultValue);
end;


CREATE TRIGGER triggerForCheckListAndInteger after insert on validateForIntegerValues
when (select count(*) from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId  from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId and productTypePropertyTypeValidationId in (select productTypePropertyTypeValidationTypeId from productTypePropertyTypeValidationTypes where abstractProductTypePropertyValueTypeTypeId in (select abstractProductTypePropertyValueTypeTypeId from abstractProductTypePropertyValueTypeTypes order by abstractProductTypePropertyValueTypeTypeId asc limit 1,1)))) > 0
begin
 insert into checkForListAndInteger(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId,NEW.productTypePropertyDefaultValue);
end;

CREATE TRIGGER triggerForCheckListAndString after insert on validateForStringValues
when (select count(*) from productTypePropertyTypeValidationValues where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId  from productTypePropertyTypeValueTypes where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId and productTypePropertyTypeValidationId in (select productTypePropertyTypeValidationTypeId from productTypePropertyTypeValidationTypes where abstractProductTypePropertyValueTypeTypeId in (select abstractProductTypePropertyValueTypeTypeId from abstractProductTypePropertyValueTypeTypes order by abstractProductTypePropertyValueTypeTypeId asc limit 1,1)))) > 0
begin
 insert into checkForListAndString(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId,NEW.productTypePropertyDefaultValue);
end;


