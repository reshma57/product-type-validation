DROP TABLE IF EXISTS checkPropertyTypeAndPropertyValues;
DROP TABLE IF EXISTS checkProductTypeForInteger;
DROP TABLE IF EXISTS checkProductTypeForFloat;
DROP TABLE IF EXISTS checkProductTypeForString;
DROP TABLE IF EXISTS validateForStringValues;
DROP TABLE IF EXISTS validateForFloatValues;
DROP TABLE IF EXISTS validateForIntegerValues;
DROP TABLE IF EXISTS zeroToTenNumber;

DROP TRIGGER IF EXISTS triggerForInsertionIncheckPropertyTypeAndPropertyValues;
DROP TRIGGER IF EXISTS triggerForCheckProductTypeForFloat;
DROP TRIGGER IF EXISTS triggerForCheckProductTypeForInteger;
DROP TRIGGER IF EXISTS triggerForCheckProductTypeForString;
DROP TRIGGER IF EXISTS triggerIsValidationOfFloatValues;
DROP TRIGGER IF EXISTS triggerValidationOfFloatValues;
DROP TRIGGER IF EXISTS triggerIsValidationOfIntegerValues;
DROP TRIGGER IF EXISTS triggerValidationOfIntegerValues;
DROP TRIGGER IF EXISTS triggerIsValidationOfStringValues;
DROP TRIGGER IF EXISTS triggerValidationOfStringValues;


CREATE TABLE checkPropertyTypeAndPropertyValues (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(255) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  Constraint pk_checkPropertyTypeAndPropertyValues Primary Key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId) REFERENCES productTypes(productTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypePropertyTypeId) REFERENCES requiredProductTypeProperties(productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE checkProductTypeForFloat (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(255) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  Constraint pk_checkProductTypeForFloat Primary Key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId) REFERENCES productTypes(productTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypePropertyTypeId) REFERENCES requiredProductTypeProperties(productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE checkProductTypeForInteger (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(255) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  Constraint pk_checkProductTypeForInteger Primary Key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId) REFERENCES productTypes(productTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypePropertyTypeId) REFERENCES requiredProductTypeProperties(productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE checkProductTypeForString (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(255) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  Constraint pk_checkProductTypeForString Primary Key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId) REFERENCES productTypes(productTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypePropertyTypeId) REFERENCES requiredProductTypeProperties(productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE validateForFloatValues (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(255) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  productTypePropertyDefaultValue varchar(255) NOT NULL,
  Constraint pk_validateForFloatValues Primary Key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId) REFERENCES productTypes(productTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypePropertyTypeId) REFERENCES requiredProductTypeProperties(productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);


CREATE TABLE validateForIntegerValues (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(255) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  productTypePropertyDefaultValue varchar(255) NOT NULL,
  Constraint pk_validateForIntegerValues Primary Key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId) REFERENCES productTypes(productTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypePropertyTypeId) REFERENCES requiredProductTypeProperties(productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE validateForStringValues (
  rollNo varchar(5) NOT NULL,
  productTypeId varchar(255) NOT NULL,
  productTypePropertyTypeId varchar(255) NOT NULL,
  productTypePropertyDefaultValue varchar(255) NOT NULL,
  Constraint pk_validateForStringValues Primary Key(rollNo,productTypeId,productTypePropertyTypeId),
  Foreign Key(rollNo) REFERENCES students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypeId) REFERENCES productTypes(productTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE,
  Foreign Key(productTypePropertyTypeId) REFERENCES requiredProductTypeProperties(productTypePropertyTypeId)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE zeroToTenNumber (
  num integer NOT NULL,
  Constraint pk_zeroToTenNumber Primary Key(num)
);

insert into zeroToTenNumber (num) values(0),(1),(2),(3),(4),(5),(6),(7),(8),(9);

CREATE TRIGGER triggerForCheckHirarchy after insert on checkPropertyTypeAndPropertyValues
when (select count(*) from validationOfDepthAndElem where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId) = 0
begin
  delete from rejectedStudents where rollNo = NEW.rollNo and msgId = 5;
  insert into rejectedStudents(rollNo,msgId,msg) values(NEW.rollNo,5,(select msg from ErrorMsg where msgId = 5));
  insert into g(a,b,c) values(5,NEW.productTypeId,NEW.productTypePropertyTypeId);
end;

CREATE TRIGGER triggerForInsertionIncheckPropertyTypeAndPropertyValues after insert on  checkValueOfProductTypePropertyTypeValues
when (select count(*) from checkPropertyTypeAndPropertyValues where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId) = 0
begin
  delete from rejectedStudents where rollNo = NEW.rollNo and msgId = 5;
  insert into checkPropertyTypeAndPropertyValues(rollNo,productTypeId,productTypePropertyTypeId) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId);
end;


CREATE TRIGGER triggerForCheckProductTypeForFloat after insert on checkPropertyTypeAndPropertyValues
when (select lower(distinct concreteSQLProductTypePropertyValueTypeDataTypeId) from productTypePropertyTypeValidationTypes where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where productTypePropertyTypeId = NEW.productTypePropertyTypeId and rollNo = NEW.rollNo)) = (select concreteSQLProductTypePropertyValueTypeDataTypeId from concreteSQLProductTypePropertyValueTypeDataTypes order by concreteSQLProductTypePropertyValueTypeDataTypeId asc limit 1)
begin
  insert into checkProductTypeForFloat(rollNo,productTypeId,productTypePropertyTypeId) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId);
end;

CREATE TRIGGER triggerForCheckProductTypeForInteger after insert on checkPropertyTypeAndPropertyValues
when (select lower(distinct concreteSQLProductTypePropertyValueTypeDataTypeId) from productTypePropertyTypeValidationTypes where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where productTypePropertyTypeId = NEW.productTypePropertyTypeId)) = (select concreteSQLProductTypePropertyValueTypeDataTypeId from concreteSQLProductTypePropertyValueTypeDataTypes order by concreteSQLProductTypePropertyValueTypeDataTypeId asc limit 1,1)
begin
  insert into checkProductTypeForInteger(rollNo,productTypeId,productTypePropertyTypeId) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId);
end;


CREATE TRIGGER triggerForCheckProductTypeForString after insert on checkPropertyTypeAndPropertyValues
when (select lower(distinct concreteSQLProductTypePropertyValueTypeDataTypeId) from productTypePropertyTypeValidationTypes where productTypePropertyTypeValidationTypeId in (select productTypePropertyTypeValidationId from productTypePropertyTypeValueTypes where productTypePropertyTypeId = NEW.productTypePropertyTypeId)) = (select concreteSQLProductTypePropertyValueTypeDataTypeId from concreteSQLProductTypePropertyValueTypeDataTypes order by concreteSQLProductTypePropertyValueTypeDataTypeId desc limit 1)
begin
  insert into checkProductTypeForString(rollNo,productTypeId,productTypePropertyTypeId) values(NEW.rollNo,NEW.productTypeId,NEW.productTypePropertyTypeId);
end;

CREATE TRIGGER triggerIsValidationOfFloatValues after insert on checkProductTypeForFloat
when (select instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),".")) = 0
begin
  delete from rejectedStudents where rollNo = NEW.rollNo and msgId = 6;
  insert into rejectedStudents(rollNo,msgId,msg) values(NEW.rollNo,6,(select msg from ErrorMsg where msgId = 6));
  insert into g(a,b,c) values(6,NEW.productTypeId,NEW.productTypePropertyTypeId);
end;

CREATE TRIGGER triggerValidationOfFloatValues after insert on checkProductTypeForFloat
when (select instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId),".")) > 0
begin
  insert into validateForFloatValues(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue) select distinct rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId;
end;

CREATE TRIGGER triggerIsValidationOfIntegerValues after insert on checkProductTypeForInteger 
when (select instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),".")) > 0
  or
(select instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 0,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 1,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 2,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 3,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 4,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 5,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 6,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 7,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 8,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 9,1))) = 0
begin
  delete from rejectedStudents where rollNo = NEW.rollNo and msgId = 7;
  insert into rejectedStudents(rollNo,msgId,msg) values(NEW.rollNo,7,(select msg from ErrorMsg where msgId = 7));
  insert into g(a,b,c) values(7,NEW.productTypeId,NEW.productTypePropertyTypeId);
end;


CREATE TRIGGER triggerValidationOfIntegerValues after insert on checkProductTypeForInteger 
when (select instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),".")) = 0
  and
(select instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 0,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 1,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 2,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 3,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 4,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 5,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 6,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 7,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 8,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 9,1))) > 0
begin
  insert into validateForIntegerValues(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue) select rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId;
end;

CREATE TRIGGER triggerValidationOfStringValues after insert on checkProductTypeForString
when (select instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),".")) = 0
  and
(select instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 0,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 1,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 2,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 3,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 4,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 5,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 6,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 7,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 8,1)) or instr((select productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and  productTypePropertyTypeId = NEW.productTypePropertyTypeId),(select num from zeroToTenNumber limit 9,1))) = 0
begin
  insert into validateForStringValues(rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue) select distinct rollNo,productTypeId,productTypePropertyTypeId,productTypePropertyDefaultValue from requiredProductTypeProperties where rollNo = NEW.rollNo and productTypeId = NEW.productTypeId and productTypePropertyTypeId = NEW.productTypePropertyTypeId;
end;
