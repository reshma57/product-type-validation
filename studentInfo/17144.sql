insert into productTypes values("17144","588","candle","https://en.wikipedia.org/wiki/Candle");
insert into productTypes values("17144","588-01","Little India DD201 Candle " ,"https://www.flipkart.com/little-india-dd201-candle/p/itmexwq57nyzfqrk?pid=CANEXWQ5YMAVFXZB&srno=s_1_5&otracker=search&lid=LSTCANEXWQ5YMAVFXZB5Q43QB&fm=SEARCH&iid=6c28b5be-4018-4c25-9a63-ba003e5e30b7.CANEXWQ5YMAVFXZB.SEARCH&qH=ce23a7324c0caa15");
insert into productTypes values("17144","588-02","Little India DD203R Candle ", "https://www.flipkart.com/little-india-dd203r-candle/p/itmexwq5dpny8pvn?pid=CANEXWQ5G6VSZHYS&srno=s_1_10&otracker=search&lid=LSTCANEXWQ5G6VSZHYS9RNXM4&fm=SEARCH&iid=9a9049f9-ca3d-45ec-9190-bb8d3559d03c.CANEXWQ5G6VSZHYS.SEARCH&qH=ce23a7324c0caa15");

insert into productTypeHierarchy values("17144","588","588-01");
insert into productTypeHierarchy values("17144","588","588-02");

insert into productTypePropertyTypes values("17144","candleFeatures","features of candle");
insert into productTypePropertyTypes values("17144","candleMaterial","Microcrystalline wax/Beeswax/plastic");
insert into productTypePropertyTypes values("17144","SizeOfcandle","sizes(small/medium/large)");
insert into productTypePropertyTypes values("17144","Wick","is candle have wick");
insert into productTypePropertyTypes values("17144","wicktype","type of Wick");
insert into productTypePropertyTypes values("17144","candleDiameter","how much diameter candle have?");
insert into productTypePropertyTypes values("17144","wickDiameter","how much diameter wick have?");
insert into productTypePropertyTypes values("17144","stiffness","does it have stiffness(yes/no)");
insert into productTypePropertyTypes values("17144","FireResistance","Is it fire resistance(yes/no)");
insert into productTypePropertyTypes values("17144","burnTime","how much time candle burns?");
insert into productTypePropertyTypes values("17144","temperature","how much temp. flame have?");
insert into productTypePropertyTypes values("17144","fragrance","candle have which fragrance?");
insert into productTypePropertyTypes values("17144","luminousIntensity","how much luminous intensity candle have?");
insert into productTypePropertyTypes values("17144","Shape","which shape candle have"); 
insert into productTypePropertyTypes values("17144","occasion","occasion of use(festival/decorative/birthday)"); 
insert into productTypePropertyTypes values("17144","stand","is stand required?(yes/no)");
insert into productTypePropertyTypes values("17144","designimpresion","impresive look(yes/no)"); 
insert into productTypePropertyTypes values("17144","tray","is tray required?(yes/no)"); 
insert into productTypePropertyTypes values("17144","status","status of candle(good/medium/ordinary)"); 
insert into productTypePropertyTypes values("17144","specialFeatures","special features of candle"); 

insert into productTypePropertyTypeContainer values("17144","candleFeatures","Wick","1");
insert into productTypePropertyTypeContainer values("17144","candleFeatures","fragrance","2");
insert into productTypePropertyTypeContainer values("17144","candleFeatures","specialFeatures","3");

insert into productTypePropertyTypeContainer values("17144","Wick","wickDiameter","1");
insert into productTypePropertyTypeContainer values("17144","Wick","luminousIntensity","2");
insert into productTypePropertyTypeContainer values("17144","Wick","temperature","3");
insert into productTypePropertyTypeContainer values("17144","Wick","wicktype","4");

insert into productTypePropertyTypeContainer values("17144","fragrance","sandalwood","1");
insert into productTypePropertyTypeContainer values("17144","fragrance","lemon","2");
insert into productTypePropertyTypeContainer values("17144","fragrance","lotus","3");
insert into productTypePropertyTypeContainer values("17144","fragrance","chocolate","4");
insert into productTypePropertyTypeContainer values("17144","fragrance","vanilla","5");
insert into productTypePropertyTypeContainer values("17144","fragrance","rose","6");

insert into productTypePropertyTypeContainer values("17144","specialFeatures","burnTime","1");
insert into productTypePropertyTypeContainer values("17144","specialFeatures","FireResistance","2");
insert into productTypePropertyTypeContainer values("17144","specialFeatures","designimpresion","3");
insert into productTypePropertyTypeContainer values("17144","specialFeatures","stiffness","4");

insert into requiredProductTypeProperties values("17144","588","instructions","Always keep a burning candle within sight");
insert into requiredProductTypeProperties values("17144","588","subInstruction1","Always use a candle-holder specifically designed for candle use");
insert into requiredProductTypeProperties values("17144","588","subInstruction2","Never touch or move a burning candle or container candle when the wax is liquid");
insert into requiredProductTypeProperties values("17144","588","subInstruction3","Keep burning candles out of the reach of children and pets");
insert into requiredProductTypeProperties values("17144","588","subInstruction4","Be sure the candle-holder is placed on a stable, heat-resistant surface");

insert into requiredProductTypeProperties values("17144","588","weight","100");   
insert into requiredProductTypeProperties values("17144","588","dim1","4");
insert into requiredProductTypeProperties values("17144","588","dim2","4");
insert into requiredProductTypeProperties values("17144","588","dim3","5");
insert into requiredProductTypeProperties values("17144","588","price","139.00");
insert into requiredProductTypeProperties values("17144","588","SellerName","RetailNet (4.1)"); 
insert into requiredProductTypeProperties values("17144","588","mfgDate","30 December 2016");
insert into requiredProductTypeProperties values("17144","588","color","red");

insert into requiredProductTypeProperties values("17144","588","candleFeatures","features");
insert into requiredProductTypeProperties values("17144","588","candleMaterial","plastic");
insert into requiredProductTypeProperties values("17144","588","SizeOfcandle","small");
insert into requiredProductTypeProperties values("17144","588","Wick","yes");
insert into requiredProductTypeProperties values("17144","588","wicktype","flat");
insert into requiredProductTypeProperties values("17144","588","candleDiameter","11");
insert into requiredProductTypeProperties values("17144","588","wickDiameter","5");
insert into requiredProductTypeProperties values("17144","588","stiffness","yes");
insert into requiredProductTypeProperties values("17144","588","FireResistance","yes");
insert into requiredProductTypeProperties values("17144","588","burnTime","40");
insert into requiredProductTypeProperties values("17144","588","temperature","60");
insert into requiredProductTypeProperties values("17144","588","fragrance","sandalwood");
insert into requiredProductTypeProperties values("17144","588","luminousIntensity","2");
insert into requiredProductTypeProperties values("17144","588","Shape","cylindrical"); 
insert into requiredProductTypeProperties values("17144","588","occasion","festival"); 
insert into requiredProductTypeProperties values("17144","588","stand","yes");
insert into requiredProductTypeProperties values("17144","588","designimpresion","yes"); 
insert into requiredProductTypeProperties values("17144","588","tray","yes"); 
insert into requiredProductTypeProperties values("17144","588","status","good"); 
insert into requiredProductTypeProperties values("17144","588","specialFeatures","yes"); 

insert into requiredProductTypeProperties values("17144","588-01","weight","110");   
insert into requiredProductTypeProperties values("17144","588-01","dim1","4");
insert into requiredProductTypeProperties values("17144","588-01","dim2","4");
insert into requiredProductTypeProperties values("17144","588-01","dim3","5");
insert into requiredProductTypeProperties values("17144","588-01","price","139.00");
insert into requiredProductTypeProperties values("17144","588-01","SellerName","RetailNet (4.1)"); 
insert into requiredProductTypeProperties values("17144","588-01","mfgDate","30 December 2016");
insert into requiredProductTypeProperties values("17144","588-01","color","white");

insert into requiredProductTypeProperties values("17144","588-01","candleFeatures","features");
insert into requiredProductTypeProperties values("17144","588-01","candleMaterial","plastic");
insert into requiredProductTypeProperties values("17144","588-01","SizeOfcandle","medium");
insert into requiredProductTypeProperties values("17144","588-01","Wick","yes");
insert into requiredProductTypeProperties values("17144","588-01","wicktype","flat");
insert into requiredProductTypeProperties values("17144","588-01","candleDiameter","12");
insert into requiredProductTypeProperties values("17144","588-01","wickDiameter","6");
insert into requiredProductTypeProperties values("17144","588-01","stiffness","yes");
insert into requiredProductTypeProperties values("17144","588-01","FireResistance","yes");
insert into requiredProductTypeProperties values("17144","588-01","burnTime","45");
insert into requiredProductTypeProperties values("17144","588-01","temperature","60");
insert into requiredProductTypeProperties values("17144","588-01","fragrance","sandalwood");
insert into requiredProductTypeProperties values("17144","588-01","luminousIntensity","2");
insert into requiredProductTypeProperties values("17144","588-01","Shape","cylindrical"); 
insert into requiredProductTypeProperties values("17144","588-01","occasion","festival"); 
insert into requiredProductTypeProperties values("17144","588-01","stand","yes");
insert into requiredProductTypeProperties values("17144","588-01","designimpresion","yes"); 
insert into requiredProductTypeProperties values("17144","588-01","tray","yes"); 
insert into requiredProductTypeProperties values("17144","588-01","status","good"); 
insert into requiredProductTypeProperties values("17144","588-01","specialFeatures","yes"); 

insert into requiredProductTypeProperties values("17144","588-02","weight","120");   
insert into requiredProductTypeProperties values("17144","588-02","dim1","4");
insert into requiredProductTypeProperties values("17144","588-02","dim2","4");
insert into requiredProductTypeProperties values("17144","588-02","dim3","5");
insert into requiredProductTypeProperties values("17144","588-02","price","139.00");
insert into requiredProductTypeProperties values("17144","588-02","SellerName","RetailNet (4.1)"); 
insert into requiredProductTypeProperties values("17144","588-02","mfgDate","30 December 2016");
insert into requiredProductTypeProperties values("17144","588-02","color","red");

insert into requiredProductTypeProperties values("17144","588-02","candleFeatures","features");
insert into requiredProductTypeProperties values("17144","588-02","candleMaterial","plastic");
insert into requiredProductTypeProperties values("17144","588-02","SizeOfcandle","large");
insert into requiredProductTypeProperties values("17144","588-02","Wick","yes");
insert into requiredProductTypeProperties values("17144","588-02","wicktype","flat");
insert into requiredProductTypeProperties values("17144","588-02","candleDiameter","13");
insert into requiredProductTypeProperties values("17144","588-02","wickDiameter","5");
insert into requiredProductTypeProperties values("17144","588-02","stiffness","yes");
insert into requiredProductTypeProperties values("17144","588-02","FireResistance","yes");
insert into requiredProductTypeProperties values("17144","588-02","burnTime","45");
insert into requiredProductTypeProperties values("17144","588-02","temperature","60");
insert into requiredProductTypeProperties values("17144","588-02","fragrance","sandalwood");
insert into requiredProductTypeProperties values("17144","588-02","luminousIntensity","2");
insert into requiredProductTypeProperties values("17144","588-02","Shape","cylindrical"); 
insert into requiredProductTypeProperties values("17144","588-02","occasion","festival"); 
insert into requiredProductTypeProperties values("17144","588-02","stand","yes");
insert into requiredProductTypeProperties values("17144","588-02","designimpresion","yes"); 
insert into requiredProductTypeProperties values("17144","588-02","tray","yes"); 
insert into requiredProductTypeProperties values("17144","588-02","status","good"); 
insert into requiredProductTypeProperties values("17144","588-02","specialFeatures","yes"); 

insert into productTypePropertyTypeValidationTypes values("17144","17144-588-instructions","container","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-subInstructions1","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-subInstructions2","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-subInstructions3","list","string");

insert into productTypePropertyTypeValidationTypes values("17144","17144-588-weight","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-dim1","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-dim2","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-dim3","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-price","range","float");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-SellerName","list","string"); 
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-mfgDate","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-color","list","string");

insert into productTypePropertyTypeValidationTypes values("17144","17144-588-candleFeatures","container","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-candleMaterial","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-SizeOfcandle","range","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-Wick","container","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-wicktype","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-candleDiameter","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-wickDiameter","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-stiffness","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-FireResistance","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-burnTime","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-temperature","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-fragrance","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-luminousIntensity","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-Shape","list","string"); 
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-occasion","list","string"); 
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-stand","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-designimpresion","list","string"); 
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-tray","list","string"); 
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-status","list","string"); 
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-specialFeatures","list","string"); 

insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-weight","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-dim1","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-dim2","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-dim3","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-price","range","float");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-SellerName","list","string"); 
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-mfgDate","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-color","list","string");

insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-candleFeatures","container","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-candleMaterial","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-SizeOfcandle","range","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-Wick","container","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-wicktype","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-candleDiameter","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-wickDiameter","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-stiffness","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-FireResistance","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-burnTime","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-temperature","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-fragrance","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-luminousIntensity","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-Shape","list","string"); 
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-occasion","list","string"); 
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-stand","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-designimpresion","list","string"); 
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-tray","list","string"); 
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-status","list","string"); 
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-01-specialFeatures","list","string"); 

insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-weight","range","float");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-dim1","range","float");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-dim2","range","float");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-dim3","range","float");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-price","range","float");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-SellerName","list","string"); 
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-mfgDate","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-color","list","string");

insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-candleFeatures","container","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-candleMaterial","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-SizeOfcandle","range","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-Wick","container","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-wicktype","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-candleDiameter","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-wickDiameter","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-stiffness","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-FireResistance","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-burnTime","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-temperature","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-fragrance","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-luminousIntensity","range","int");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-Shape","list","string"); 
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-occasion","list","string"); 
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-stand","list","string");
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-designimpresion","list","string"); 
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-tray","list","string"); 
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-status","list","string"); 
insert into productTypePropertyTypeValidationTypes values("17144","17144-588-02-specialFeatures","list","string"); 

insert into productTypePropertyTypeValueTypes values("17144","588","instructions","","17144-588-instructions");
insert into productTypePropertyTypeValueTypes values("17144","588","subInstruction1","","17144-588-subInstructions1");
insert into productTypePropertyTypeValueTypes values("17144","588","subInstruction2","","17144-588-subInstructions2");
insert into productTypePropertyTypeValueTypes values("17144","588","subInstruction3","","17144-588-subInstructions3");

insert into productTypePropertyTypeValueTypes values("17144","588","weight","gm","17144-588-weight");
insert into productTypePropertyTypeValueTypes values("17144","588","dim1","cm","17144-588-dim1");
insert into productTypePropertyTypeValueTypes values("17144","588","dim2","cm","17144-588-dim2");
insert into productTypePropertyTypeValueTypes values("17144","588","dim3","cm","17144-588-dim3");
insert into productTypePropertyTypeValueTypes values("17144","588","price","rs","17144-588-price");
insert into productTypePropertyTypeValueTypes values("17144","588","SellerName","","17144-588-SellerName");
insert into productTypePropertyTypeValueTypes values("17144","588","mfgDate","","17144-588-mfgDate");
insert into productTypePropertyTypeValueTypes values("17144","588","color","","17144-588-color");

insert into productTypePropertyTypeValueTypes values("17144","588","candleFeatures","","17144-588-candleFeatures");
insert into productTypePropertyTypeValueTypes values("17144","588","candleMaterial","","17144-588-candleMaterial");
insert into productTypePropertyTypeValueTypes values("17144","588","SizeOfcandle","inches","17144-588-SizeOfcandle");
insert into productTypePropertyTypeValueTypes values("17144","588","Wick","","17144-588-Wick");
insert into productTypePropertyTypeValueTypes values("17144","588","wicktype","","17144-588-wicktype");
insert into productTypePropertyTypeValueTypes values("17144","588","candleDiameter","inches","17144-588-candleDiameter");
insert into productTypePropertyTypeValueTypes values("17144","588","wickDiameter","inches","17144-588-wickDiameter");
insert into productTypePropertyTypeValueTypes values("17144","588","stiffness","","17144-588-stiffness");
insert into productTypePropertyTypeValueTypes values("17144","588","FireResistance","","17144-588-FireResistance");
insert into productTypePropertyTypeValueTypes values("17144","588","burnTime","minute","17144-588-burnTime");
insert into productTypePropertyTypeValueTypes values("17144","588","temperature","DCelcius","17144-588-temperature");
insert into productTypePropertyTypeValueTypes values("17144","588","fragrance","","17144-588-fragrance");
insert into productTypePropertyTypeValueTypes values("17144","588","luminousIntensity","candela","17144-588-luminousIntensity");
insert into productTypePropertyTypeValueTypes values("17144","588","Shape","","17144-588-Shape"); 
insert into productTypePropertyTypeValueTypes values("17144","588","occasion","","17144-588-occasion"); 
insert into productTypePropertyTypeValueTypes values("17144","588","stand","","17144-588-stand");
insert into productTypePropertyTypeValueTypes values("17144","588","designimpresion","","17144-588-designimpresion"); 
insert into productTypePropertyTypeValueTypes values("17144","588","tray","","17144-588-tray"); 
insert into productTypePropertyTypeValueTypes values("17144","588","status","","17144-588-status"); 
insert into productTypePropertyTypeValueTypes values("17144","588","specialFeatures","","17144-588-specialFeatures"); 

insert into productTypePropertyTypeValueTypes values("17144","588-01","weight","gm","17144-588-01-weight");
insert into productTypePropertyTypeValueTypes values("17144","588-01","dim1","cm","17144-588-01-dim1");
insert into productTypePropertyTypeValueTypes values("17144","588-01","dim2","cm","17144-588-01-dim2");
insert into productTypePropertyTypeValueTypes values("17144","588-01","dim3","cm","17144-588-01-dim3");
insert into productTypePropertyTypeValueTypes values("17144","588-01","price","rs","17144-588-01-price");
insert into productTypePropertyTypeValueTypes values("17144","588-01","SellerName","","17144-588-01-SellerName");
insert into productTypePropertyTypeValueTypes values("17144","588-01","mfgDate","","17144-588-01-mfgDate");
insert into productTypePropertyTypeValueTypes values("17144","588-01","color","","17144-588-01-color");

insert into productTypePropertyTypeValueTypes values("17144","588-01","candleFeatures","","17144-588-01-candleFeatures");
insert into productTypePropertyTypeValueTypes values("17144","588-01","candleMaterial","","17144-588-01-candleMaterial");
insert into productTypePropertyTypeValueTypes values("17144","588-01","SizeOfcandle","inches","17144-588-01-SizeOfcandle");
insert into productTypePropertyTypeValueTypes values("17144","588-01","Wick","","17144-588-01-Wick");
insert into productTypePropertyTypeValueTypes values("17144","588-01","wicktype","","17144-588-01-wicktype");
insert into productTypePropertyTypeValueTypes values("17144","588-01","candleDiameter","inches","17144-588-01-candleDiameter");
insert into productTypePropertyTypeValueTypes values("17144","588-01","wickDiameter","inches","17144-588-01-wickDiameter");
insert into productTypePropertyTypeValueTypes values("17144","588-01","stiffness","","17144-588-01-stiffness");
insert into productTypePropertyTypeValueTypes values("17144","588-01","FireResistance","","17144-588-01-FireResistance");
insert into productTypePropertyTypeValueTypes values("17144","588-01","burnTime","minute","17144-588-01-burnTime");
insert into productTypePropertyTypeValueTypes values("17144","588-01","temperature","DCelcius","17144-588-01-temperature");
insert into productTypePropertyTypeValueTypes values("17144","588-01","fragrance","","17144-588-01-fragrance");
insert into productTypePropertyTypeValueTypes values("17144","588-01","luminousIntensity","candela","17144-588-01-luminousIntensity");
insert into productTypePropertyTypeValueTypes values("17144","588-01","Shape","","17144-588-01-Shape"); 
insert into productTypePropertyTypeValueTypes values("17144","588-01","occasion","","17144-588-01-occasion"); 
insert into productTypePropertyTypeValueTypes values("17144","588-01","stand","","17144-588-01-stand");
insert into productTypePropertyTypeValueTypes values("17144","588-01","designimpresion","","17144-588-01-designimpresion"); 
insert into productTypePropertyTypeValueTypes values("17144","588-01","tray","","17144-588-01-tray"); 
insert into productTypePropertyTypeValueTypes values("17144","588-01","status","","17144-588-01-status"); 
insert into productTypePropertyTypeValueTypes values("17144","588-01","specialFeatures","","17144-588-01-specialFeatures"); 

insert into productTypePropertyTypeValueTypes values("17144","588-02","weight","gm","17144-588-02-weight");
insert into productTypePropertyTypeValueTypes values("17144","588-02","dim1","cm","17144-588-02-dim1");
insert into productTypePropertyTypeValueTypes values("17144","588-02","dim2","cm","17144-588-02-dim2");
insert into productTypePropertyTypeValueTypes values("17144","588-02","dim3","cm","17144-588-02-dim3");
insert into productTypePropertyTypeValueTypes values("17144","588-02","price","rs","17144-588-02-price");
insert into productTypePropertyTypeValueTypes values("17144","588-02","SellerName","","17144-588-02-SellerName");
insert into productTypePropertyTypeValueTypes values("17144","588-02","mfgDate","","17144-588-02-mfgDate");
insert into productTypePropertyTypeValueTypes values("17144","588-02","color","","17144-588-02-color");

insert into productTypePropertyTypeValueTypes values("17144","588-02","candleFeatures","","17144-588-02-candleFeatures");
insert into productTypePropertyTypeValueTypes values("17144","588-02","candleMaterial","","17144-588-02-candleMaterial");
insert into productTypePropertyTypeValueTypes values("17144","588-02","SizeOfcandle","inches","17144-588-02-SizeOfcandle");
insert into productTypePropertyTypeValueTypes values("17144","588-02","Wick","","17144-588-02-Wick");
insert into productTypePropertyTypeValueTypes values("17144","588-02","wicktype","","17144-588-02-wicktype");
insert into productTypePropertyTypeValueTypes values("17144","588-02","candleDiameter","inches","17144-588-02-candleDiameter");
insert into productTypePropertyTypeValueTypes values("17144","588-02","wickDiameter","inches","17144-588-02-wickDiameter");
insert into productTypePropertyTypeValueTypes values("17144","588-02","stiffness","","17144-588-02-stiffness");
insert into productTypePropertyTypeValueTypes values("17144","588-02","FireResistance","","17144-588-02-FireResistance");
insert into productTypePropertyTypeValueTypes values("17144","588-02","burnTime","minute","17144-588-02-burnTime");
insert into productTypePropertyTypeValueTypes values("17144","588-02","temperature","DCelcius","17144-588-02-temperature");
insert into productTypePropertyTypeValueTypes values("17144","588-02","fragrance","","17144-588-02-fragrance");
insert into productTypePropertyTypeValueTypes values("17144","588-02","luminousIntensity","candela","17144-588-02-luminousIntensity");
insert into productTypePropertyTypeValueTypes values("17144","588-02","Shape","","17144-588-02-Shape"); 
insert into productTypePropertyTypeValueTypes values("17144","588-02","occasion","","17144-588-02-occasion"); 
insert into productTypePropertyTypeValueTypes values("17144","588-02","stand","","17144-588-02-stand");
insert into productTypePropertyTypeValueTypes values("17144","588-02","designimpresion","","17144-588-02-designimpresion"); 
insert into productTypePropertyTypeValueTypes values("17144","588-02","tray","","17144-588-02-tray"); 
insert into productTypePropertyTypeValueTypes values("17144","588-02","status","","17144-588-02-status"); 
insert into productTypePropertyTypeValueTypes values("17144","588-02","specialFeatures","","17144-588-02-specialFeatures"); 

insert into productTypePropertyTypeValidationValues values("17144","17144-588-instructions","1","Always keep a burning candle within sight");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-subInstruction1","2","Always use a candle-holder specifically designed for candle use");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-subInstruction2","3","Never touch or move a burning candle or container candle when the wax is liquid");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-subInstruction3","4","Keep burning candles out of the reach of children and pets");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-subInstruction4","5","Be sure the candle-holder is placed on a stable, heat-resistant surface");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-weight","1","50");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-weight","2","150");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-dim1","1","3");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-dim1","2","6");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-dim2","1","3");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-dim2","2","6");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-dim3","1","3");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-dim3","2","6");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-price","1","100.00");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-price","2","200.00");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-SellerName","1","RetailNet (4.1)"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-mfgDate","1","30 December 2016");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-color","1","white");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-color","2","red");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-candleFeatures","1","Wick");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-candleFeatures","2","fragrance");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-candleFeatures","3","specialFeatures");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-candleMaterial","1","Microcrystalline wax");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-candleMaterial","2","Beeswax");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-candleMaterial","3","plastic");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-SizeOfcandle","1","small");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-SizeOfcandle","2","medium");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-SizeOfcandle","3","large");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-Wick","1","yes");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-Wick","2","no");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-wicktype","1","flat");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-wicktype","2","square");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-wicktype","3","cored");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-candleDiameter","1","10");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-candleDiameter","2","18");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-wickDiameter","1","3");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-wickDiameter","2","7");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-stiffness","1","yes");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-stiffness","2","no");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-FireResistance","1","yes");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-FireResistance","2","no");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-burnTime","1","30");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-burnTime","2","50");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-temperature","1","50");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-temperature","2","100");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-fragrance","1","sandalwood");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-fragrance","2","lemon");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-fragrance","3","lotus");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-fragrance","4","chocolate");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-fragrance","5","vanilla");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-fragrance","6","rose");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-luminousIntensity","1","1");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-luminousIntensity","2","3");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-Shape","1","cylindrical"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-Shape","2","square"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-Shape","3","rectangular"); 

insert into productTypePropertyTypeValidationValues values("17144","17144-588-occasion","1","festival"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-occasion","2","decorative"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-occasion","3","birthday"); 

insert into productTypePropertyTypeValidationValues values("17144","17144-588-stand","1","yes");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-stand","2","no");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-designimpresion","1","yes"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-designimpresion","2","no"); 

insert into productTypePropertyTypeValidationValues values("17144","17144-588-tray","1","yes"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-tray","2","no"); 

insert into productTypePropertyTypeValidationValues values("17144","17144-588-status","1","good"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-status","2","medium"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-status","3","ordinary"); 

insert into productTypePropertyTypeValidationValues values("17144","17144-588-specialFeatures","1","yes"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-specialFeatures","2","no"); 


insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-weight","1","50");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-weight","2","150");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-dim1","1","3");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-dim1","2","6");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-dim2","1","3");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-dim2","2","6");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-dim3","1","3");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-dim3","2","6");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-price","1","100.00");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-price","2","200.00");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-SellerName","1","RetailNet (4.1)"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-mfgDate","1","30 December 2016");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-color","1","white");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-color","2","red");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-candleFeatures","1","Wick");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-candleFeatures","2","fragrance");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-candleFeatures","3","specialFeatures");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-candleMaterial","1","Microcrystalline wax");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-candleMaterial","2","Beeswax");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-candleMaterial","3","plastic");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-SizeOfcandle","1","small");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-SizeOfcandle","2","medium");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-SizeOfcandle","3","large");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-Wick","1","yes");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-Wick","2","no");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-wicktype","1","flat");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-wicktype","2","square");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-wicktype","3","cored");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-candleDiameter","1","10");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-candleDiameter","2","18");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-wickDiameter","1","3");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-wickDiameter","2","7");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-stiffness","1","yes");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-stiffness","2","no");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-FireResistance","1","yes");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-FireResistance","2","no");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-burnTime","1","30");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-burnTime","2","50");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-temperature","1","50");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-temperature","2","100");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-fragrance","1","sandalwood");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-fragrance","2","lemon");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-fragrance","3","lotus");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-fragrance","4","chocolate");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-fragrance","5","vanilla");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-fragrance","6","rose");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-luminousIntensity","1","1");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-luminousIntensity","2","3");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-Shape","1","cylindrical"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-Shape","2","square"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-Shape","3","rectangular"); 

insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-occasion","1","festival"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-occasion","2","decorative"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-occasion","3","birthday"); 

insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-stand","1","yes");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-stand","2","no");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-designimpresion","1","yes"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-designimpresion","2","no"); 

insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-tray","1","yes"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-tray","2","no"); 

insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-status","1","good"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-status","2","medium"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-status","3","ordinary"); 

insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-specialFeatures","1","yes"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-01-specialFeatures","2","no"); 



insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-weight","1","50");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-weight","2","150");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-dim1","1","3");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-dim1","2","6");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-dim2","1","3");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-dim2","2","6");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-dim3","1","3");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-dim3","2","6");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-price","1","100.00");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-price","2","200.00");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-SellerName","1","RetailNet (4.1)"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-mfgDate","2","28 June 2017");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-color","1","white");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-color","2","red");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-candleFeatures","1","Wick");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-candleFeatures","2","fragrance");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-candleFeatures","3","specialFeatures");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-candleMaterial","1","Microcrystalline wax");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-candleMaterial","2","Beeswax");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-candleMaterial","3","plastic");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-SizeOfcandle","1","small");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-SizeOfcandle","2","medium");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-SizeOfcandle","3","large");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-Wick","1","yes");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-Wick","2","no");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-wicktype","1","flat");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-wicktype","2","square");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-wicktype","3","cored");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-candleDiameter","1","10");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-candleDiameter","2","18");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-wickDiameter","1","3");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-wickDiameter","2","7");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-stiffness","1","yes");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-stiffness","2","no");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-FireResistance","1","yes");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-FireResistance","2","no");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-burnTime","1","30");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-burnTime","2","50");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-temperature","1","50");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-temperature","2","100");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-fragrance","1","sandalwood");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-fragrance","2","lemon");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-fragrance","3","lotus");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-fragrance","4","chocolate");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-fragrance","5","vanilla");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-fragrance","6","rose");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-luminousIntensity","1","1");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-luminousIntensity","2","3");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-Shape","1","cylindrical"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-Shape","2","square"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-Shape","3","rectangular"); 

insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-occasion","1","festival"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-occasion","2","decorative"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-occasion","3","birthday"); 

insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-stand","1","yes");
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-stand","2","no");

insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-designimpresion","1","yes"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-designimpresion","2","no"); 

insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-tray","1","yes"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-tray","2","no"); 

insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-status","1","good"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-status","2","medium"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-status","3","ordinary");



insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-specialFeatures","1","yes"); 
insert into productTypePropertyTypeValidationValues values("17144","17144-588-02-specialFeatures","2","no");  

