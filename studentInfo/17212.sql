insert into productTypes values('17212','978','oddy','https://prezi.com/mdyglkqmpfs3/physical-and-chemical-properties-of-chalk/');
insert into productTypes values('17212','978-01','National Chalk','https://www.amazon.in/National-Dustless-Chalk-White-Boxes/dp/B01N2Q6XRT/ref=sr_1_2?ie=UTF8&qid=1508737185&sr=8-2&keywords=chalk+box');
insert into productTypes values('17212','978-02','Apsara Chalk','https://www.amazon.in/Apsara-Chalk-Stick-White-Pack/dp/B00LQIZRNA/ref=sr_1_1?s=toys&ie=UTF8&qid=1509799819&sr=8-1&keywords=chalk+apsara');

insert into productTypeHierarchy values('17212','978','978-01');
insert into productTypeHierarchy values('17212','978','978-02');





insert into productTypePropertyTypes values('17212','chalkMaterial','calcium/silt/clay');
insert into productTypePropertyTypes values('17212','sizeOfChalk','small/medium');
insert into productTypePropertyTypes values('17212','isChalkWaterproof','yes/no');
insert into productTypePropertyTypes values('17212','isChalkDurable','yes/no');
insert into productTypePropertyTypes values('17212','isChalkEasyWriting','yes/no');
insert into productTypePropertyTypes values('17212','ChalkFeatures','features of chalk');
insert into productTypePropertyTypes values('17212','Build','build features of chalk of chalk');
insert into productTypePropertyTypes values('17212','ChemicalProperty','chemical property of chalk');
insert into productTypePropertyTypes values('17212','Usage','uses of Chalk');
insert into productTypePropertyTypes values('17212','Material',' yes/no');
insert into productTypePropertyTypes values('17212','Durable','yes/no');
insert into productTypePropertyTypes values('17212','EnvironmentFriendly','how much environmentfriendly it is');
insert into productTypePropertyTypes values('17212','Luster','yes/no');
insert into productTypePropertyTypes values('17212','Malleability',' yes/no');
insert into productTypePropertyTypes values('17212','Ductility',' yes/no');
insert into productTypePropertyTypes values('17212','Writing',' yes/no');
insert into productTypePropertyTypes values('17212','Visibility','hows the visibilty of the chalk');
insert into productTypePropertyTypes values('17212','Dustless',' yes/no');



insert into productTypePropertyTypeContainer values('17212','ChalkFeatures','Build','1');
insert into productTypePropertyTypeContainer values('17212','ChalkFeatures','ChemicalProperty','2');
insert into productTypePropertyTypeContainer values('17212','ChalkFeatures','Usage','3');
insert into productTypePropertyTypeContainer values('17212','Build','Material','1');
insert into productTypePropertyTypeContainer values('17212','Build','Durable','2');
insert into productTypePropertyTypeContainer values('17212','Build','EnvironmentFriendly','3');
insert into productTypePropertyTypeContainer values('17212','ChemicalProperty','Luster','1');
insert into productTypePropertyTypeContainer values('17212','ChemicalProperty','Malleability','2');
insert into productTypePropertyTypeContainer values('17212','ChemicalProperty','Ductility','3');
insert into productTypePropertyTypeContainer values('17212','Usage','Writing','1');
insert into productTypePropertyTypeContainer values('17212','Usage','Visibility','2');
insert into productTypePropertyTypeContainer values('17212','Usage','Dustless','3');






insert into requiredProductTypeProperties values('17212','978','weight','20.0');
insert into requiredProductTypeProperties values('17212','978','dim1','10.2');
insert into requiredProductTypeProperties values('17212','978','dim2','7.2');
insert into requiredProductTypeProperties values('17212','978','dim3','2.6');
insert into requiredProductTypeProperties values('17212','978','color','multi');
insert into requiredProductTypeProperties values('17212','978','manufacturer','oddy');
insert into requiredProductTypeProperties values('17212','978','price','142.0');
insert into requiredProductTypeProperties values('17212','978','mfgDate','24/10/2017');
insert into requiredProductTypeProperties values('17212','978','url','https://prezi.com/mdyglkqmpfs3/physical-and-chemical-properties-of-chalk/');
insert into requiredProductTypeProperties values('17212','978','instructions','Chalk instructions');
insert into requiredProductTypeProperties values('17212','978','subInstruction1','Never use small broken Chalk');
insert into requiredProductTypeProperties values('17212','978','subInstruction2','Should keep away from kids');
insert into requiredProductTypeProperties values('17212','978','subInstruction3','Do not immerse in water');
insert into requiredProductTypeProperties values('17212','978','subInstruction4','Fragile so handle with care');
insert into requiredProductTypeProperties values('17212','978','subInstruction5','keep away from skin contact');
insert into requiredProductTypeProperties values('17212','978','subInstruction6','Do not inhale ');
insert into requiredProductTypeProperties values('17212','978','subInstruction7','keep away from eye contact');
insert into requiredProductTypeProperties values('17212','978','subInstruction8','Avoid ingestion');
insert into requiredProductTypeProperties values('17212','978','chalkMaterial','calcium carbonate');
insert into requiredProductTypeProperties values('17212','978','sizeOfChalk','medium');
insert into requiredProductTypeProperties values('17212','978','isChalkWaterproof','no');
insert into requiredProductTypeProperties values('17212','978','isChalkDurable','yes');
insert into requiredProductTypeProperties values('17212','978','isChalkEasyWriting','yes');
insert into requiredProductTypeProperties values('17212','978','ChalkFeatures','features of chalk');
insert into requiredProductTypeProperties values('17212','978','Build','build features of chalk');
insert into requiredProductTypeProperties values('17212','978','ChemicalProperty','chemical property of chalk');
insert into requiredProductTypeProperties values('17212','978','Usage','uses of Chalk');
insert into requiredProductTypeProperties values('17212','978','Material','yes');
insert into requiredProductTypeProperties values('17212','978','Durable','yes');
insert into requiredProductTypeProperties values('17212','978','EnvironmentFriendly','94');
insert into requiredProductTypeProperties values('17212','978','Luster','yes');
insert into requiredProductTypeProperties values('17212','978','Malleability','no');
insert into requiredProductTypeProperties values('17212','978','Ductility','yes');
insert into requiredProductTypeProperties values('17212','978','Writing','yes');
insert into requiredProductTypeProperties values('17212','978','Visibility','90');
insert into requiredProductTypeProperties values('17212','978','Dustless','yes');


insert into requiredProductTypeProperties values('17212','978-01','weight','23.0');
insert into requiredProductTypeProperties values('17212','978-01','dim1','10.2');
insert into requiredProductTypeProperties values('17212','978-01','dim2','7.6');
insert into requiredProductTypeProperties values('17212','978-01','dim3','2.5');
insert into requiredProductTypeProperties values('17212','978-01','color','multi');
insert into requiredProductTypeProperties values('17212','978-01','manufacturer','National');
insert into requiredProductTypeProperties values('17212','978-01','price','140.00');
insert into requiredProductTypeProperties values('17212','978-01','mfgDate','20/07/2014');
insert into requiredProductTypeProperties values('17212','978-01','url','https://www.amazon.in/National-Dustless-Chalk-White-Boxes/dp/B01N2Q6XRT/ref=sr_1_2?ie=UTF8&qid=1508737185&sr=8-2&keywords=chalk+box');
insert into requiredProductTypeProperties values('17212','978-01','chalkMaterial','silt');
insert into requiredProductTypeProperties values('17212','978-01','sizeOfChalk','small');
insert into requiredProductTypeProperties values('17212','978-01','isChalkWaterproof','no');
insert into requiredProductTypeProperties values('17212','978-01','isChalkDurable','yes');
insert into requiredProductTypeProperties values('17212','978-01','isChalkEasyWriting','yes');
insert into requiredProductTypeProperties values('17212','978-01','ChalkFeatures','features of chalk');
insert into requiredProductTypeProperties values('17212','978-01','Build','build features of chalk');
insert into requiredProductTypeProperties values('17212','978-01','ChemicalProperty','chemical property of chalk');
insert into requiredProductTypeProperties values('17212','978-01','Usage','uses of Chalk');
insert into requiredProductTypeProperties values('17212','978-01','Material','yes');
insert into requiredProductTypeProperties values('17212','978-01','Durable','yes');
insert into requiredProductTypeProperties values('17212','978-01','EnvironmentFriendly','95');
insert into requiredProductTypeProperties values('17212','978-01','Luster','yes');
insert into requiredProductTypeProperties values('17212','978-01','Malleability','no');
insert into requiredProductTypeProperties values('17212','978-01','Ductility','yes');
insert into requiredProductTypeProperties values('17212','978-01','Writing','yes');
insert into requiredProductTypeProperties values('17212','978-01','Visibility','100');
insert into requiredProductTypeProperties values('17212','978-01','Dustless','yes');




insert into requiredProductTypeProperties values('17212','978-02','weight','24.0');
insert into requiredProductTypeProperties values('17212','978-02','dim1','10.1');
insert into requiredProductTypeProperties values('17212','978-02','dim2','7.5');
insert into requiredProductTypeProperties values('17212','978-02','dim3','2.4');
insert into requiredProductTypeProperties values('17212','978-02','color','multi');
insert into requiredProductTypeProperties values('17212','978-02','manufacturer','Apsara');
insert into requiredProductTypeProperties values('17212','978-02','price','383.00');
insert into requiredProductTypeProperties values('17212','978-02','mfgDate','10/06/2015');
insert into requiredProductTypeProperties values('17212','978-02','url','https://www.amazon.in/Apsara-Chalk-Stick-White-Pack/dp/B00LQIZRNA/ref=sr_1_1?s=toys&ie=UTF8&qid=1509799819&sr=8-1&keywords=chalk+apsara');
insert into requiredProductTypeProperties values('17212','978-02','chalkMaterial','clay');
insert into requiredProductTypeProperties values('17212','978-02','sizeOfChalk','large');
insert into requiredProductTypeProperties values('17212','978-02','isChalkWaterproof','no');
insert into requiredProductTypeProperties values('17212','978-02','isChalkDurable','yes');
insert into requiredProductTypeProperties values('17212','978-02','isChalkEasyWriting','yes');
insert into requiredProductTypeProperties values('17212','978-02','ChalkFeatures','features of chalk');
insert into requiredProductTypeProperties values('17212','978-02','Build','build features of chalk');
insert into requiredProductTypeProperties values('17212','978-02','ChemicalProperty','chemical property of chalk');
insert into requiredProductTypeProperties values('17212','978-02','Usage','uses of Chalk');
insert into requiredProductTypeProperties values('17212','978-02','Material','yes');
insert into requiredProductTypeProperties values('17212','978-02','Durable','yes');
insert into requiredProductTypeProperties values('17212','978-02','EnvironmentFriendly','92');
insert into requiredProductTypeProperties values('17212','978-02','Luster','yes');
insert into requiredProductTypeProperties values('17212','978-02','Malleability','no');
insert into requiredProductTypeProperties values('17212','978-02','Ductility','yes');
insert into requiredProductTypeProperties values('17212','978-02','Writing','yes');
insert into requiredProductTypeProperties values('17212','978-02','Visibility','99');
insert into requiredProductTypeProperties values('17212','978-02','Dustless','yes');




insert into productTypePropertyTypeValidationTypes values('17212','17212-978-weight','range','float');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-dim1','range','float');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-dim2','range','float');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-dim3','range','float');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-color','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-manufacturer','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-price','range','float');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-mfgDate','list','string');
insert INTO productTypePropertyTypeValidationTypes VALUES('17212','17212-978-url','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-instructionsValRule','container','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-subInstructionsValRule','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-instructions','container','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-subInstructions1','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-subInstructions2','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-subInstructions3','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-subInstructions4','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-subInstructions5','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-subInstructions6','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-subInstructions7','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-subInstructions8','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-chalkMaterial','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-sizeOfChalk','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-isChalkWaterproof','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-isChalkDurable','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-isChalkEasyWriting','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-ChalkFeatures','container','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-Build','container','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-ChemicalProperty','container','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-Usage','container','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-Material','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-Durable','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-EnvironmentFriendly','list','int');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-Luster','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-Malleability','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-Ductility','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-Writing','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-Visibility','list','int');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-Dustless','list','string');

insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-weight','range','float');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-dim1','range','float');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-dim2','range','float');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-dim3','range','float');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-color','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-manufacturer','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-price','range','float');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-mfgDate','list','string');
insert INTO productTypePropertyTypeValidationTypes VALUES('17212','17212-978-01-url','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-chalkMaterial','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-sizeOfChalk','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-isChalkWaterproof','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-isChalkDurable','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-isChalkEasyWriting','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-ChalkFeatures','container','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-Build','container','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-ChemicalProperty','container','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-Usage','container','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-Material','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-Durable','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-EnvironmentFriendly','list','int');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-Luster','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-Malleability','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-Ductility','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-Writing','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-Visibility','list','int');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-01-Dustless','list','string');




insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-weight','range','float');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-dim1','range','float');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-dim2','range','float');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-dim3','range','float');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-color','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-manufacturer','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-price','range','float');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-mfgDate','list','string');
insert INTO productTypePropertyTypeValidationTypes VALUES('17212','17212-978-02-url','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-chalkMaterial','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-sizeOfChalk','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-isChalkWaterproof','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-isChalkDurable','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-isChalkEasyWriting','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-ChalkFeatures','container','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-Build','container','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-ChemicalProperty','container','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-Usage','container','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-Material','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-Durable','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-EnvironmentFriendly','list','int');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-Luster','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-Malleability','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-Ductility','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-Writing','list','string');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-Visibility','list','int');
insert into productTypePropertyTypeValidationTypes values('17212','17212-978-02-Dustless','list','string');


insert into productTypePropertyTypeValueTypes values('17212','978','weight','gm','17212-978-weight');
insert into productTypePropertyTypeValueTypes values('17212','978','dim1','cm','17212-978-dim1');
insert into productTypePropertyTypeValueTypes values('17212','978','dim2','cm','17212-978-dim2');
insert into productTypePropertyTypeValueTypes values('17212','978','dim3','cm','17212-978-dim3');
insert into productTypePropertyTypeValueTypes values('17212','978','color','','17212-978-color');
insert into productTypePropertyTypeValueTypes values('17212','978','manufacturer','','17212-978-manufacturer');
insert into productTypePropertyTypeValueTypes values('17212','978','price','rs','17212-978-price');
insert into productTypePropertyTypeValueTypes values('17212','978','mfgDate','','17212-978-mfgDate');
insert into productTypePropertyTypeValueTypes values('17212','978','url','','17212-978-url');
insert into productTypePropertyTypeValueTypes values('17212','978','instructions','','17212-978-instructions');
insert into productTypePropertyTypeValueTypes values('17212','978','subInstruction1','','17212-978-subInstruction1');
insert into productTypePropertyTypeValueTypes values('17212','978','subInstruction2','','17212-978-subInstruction2');
insert into productTypePropertyTypeValueTypes values('17212','978','subInstruction3','','17212-978-subInstruction3');
insert into productTypePropertyTypeValueTypes values('17212','978','subInstruction4','','17212-978-subInstruction4');
insert into productTypePropertyTypeValueTypes values('17212','978','subInstruction5','','17212-978-subInstruction5');
insert into productTypePropertyTypeValueTypes values('17212','978','subInstruction6','','17212-978-subInstruction6');
insert into productTypePropertyTypeValueTypes values('17212','978','subInstruction7','','17212-978-subInstruction7');
insert into productTypePropertyTypeValueTypes values('17212','978','subInstruction8','','17212-978-subInstruction8');
insert into productTypePropertyTypeValueTypes values('17212','978','chalkMaterial','','17212-978-chalkMaterial');
insert into productTypePropertyTypeValueTypes values('17212','978','sizeOfChalk','','17212-978-sizeOfChalk');
insert into productTypePropertyTypeValueTypes values('17212','978','isChalkWaterproof','','17212-978-isChalkWaterproof');
insert into productTypePropertyTypeValueTypes values('17212','978','isChalkDurable','','17212-978-isChalkDurable');
insert into productTypePropertyTypeValueTypes values('17212','978','isChalkEasyWriting','','17212-978-isChalkEasyWriting');
insert into productTypePropertyTypeValueTypes values('17212','978','ChalkFeatures','','17212-978-ChalkFeatures');
insert into productTypePropertyTypeValueTypes values('17212','978','Build','','17212-978-Build');
insert into productTypePropertyTypeValueTypes values('17212','978','ChemicalProperty','','17212-978-ChemicalProperty');
insert into productTypePropertyTypeValueTypes values('17212','978','Usage','','17212-978-Usage');
insert into productTypePropertyTypeValueTypes values('17212','978','Material','','17212-978-Material');
insert into productTypePropertyTypeValueTypes values('17212','978','Durable','','17212-978-Durable');
insert into productTypePropertyTypeValueTypes values('17212','978','EnvironmentFriendly','','17212-978-EnvironmentFriendly');
insert into productTypePropertyTypeValueTypes values('17212','978','Luster','','17212-978-Luster');
insert into productTypePropertyTypeValueTypes values('17212','978','Malleability','','17212-978-Malleability');
insert into productTypePropertyTypeValueTypes values('17212','978','Ductility','','17212-978-Ductility');
insert into productTypePropertyTypeValueTypes values('17212','978','Writing','','17212-978-Writing');
insert into productTypePropertyTypeValueTypes values('17212','978','Visibility','','17212-978-Visibility');
insert into productTypePropertyTypeValueTypes values('17212','978','Dustless','','17212-978-Dustless');


insert into productTypePropertyTypeValueTypes values('17212','978-01','weight','gm','17212-978-01-weight');
insert into productTypePropertyTypeValueTypes values('17212','978-01','dim1','cm','17212-978-01-dim1');
insert into productTypePropertyTypeValueTypes values('17212','978-01','dim2','cm','17212-978-01-dim2');
insert into productTypePropertyTypeValueTypes values('17212','978-01','dim3','cm','17212-978-01-dim3');
insert into productTypePropertyTypeValueTypes values('17212','978-01','color','','17212-978-01-color');
insert into productTypePropertyTypeValueTypes values('17212','978-01','manufacturer','','17212-978-01-manufacturer');
insert into productTypePropertyTypeValueTypes values('17212','978-01','price','rs','17212-978-01-price');
insert into productTypePropertyTypeValueTypes values('17212','978-01','mfgDate','','17212-978-01-mfgDate');
insert into productTypePropertyTypeValueTypes values('17212','978-01','url','','17212-978-01-url');
insert into productTypePropertyTypeValueTypes values('17212','978-01','chalkMaterial','','17212-978-01-chalkMaterial');
insert into productTypePropertyTypeValueTypes values('17212','978-01','sizeOfChalk','','17212-978-01-sizeOfChalk');
insert into productTypePropertyTypeValueTypes values('17212','978-01','isChalkWaterproof','','17212-978-01-isChalkWaterproof');
insert into productTypePropertyTypeValueTypes values('17212','978-01','isChalkDurable','','17212-978-01-isChalkDurable');
insert into productTypePropertyTypeValueTypes values('17212','978-01','isChalkEasyWriting','','17212-978-01-isChalkEasyWriting');
insert into productTypePropertyTypeValueTypes values('17212','978-01','ChalkFeatures','','17212-978-01-ChalkFeatures');
insert into productTypePropertyTypeValueTypes values('17212','978-01','Build','','17212-978-01-Build');
insert into productTypePropertyTypeValueTypes values('17212','978-01','ChemicalProperty','','17212-978-01-ChemicalProperty');
insert into productTypePropertyTypeValueTypes values('17212','978-01','Usage','','17212-978-01-Usage');
insert into productTypePropertyTypeValueTypes values('17212','978-01','Material','','17212-978-01-Material');
insert into productTypePropertyTypeValueTypes values('17212','978-01','Durable','','17212-978-01-Durable');
insert into productTypePropertyTypeValueTypes values('17212','978-01','EnvironmentFriendly','','17212-978-01-EnvironmentFriendly');
insert into productTypePropertyTypeValueTypes values('17212','978-01','Luster','','17212-978-01-Luster');
insert into productTypePropertyTypeValueTypes values('17212','978-01','Malleability','','17212-978-01-Malleability');
insert into productTypePropertyTypeValueTypes values('17212','978-01','Ductility','','17212-978-01-Ductility');
insert into productTypePropertyTypeValueTypes values('17212','978-01','Writing','','17212-978-01-Writing');
insert into productTypePropertyTypeValueTypes values('17212','978-01','Visibility','','17212-978-01-Visibility');
insert into productTypePropertyTypeValueTypes values('17212','978-01','Dustless','','17212-978-01-Dustless');

insert into productTypePropertyTypeValueTypes values('17212','978-02','weight','gm','17212-978-02-weight');
insert into productTypePropertyTypeValueTypes values('17212','978-02','dim1','cm','17212-978-02-dim1');
insert into productTypePropertyTypeValueTypes values('17212','978-02','dim2','cm','17212-978-02-dim2');
insert into productTypePropertyTypeValueTypes values('17212','978-02','dim3','cm','17212-978-02-dim3');
insert into productTypePropertyTypeValueTypes values('17212','978-02','color','','17212-978-02-color');
insert into productTypePropertyTypeValueTypes values('17212','978-02','manufacturer','','17212-978-02-manufacturer');
insert into productTypePropertyTypeValueTypes values('17212','978-02','price','rs','17212-978-02-price');
insert into productTypePropertyTypeValueTypes values('17212','978-02','mfgDate','','17212-978-02-mfgDate');
insert into productTypePropertyTypeValueTypes values('17212','978-02','url','','17212-978-02-url');
insert into productTypePropertyTypeValueTypes values('17212','978-02','chalkMaterial','','17212-978-02-chalkMaterial');
insert into productTypePropertyTypeValueTypes values('17212','978-02','sizeOfChalk','','17212-978-02-sizeOfChalk');
insert into productTypePropertyTypeValueTypes values('17212','978-02','isChalkWaterproof','','17212-978-02-isChalkWaterproof');
insert into productTypePropertyTypeValueTypes values('17212','978-02','isChalkDurable','','17212-978-02-isChalkDurable');
insert into productTypePropertyTypeValueTypes values('17212','978-02','isChalkEasyWriting','','17212-978-02-isChalkEasyWriting');
insert into productTypePropertyTypeValueTypes values('17212','978-02','ChalkFeatures','','17212-978-02-ChalkFeatures');
insert into productTypePropertyTypeValueTypes values('17212','978-02','Build','','17212-978-02-Build');
insert into productTypePropertyTypeValueTypes values('17212','978-02','ChemicalProperty','','17212-978-02-ChemicalProperty');
insert into productTypePropertyTypeValueTypes values('17212','978-02','Usage','','17212-978-02-Usage');
insert into productTypePropertyTypeValueTypes values('17212','978-02','Material','','17212-978-02-Material');
insert into productTypePropertyTypeValueTypes values('17212','978-02','Durable','','17212-978-02-Durable');
insert into productTypePropertyTypeValueTypes values('17212','978-02','EnvironmentFriendly','','17212-978-02-EnvironmentFriendly');
insert into productTypePropertyTypeValueTypes values('17212','978-02','Luster','','17212-978-02-Luster');
insert into productTypePropertyTypeValueTypes values('17212','978-02','Malleability','','17212-978-02-Malleability');
insert into productTypePropertyTypeValueTypes values('17212','978-02','Ductility','','17212-978-02-Ductility');
insert into productTypePropertyTypeValueTypes values('17212','978-02','Writing','','17212-978-02-Writing');
insert into productTypePropertyTypeValueTypes values('17212','978-02','Visibility','','17212-978-02-Visibility');
insert into productTypePropertyTypeValueTypes values('17212','978-02','Dustless','','17212-978-02-Dustless');





insert into productTypePropertyTypeValidationValues values('17212','17212-978-weight','1','20.0');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-dim1','1','10.2');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-dim2','1','7.2');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-dim3','1','2.6');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-color','1','multi');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-manufacturer','1','oddy');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-price','1','142.0');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-mfgDate','1','24/10/2017');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-url','1','https://prezi.com/mdyglkqmpfs3/physical-and-chemical-properties-of-chalk/');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-instructions','1','Chalk instructions');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-subInstruction1','1','Never use small broken Chalk');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-subInstruction2','1','Should keep away from kids');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-subInstruction3','1','Do not immerse in water');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-subInstruction4','1','Fragile so handle with care');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-subInstruction5','1','keep away from skin contact');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-subInstruction6','1','Do not inhale');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-subInstruction7','1','keep away from eye contact');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-subInstruction8','1','Avoid ingestion');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-chalkMaterial','1','calcium carbonate');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-sizeOfChalk','1','medium');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-isChalkWaterproof','1','no');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-isChalkDurable','1','yes');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-isChalkEasyWriting','1','yes');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-chalkFeatures','1','features of chalk');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-Build','1','build features of chalk');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-ChemicalProperty','1','chemical property of chalk ');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-Usage','1','uses of Chalk');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-Material','1','yes');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-Durable','1','yes');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-EnvironmentFriendly','1','94');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-Luster','1','yes');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-Malleability','1','no');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-Ductility','1','yes');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-Writing','1','yes');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-Visibility','1','90');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-Dustless','1','yes');


insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-weight','1','23.0');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-dim1','1','10.2');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-dim2','1','7.6');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-dim3','1','2.5');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-color','1','multi');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-manufacturer','1','National');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-price','1','140.00');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-mfgDate','1','20/07/2014');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-url','1','https://www.amazon.in/National-Dustless-Chalk-White-Boxes/dp/B01N2Q6XRT/ref=sr_1_2?ie=UTF8&qid=1508737185&sr=8-2&keywords=chalk+box');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-chalkMaterial','1','silt');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-sizeOfChalk','1','small');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-isChalkWaterproof','1','no');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-isChalkDurable','1','yes');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-isChalkEasyWriting','1','yes');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-chalkFeatures','1','features of chalk');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-Build','1','build features of chalk');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-ChemicalProperty','1','chemical property of chalk ');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-Usage','1','uses of Chalk');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-Material','1','yes');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-Durable','1','yes');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-EnvironmentFriendly','1','95');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-Luster','1','yes');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-Malleability','1','no');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-Ductility','1','yes');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-Writing','1','yes');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-Visibility','1','100');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-01-Dustless','1','yes');


insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-weight','1','24.0');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-dim1','1','10.1');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-dim2','1','7.5');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-dim3','1','2.4');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-color','1','multi');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-manufacturer','1','Apsara'); 
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-price','1','383.00');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-mfgDate','1','10/06/2015');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-url','1','https://www.amazon.in/Apsara-Chalk-Stick-White-Pack/dp/B00LQIZRNA/ref=sr_1_1?s=toys&ie=UTF8&qid=1509799819&sr=8-1&keywords=chalk+apsara');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-chalkMaterial','1','clay');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-sizeOfChalk','1','large');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-isChalkWaterproof','1','no');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-isChalkDurable','1','yes');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-isChalkEasyWriting','1','yes');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-chalkFeatures','1','features of chalk');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-Build','1','build features of chalk');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-ChemicalProperty','1','chemical property of chalk ');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-Usage','1','uses of Chalk');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-Material','1','yes');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-Durable','1','yes');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-EnvironmentFriendly','1','92');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-Luster','1','yes');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-Malleability','1','no');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-Ductility','1','yes');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-Writing','1','yes');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-Visibility','1','99');
insert into productTypePropertyTypeValidationValues values('17212','17212-978-02-Dustless','1','yes');


