INSERT INTO productTypes VALUES ('17113','187','shoes','typeUrl:https://en.wikipedia.org/wiki/Shoe');
INSERT INTO productTypes VALUES ('17113','187-01','Foot-N-StyleFormalShoes','typeUrl:https://www.naaptol.com/formal-shoes/foot-n-style-formal-shoes-for-men-fs3196-brown/p/12605620.html?ntzoneid=9266&nts=Footer_Recent_View&ntz=Footer_Recent_View');
INSERT INTO productTypes VALUES ('17113','187-02','AlphaBOUNCE Running Shoe','typeUrl:https://www.shoes.com/adidas-alphabounce-running-shoe/792701/1751719?cm_vc=bn_searchresults_carou_loc1');

INSERT INTO productTypeHierarchy VALUES ('17113','187','187-01');
INSERT INTO productTypeHierarchy VALUES ('17113','187','187-02');

INSERT INTO productTypePropertyTypes VALUES('17113','shoes_Material-1001','shoes Material use to construct shoes');
INSERT INTO productTypePropertyTypes VALUES('17113','lyfestyle-1003','lyfestyle is purepose of useing shoes office or sports');
INSERT INTO productTypePropertyTypes VALUES('17113','shoes_size1004','define the size of shoes');
INSERT INTO productTypePropertyTypes VALUES('17113','shoes_waterproof-1005','shoes is waterproof (yes/no)');
INSERT INTO productTypePropertyTypes VALUES('17113','ANSI-1006','Electrick Resistant protect from electricity shock');
INSERT INTO productTypePropertyTypes VALUES('17113','shoes_feature-1007','define a shoe feature ');
INSERT INTO productTypePropertyTypes VALUES('17113','protection-1008','protection');
INSERT INTO productTypePropertyTypes VALUES('17113','achilles_Tendonites-1009','achilles Tendonites is tendon connecting calf muscles to the heel');

INSERT INTO productTypePropertyTypes VALUES('17113','inner_sole-1010-1','innner Sole');
INSERT INTO productTypePropertyTypes VALUES('17113','outer_sole-1010-2','Outer Sole should be');  
INSERT INTO productTypePropertyTypes VALUES('17113','soleOfShoes-1010-01','lug sole is protect from bouncy reagen');
INSERT INTO productTypePropertyTypes VALUES('17113','sefety_Toe-1045','sefety provide to Toe ');
INSERT INTO productTypePropertyTypes VALUES('17113','slip_resistant-1015','slip Resistant in slip land');
INSERT INTO productTypePropertyTypes VALUES('17113','anti_microbial-1016','Anti-Microbial');
INSERT INTO productTypePropertyTypes VALUES('17113','flexibale-1020','Flexible (yes/no)');
INSERT INTO productTypePropertyTypes VALUES('17113','quick_dry-1002','Quick Dry from water');
INSERT INTO productTypePropertyTypes VALUES('17113','ventilated-1033','Ventilated is free pass to error');
INSERT INTO productTypePropertyTypes VALUES('17113','washable-1013','Washable (yes/not)');
INSERT INTO productTypePropertyTypes VALUES('17113','synthetic-1028','synthetic is different material mixing');      
INSERT INTO productTypePropertyTypes VALUES('17113','Stain_resistant-1032','Stain Resistant is protect from mud stains on my shoes');
INSERT INTO productTypePropertyTypes VALUES('17113','closer_type-1011','less is used to fixed the shoe (yes/no)');
INSERT INTO productTypePropertyTypes VALUES('17113','shoe_burn-1012','shoe_burn (yes/no)');
INSERT INTO productTypePropertyTypes VALUES('17113','shoes_is_sparkable-1014','shoes_is_sparkable (yes/no)');
INSERT INTO productTypePropertyTypes VALUES('17113','shoes_manufacturer-1018','shoes manufacturer details');
INSERT INTO productTypePropertyTypes VALUES('17113','top_angle_of_shoes-1019','top_angle_of_shoes (angular/rounded)');
INSERT INTO productTypePropertyTypes VALUES('17113','heel-1021','heel is present or not');
INSERT INTO productTypePropertyTypes VALUES('17113','welt-1022','welt of shoes');
INSERT INTO productTypePropertyTypes VALUES('17113','heelcape-1023','heelcape is security of cape');
INSERT INTO productTypePropertyTypes VALUES('17113','heel_grip-1024','heel grip used to prevent the shoe from slipping on the heel if the fit is not perfect');	 	
INSERT INTO productTypePropertyTypes VALUES('17113','shoe_polish-1026','shining of shoes');
INSERT INTO productTypePropertyTypes VALUES('17113','protectFromHeat-1027','protect from hot land to plantar');
INSERT INTO productTypePropertyTypes VALUES('17113','protectFromWater-1028','protect in rain sesson');
INSERT INTO productTypePropertyTypes VALUES('17113','protectFromInjuries-1029','corner stone not totear to sole');
INSERT INTO productTypePropertyTypes VALUES('17113','walking-1030','walking');
INSERT INTO productTypePropertyTypes VALUES('17113','running-1031','running');
INSERT INTO productTypePropertyTypes VALUES('17113','warnty-1034','warnty gives by manufacturer');
INSERT INTO productTypePropertyTypes VALUES('17113','protect_to_planter-1041','protect to planter');
INSERT INTO productTypePropertyTypes VALUES('17113','protctFromHeat-1042','protct rom eat');
INSERT INTO productTypePropertyTypes VALUES('17113','protect_heelcap_and_lanter-1043','protect heelcap and planter');
INSERT INTO productTypePropertyTypes VALUES('17113','subInstruction-1046','sub instruction');
INSERT INTO productTypePropertyTypes VALUES('17113','subInstruction-1047','sub instruction second');
INSERT INTO productTypePropertyTypes VALUES('17113','insole-1048','in sole of inner shoe sole');
INSERT INTO productTypePropertyTypes VALUES('17113','midsole-1050','in sole of middle shoe sole');
INSERT INTO productTypePropertyTypes VALUES('17113','wedge-1051','');
INSERT INTO productTypePropertyTypes VALUES('17113','counter_sheet-1052','resistant to the twist and Terns,water resistance');
INSERT INTO productTypePropertyTypes VALUES('17113','leather-1053','');
INSERT INTO productTypePropertyTypes VALUES('17113','fabric-1054','');
INSERT INTO productTypePropertyTypes VALUES('17113','pvc-1055','');
INSERT INTO productTypePropertyTypes VALUES('17113','metalic-1056','');
INSERT INTO productTypePropertyTypes VALUES('17113','vamp-1057','');
INSERT INTO productTypePropertyTypes VALUES('17113','tongue-1058','');
INSERT INTO productTypePropertyTypes VALUES('17113','heel_counter-1059','');
INSERT INTO productTypePropertyTypes VALUES('17113','back_stey-1060','');
INSERT INTO productTypePropertyTypes VALUES('17113','quarters-1061','');
INSERT INTO productTypePropertyTypes VALUES('17113','toeOfShoes-1062','');
INSERT INTO productTypePropertyTypes VALUES('17113','hardToePuff-1063','');
INSERT INTO productTypePropertyTypes VALUES('17113','vamp-1064','');
INSERT INTO productTypePropertyTypes VALUES('17113','upperPart-1049','how made upper part of shoes');
INSERT INTO productTypePropertyTypes VALUES('17113','white','');
INSERT INTO productTypePropertyTypes VALUES('17113','black','');
INSERT INTO productTypePropertyTypes VALUES('17113','blue','');
INSERT INTO productTypePropertyTypes VALUES('17113','pink','');


INSERT INTO productTypePropertyTypeContainer VALUES('17113','shoes_feature-1007','inner_sole-1010-1','1');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','shoes_feature-1007','outer_sole-1010-2','2');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','shoes_feature-1007','shoes_Material-1001','3');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','shoes_feature-1007','upperPart-1049','4');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','shoes_feature-1007','color','5');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','color','white','1');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','color','black','2');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','color','blue','3');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','color','pink','4');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','inner_sole-1010-1','insole-1048','1');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','inner_sole-1010-1','midsole-1050','2');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','inner_sole-1010-1','wedge-1051','3'); 
INSERT INTO productTypePropertyTypeContainer VALUES('17113','outer_sole-1010-2','counter_sheet-1052','1');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','shoes_Material-1001','synthetic-1028','1');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','synthetic-1028','leather-1053','1');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','synthetic-1028','fabric-1054','2');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','synthetic-1028','pvc-1055','3');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','synthetic-1028','metalic-1056','4');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','upperPart-1049','vamp-1057','1');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','upperPart-1049','tongue-1058','2');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','upperPart-1049','heel_counter-1059','3');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','upperPart-1049','back_stey-1060','4');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','upperPart-1049','quarters-1061','5');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','upperPart-1049','toeOfShoes-1062','6');
INSERT INTO productTypePropertyTypeContainer VALUES('17113','upperPart-1049','hardToePuff-1063','7');


INSERT INTO requiredProductTypeProperties VALUES ('17113','187','shoes_Material-1001','rubber');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','weight','1.0');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','dim1','38.0');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','dim2','25.0');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','dim3','10.0');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','shoes_size1004','6');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','closer_type-1011','less');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','color','white');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','warnty-1034','0');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','instructions','Avoid liquid shoe polishes and silicone sprays');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','price','100.00');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','mfgDate','01/12/1850');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','ANSI-1006','No');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','washable-1013','Yes');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','manufacturer','adidas');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','lyfestyle-1003','office');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','inner_sole-1010-1','rubber'); 
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','outer_sole-1010-2','TPR');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','slip_resistant-1015','Yes'); 
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','anti_microbial-1016','No'); 
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','flexibale-1020','Yes');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','quick_dry-1002','Yes');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','ventilated-1033','NO');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','synthetic-1028','Yes');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','heelcape-1023','Yes');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','heel_grip-1024','Yes');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','shoes_Material-1001','Leather');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','weight','1.5');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','dim1','35.0');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','dim2','29.0');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','dim3','15.0');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','shoes_size1004','6');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','closer_type-1011','Bucklet');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','color','Black');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','warnty-1034','3');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','instructions','Avoid liquid shoe polishes and silicone sprays');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','price','499.00');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','mfgDate','10/10/2016');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','url','https://www.amazon.in/Foot-Style-Black-Leather-Formal/dp/B011KHVOOC/ref=sr_1_fkmr0_3?s=shoes&ie=UTF8&qid=1509818435&sr=1-3-fkmr0&nodeID=1983572031&psd=1&keywords=Foot+n+Style+Formal+Shoes+For+Men_FS3196+-+Brown');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','ANSI-1006','No');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','washable-1013','Yes');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','manufacturer','Foot N style Delhi');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','lyfestyle-1003','office');

INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','white','white');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','black','black');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','blue','blue');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','pink','pink');

INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','shoes_feature-1007','run');

INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','insole-1048','insole'); 
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','midsole-1050','midsole'); 
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','wedge-1051','wedge'); 
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','counter_sheet-1052','counter_sheet'); 
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','leather-1053','leather');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','fabric-1054','fabric');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','pvc-1055','pvc');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','metalic-1056','metalic');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','upperPart-1049','upper part');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','vamp-1057','vamp');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','tongue-1058','tongue');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','heel_counter-1059','heel_counter');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','back_stey-1060','back_stey');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','quarters-1061','quarters');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','toeOfShoes-1062','toeOfShoes');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-01','hardToePuff-1063','hardToePuff');






INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','shoes_Material-1001','rubber');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','weight','0.5');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','dim1','26.0');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','dim2','18.0');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','dim3','10.0');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','shoes_size1004','5');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','closer_type-1011','less');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','color','blue');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','warnty-1034','1');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','instructions','Never use a wire brush or polish on suede or nubuck');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','price','3999.00');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','mfgDate','10/10/2010');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','url','https://www.naaptol.com/sports-shoes/adidas-alphabounce-men-shoes-alpha-blue/p/12604709.html?ntpromoid=8092&utm_source=GOOGLE&utm_medium=Search&utm_campaign=PLA_G&utm_code=PLA&ntpromoid=8092&utm_source=GOOGLE&utm_medium=Search&utm_campaign=PLA_G&utm_code=PLA&gclid=Cj0KCQjwyvXPBRD-ARIsAIeQeoGLCrhWVsC7Br-L4gcnvjBUWvfle4hSZklonUNEhp2ybu5DM8khtGIaAqqzEALw_wcB');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','ANSI-1006','Yes');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','washable-1013','Yes');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','manufacturer','Adidas');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','lyfestyle-1003','sport');

INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','shoes_feature-1007','run');

INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','insole-1048','insole'); 
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','midsole-1050','midsole'); 
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','wedge-1051','wedge'); 
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','counter_sheet-1052','counter_sheet'); 
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','leather-1053','leather');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','fabric-1054','fabric');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','pvc-1055','pvc');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','metalic-1056','metalic');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','upperPart-1049','upper part');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','vamp-1057','vamp');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','tongue-1058','tongue');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','heel_counter-1059','heel_counter');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','back_stey-1060','back_stey');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','quarters-1061','quarters');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','toeOfShoes-1062','toeOfShoes');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187-02','hardToePuff-1063','hardToePuff');


INSERT INTO requiredProductTypeProperties VALUES ('17113','187','white','white');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','black','black');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','blue','blue');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','pink','pink');

INSERT INTO requiredProductTypeProperties VALUES ('17113','187','shoes_feature-1007','run');

INSERT INTO requiredProductTypeProperties VALUES ('17113','187','insole-1048','insole'); 
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','midsole-1050','midsole'); 
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','wedge-1051','wedge'); 
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','counter_sheet-1052','counter_sheet'); 
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','leather-1053','leather');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','fabric-1054','fabric');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','pvc-1055','pvc');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','metalic-1056','metalic');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','upperPart-1049','upper part');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','vamp-1057','vamp');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','tongue-1058','tongue');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','heel_counter-1059','heel_counter');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','back_stey-1060','back_stey');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','quarters-1061','quarters');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','toeOfShoes-1062','toeOfShoes');
INSERT INTO requiredProductTypeProperties VALUES ('17113','187','hardToePuff-1063','hardToePuff');





                                                                 
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-shoes_Material-1001','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-weight','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-dim1','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-dim2','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-dim3','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-shoes_size1004','list','int');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-closer_type-1011','range','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-warnty-1034','range','int');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-instructions','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-price','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-mfgDate','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-ANSI-1006','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-washable-1013','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-manufacturer','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-lyfestyle-1003','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-inner_sole-1010-1','container','string'); 
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-outer_sole-1010-2','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-slip_resistant-1015','list','string'); 
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-anti_microbial-1016','list','string'); 
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-flexibale-1020','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-quick_dry-1002','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-ventilated-1033','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-synthetic-1028','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-heelcape-1023','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-heel_grip-1024','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-shoes_feature-1007','container','string');

INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-upperPart-1049','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-leather-1053','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-fabric-1054','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-pvc-1055','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-vamp-1057','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-tongue-1058','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-heel_counter-1059','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-quarters-1061','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-toeOfShoes-1062','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-back_stey-1060','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-hardToePuff-1063','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-insole-1048','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-midsole-1050','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-wedge-1051','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-counter_sheet-1052','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-metalic-1056','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-color','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-white','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-black','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-blue','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-pink','list','string');


INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-shoes_Material-1001','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-weight','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-dim1','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-dim2','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-dim3','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-shoes_size1004','list','int');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-closer_type-1011','range','string');

INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-warnty-1034','range','int');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-instructions','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-price','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-mfgDate','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-url','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-ANSI-1006','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-washable-1013','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-manufacturer','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-lyfestyle-1003','list','string');

INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-upperPart-1049','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-leather-1053','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-fabric-1054','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-pvc-1055','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-vamp-1057','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-tongue-1058','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-heel_counter-1059','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-quarters-1061','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-toeOfShoes-1062','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-back_stey-1060','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-hardToePuff-1063','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-insole-1048','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-midsole-1050','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-wedge-1051','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-counter_sheet-1052','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-metalic-1056','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-color','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-white','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-black','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-blue','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-01-pink','list','string');

INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-shoes_Material-1001','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-weight','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-dim1','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-dim2','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-dim3','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-shoes_size1004','list','int');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-closer_type-1011','range','string');

INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-warnty-1034','range','int');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-instructions','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-price','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-mfgDate','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-url','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-ANSI-1006','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-washable-1013','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-manufacturer','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-lyfestyle-1003','list','string');

INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-upperPart-1049','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-leather-1053','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-fabric-1054','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-pvc-1055','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-vamp-1057','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-tongue-1058','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-heel_counter-1059','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-quarters-1061','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-toeOfShoes-1062','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-back_stey-1060','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-hardToePuff-1063','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-insole-1048','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-midsole-1050','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-wedge-1051','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-counter_sheet-1052','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-metalic-1056','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-color','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-white','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-black','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-blue','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17113','17113-187-02-pink','list','string');

INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','shoes_Material-1001','','17113-187-shoes_Material-1001');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','weight','kg','17113-187-weight');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','dim1','cm','17113-187-dim1');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','dim2','cm','17113-187-dim2');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','dim3','cm','17113-187-dim3');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','shoes_size1004','UK','17113-187-shoes_size1004');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','closer_type-1011','','17113-187-closer_type-1011');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','color','','17113-187-color');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','warnty-1034','month','17113-187-warnty-1034');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','instructions','','17113-187-instructions');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','price','Rs','17113-187-price');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','mfgDate','','17113-187-mfgDate');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','ANSI-1006','','17113-187-ANSI-1006');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','washable-1013','','17113-187-washable-1013');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','manufacturer','','17113-187-manufacturer');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','lyfestyle-1003','','17113-187-lyfestyle-1003');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','inner_sole-1010-1','','17113-187-inner_sole-1010-1'); 
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','outer_sole-1010-2','','17113-187-outer_sole-1010-2');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','slip_resistant-1015','','17113-187-slip_resistant-1015'); 
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','anti_microbial-1016','','17113-187-anti_microbial-1016'); 
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','flexibale-1020','','17113-187-flexibale-1020');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','quick_dry-1002','','17113-187-quick_dry-1002');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','ventilated-1033','','17113-187-ventilated-1033');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','synthetic-1028','','17113-187-synthetic-1028');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','heelcape-1023','','17113-187-heelcape-1023');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','heel_grip-1024','','17113-187-heel_grip-1024');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','shoes_feature-1007','','17113-187-shoes_feature-1007');

INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','upperPart-1049','','17113-187-upperPart-1049');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','leather-1053','','17113-187-leather-1053');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','fabric-1054','','17113-187-fabric-1054');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','pvc-1055','','17113-187-pvc-1055');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','vamp-1057','','17113-187-vamp-1057');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','tongue-1058','','17113-187-tongue-1058');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','heel_counter-1059','','17113-187-heel_counter-1059');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','back_stey-1060','','17113-187-back_stey-1060');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','quarters-1061','','17113-187-quarters-1061');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','toeOfShoes-1062','','17113-187-toeOfShoes-1062');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','hardToePuff-1063','','17113-187-hardToePuff-1063');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','insole-1048','','17113-187-insole-1048');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','midsole-1050','','17113-187-midsole-1050');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','wedge-1051','','17113-187-wedge-1051');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','counter_sheet-1052','','17113-187-counter_sheet-1052');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','metalic-1056','','17113-187-metalic-1056');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','white','','17113-187-white');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','black','','17113-187-black');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','blue','','17113-187-blue');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187','pink','','17113-187-pink');



INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','shoes_Material-1001','','17113-187-01-shoes_Material-1001');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','weight','kg','17113-187-01-weight');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','dim1','cm','17113-187-01-dim1');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','dim2','cm','17113-187-01-dim2');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','dim3','cm','17113-187-01-dim3');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','shoes_size1004','UK','17113-187-01-shoes_size1004');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','closer_type-1011','','17113-187-01-closer_type-1011');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','color','','17113-187-01-color');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','warnty-1034','month','17113-187-01-warnty-1034');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','instructions','','17113-187-01-instructions');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','price','Rs','17113-187-01-price');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','mfgDate','','17113-187-01-mfgDate');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','url','','17113-187-01-url');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','ANSI-1006','','17113-187-01-ANSI-1006');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','washable-1013','','17113-187-01-washable-1013');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','manufacturer','','17113-187-01-manufacturer');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','lyfestyle-1003','','17113-187-01-lyfestyle-1003');

INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','upperPart-1049','','17113-187-01-upperPart-1049');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','leather-1053','','17113-187-01-leather-1053');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','fabric-1054','','17113-187-01-fabric-1054');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','pvc-1055','','17113-187-01-pvc-1055');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','vamp-1057','','17113-187-01-vamp-1057');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','tongue-1058','','17113-187-01-tongue-1058');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','heel_counter-1059','','17113-187-01-heel_counter-1059');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','back_stey-1060','','17113-187-01-back_stey-1060');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','quarters-1061','','17113-187-01-quarters-1061');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','toeOfShoes-1062','','17113-187-01-toeOfShoes-1062');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','hardToePuff-1063','','17113-187-01-hardToePuff-1063');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','insole-1048','','17113-187-01-insole-1048');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','midsole-1050','','17113-187-01-midsole-1050');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','wedge-1051','','17113-187-01-wedge-1051');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','counter_sheet-1052','','17113-187-01-counter_sheet-1052');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','metalic-1056','','17113-187-01-metalic-1056');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','white','','17113-187-01-white');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','black','','17113-187-01-black');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','blue','','17113-187-01-blue');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-01','pink','','17113-187-01-pink');

INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','shoes_Material-1001','','17113-187-02-shoes_Material-1001');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','weight','kg','17113-187-02-weight');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','dim1','cm','17113-187-02-dim1');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','dim2','cm','17113-187-02-dim2');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','dim3','cm','17113-187-02-dim3');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','shoes_size1004','UK','17113-187-02-shoes_size1004');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','closer_type-1011','','17113-187-02-closer_type-1011');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','color','','17113-187-02-color');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','warnty-1034','month','17113-187-02-warnty-1034');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','instructions','','17113-187-02-instructions');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','price','Rs','17113-187-02-price');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','mfgDate','','17113-187-02-mfgDate');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','url','','17113-187-02-url');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','ANSI-1006','','17113-187-02-ANSI-1006');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','washable-1013','','17113-187-02-washable-1013');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','manufacturer','','17113-187-02-manufacturer');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','lyfestyle-1003','','17113-187-02-lyfestyle-1003');

INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','upperPart-1049','','17113-187-02-upperPart-1049');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','leather-1053','','17113-187-02-leather-1053');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','fabric-1054','','17113-187-02-fabric-1054');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','pvc-1055','','17113-187-02-pvc-1055');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','vamp-1057','','17113-187-02-vamp-1057');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','tongue-1058','','17113-187-02-tongue-1058');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','heel_counter-1059','','17113-187-02-heel_counter-1059');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','back_stey-1060','','17113-187-02-back_stey-1060');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','quarters-1061','','17113-187-02-quarters-1061');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','toeOfShoes-1062','','17113-187-02-toeOfShoes-1062');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','hardToePuff-1063','','17113-187-02-hardToePuff-1063');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','insole-1048','','17113-187-02-insole-1048');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','midsole-1050','','17113-187-02-midsole-1050');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','wedge-1051','','17113-187-02-wedge-1051');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','counter_sheet-1052','','17113-187-02-counter_sheet-1052');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','metalic-1056','','17113-187-02-metalic-1056');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','white','','17113-187-02-white');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','black','','17113-187-02-black');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','blue','','17113-187-02-blue');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17113','187-02','pink','','17113-187-02-pink');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-shoes_Material-1001','1','rubber');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-shoes_Material-1001','2','neolite');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-shoes_Material-1001','3','nailsWithFoilPpaper');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-shoes_Material-1001','4','TPR Material');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-shoes_Material-1001','5','Rexine');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-shoes_Material-1001','6','PVC Sole shoe');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-shoes_Material-1001','7','Leather');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-lyfestyle-1003','1','office');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-lyfestyle-1003','2','sports');


INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-inner_sole-1010-1','1','rubber'); 
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-outer_sole-1010-2','1','TPR');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-slip_resistant-1015','1','Yes'); 

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-anti_microbial-1016','1','No'); 

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-flexibale-1020','1','Yes');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-quick_dry-1002','1','Yes');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-ventilated-1033','1','NO');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-synthetic-1028','1','Yes');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-synthetic-1028','2','No');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-heelcape-1023','1','Yes');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-heel_grip-1024','1','Yes');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-weight','1','1.0');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-weight','2','2.0');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-dim1','1','30.0');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-dim1','2','35.0');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-dim2','1','25.0');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-dim2','2','28.0');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-dim3','1','15');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-dim3','2','12.0');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-shoes_size1004','1','6');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-shoes_size1004','2','9');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-closer_type-1011','1','less');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-closer_type-1011','2','Buckle');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-color','1','white');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-color','2','black');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-color','3','pink');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-color','4','grey');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-color','5','red');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-color','6','blue');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-warnty-1034','1','0');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-warnty-1034','2','12'); 
     
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-ANSI-1006','1','No'); 
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-ANSI-1006','2','Yes'); 

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-washable-1013','1','Yes'); 
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-washable-1013','2','No'); 

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-instructions','1','Avoid liquid shoe polishes and silicone sprays');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-instructions','2',' Use a soft damp cloth and a drop of mild soap to remove any haze.');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-instructions','3','Follow up with a silicone spray for water and stain resistance.');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-instructions','4','Never use a wire brush or polish on suede or nubuck');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-instructions','5','To clean, use a rubber eraser to remove dirt and smudges');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-price','1','100.00');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-price','2','1250.00');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-mfgDate','1','01/12/1850');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-mfgDate','2','01/12/9999');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-manufacturer','1','adidas');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-manufacturer','2','puma');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-manufacturer','3','Sparx');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-manufacturer','4','Lancer');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-shoes_feature-1007','1','run');


INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-upperPart-1049','1','upper part');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-leather-1053','1','leather');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-fabric-1054','1','fabric');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-pvc-1055','1','pvc');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-metalic-1056','1','metalic');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-vamp-1057','1','vamp');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-tongue-1058','1','tongue');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-heel_counter-1059','1','heel_counter');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-back_stey-1060','1','back_stey');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-quarters-1061','1','quarters');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-toeOfShoes-1062','1','toeOfShoes');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-hardToePuff-1063','1','hardToePuff');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-insole-1048','1','insole');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-midsole-1050','1','midsole');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-wedge-1051','1','wedge');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-counter_sheet-1052','1','counter_sheet');



INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-shoes_Material-1001','1','Leather');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-shoes_Material-1001','2','rubber');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-weight','1','1.5');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-weight','2','1.9');


INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-dim1','1','35.0');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-dim1','2','30.0');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-dim2','1','29.0');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-dim2','2','25.0');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-dim3','1','12.0');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-dim3','2','15.0');
/*check your validationrule and productTypePropertyTypeDefault value from requiredProductTypeProperties and productTypePropertyTypeValidationValues*/
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-shoes_size1004','1','5');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-shoes_size1004','2','9');


INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-closer_type-1011','1','Bucklet');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-color','1','Black');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-warnty-1034','1','3');


INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-instructions','1','Avoid liquid shoe polishes and silicone sprays');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-instructions','2','Use a soft damp cloth and a drop of mild soap to remove any haze.');


INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-price','1','499.00');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-price','2','950.00');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-mfgDate','1','10/10/2016');


INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-url','1','https://www.amazon.in/Foot-Style-Black-Leather-Formal/dp/B011KHVOOC/ref=sr_1_fkmr0_3?s=shoes&ie=UTF8&qid=1509818435&sr=1-3-fkmr0&nodeID=1983572031&psd=1&keywords=Foot+n+Style+Formal+Shoes+For+Men_FS3196+-+Brown');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-url','2','http://www.shopclues.com/foot-n-style-black-formal-shoes-fs375.html');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-ANSI-1006','1','No');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-washable-1013','1','Yes');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-manufacturer','1','Foot N style Delhi');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-lyfestyle-1003','1','office');


INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-shoes_feature-1007','1','run');


INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-upperPart-1049','1','upper part');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-leather-1053','1','leather');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-fabric-1054','1','fabric');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-pvc-1055','1','pvc');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-metalic-1056','1','metalic');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-vamp-1057','1','vamp');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-tongue-1058','1','tongue');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-heel_counter-1059','1','heel_counter');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-back_stey-1060','1','back_stey');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-quarters-1061','1','quarters');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-toeOfShoes-1062','1','toeOfShoes');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-hardToePuff-1063','1','hardToePuff');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-insole-1048','1','insole');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-midsole-1050','1','midsole');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-wedge-1051','1','wedge');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-01-counter_sheet-1052','1','counter_sheet');



INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-shoes_Material-1001','1','rubber');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-weight','1','0.5');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-weight','2','1.75');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-dim1','1','26.0');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-dim1','2','35.0');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-dim2','1','18.0');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-dim2','2','13.0');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-dim3','1','10.0');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-dim3','2','7.0');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-shoes_size1004','1','5');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-shoes_size1004','2','9');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-closer_type-1011','1','less');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-color','1','blue');


INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-warnty-1034','1','0');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-warnty-1034','2','12');


INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-instructions','1','Never use a wire brush or polish on suede or nubuck');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-instructions','2','To clean, use a rubber eraser to remove dirt and smudges');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-price','1','3999.00');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-price','2','5096.00');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-mfgDate','1','10/10/2010');


INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-url','1','https://www.naaptol.com/sports-shoes/adidas-alphabounce-men-shoes-alpha-blue/p/12604709.html?ntpromoid=8092&utm_source=GOOGLE&utm_medium=Search&utm_campaign=PLA_G&utm_code=PLA&ntpromoid=8092&utm_source=GOOGLE&utm_medium=Search&utm_campaign=PLA_G&utm_code=PLA&gclid=Cj0KCQjwyvXPBRD-ARIsAIeQeoGLCrhWVsC7Br-L4gcnvjBUWvfle4hSZklonUNEhp2ybu5DM8khtGIaAqqzEALw_wcB');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','187-02-ANSI-1006','1','Yes');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','187-02-washable-1013','1','Yes');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-manufacturer','1','Adidas');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-lyfestyle-1003','1','sport');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-shoes_feature-1007','1','run');


INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-upperPart-1049','1','upper part');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-leather-1053','1','leather');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-fabric-1054','1','fabric');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-pvc-1055','1','pvc');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-metalic-1056','1','metalic');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-vamp-1057','1','vamp');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-tongue-1058','1','tongue');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-heel_counter-1059','1','heel_counter');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-back_stey-1060','1','back_stey');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-quarters-1061','1','quarters');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-toeOfShoes-1062','1','toeOfShoes');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-hardToePuff-1063','1','hardToePuff');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-insole-1048','1','insole');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-midsole-1050','1','midsole');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-wedge-1051','1','wedge');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17113','17113-187-02-counter_sheet-1052','1','counter_sheet');

