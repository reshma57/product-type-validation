
INSERT INTO productTypes VALUES("17129","303","keyboard","https://www.amazon.in/b/ref=s9_acss_bw_en_Mss_d_1_2_w?_encoding=UTF8&ie=UTF8&node=1375419031&pf_rd_m=A1VBAL9TL5WCBF&pf_rd_s=merchandised-search-2&pf_rd_r=7844T5VS6J09N0V29K4B&pf_rd_r=7844T5VS6J09N0V29K4B&pf_rd_t=101&pf_rd_p=4eac7ea2-bb50-4c99-9ca1-78d7afec4f59&pf_rd_p=4eac7ea2-bb50-4c99-9ca1-78d7afec4f59&pf_rd_i=1375419031");
INSERT INTO productTypes VALUES("17129","303-01","wiredKeyboard","https://www.amazon.in/Logitech-K230-Wireless-Keyboard/dp/B01267B9CW?tag=googinhydr18418-21&tag=googinkenshoo-21&ascsubtag=ee7e37f0-bf00-41b7-94a9-4b88f4eb2ddd");
INSERT INTO productTypes VALUES("17129","303-02","wirelessKeyboard","https://www.amazon.in/dp/B00ZYLMQH0/ref=pd_lpo_sbs_dp_ss_2?pf_rd_p=cd818f9c-142a-4b42-ad2c-f0421857aaf5&pf_rd_s=lpo-top-stripe&pf_rd_t=201&pf_rd_i=B01267B9CW&pf_rd_m=A1VBAL9TL5WCBF&pf_rd_r=CPW0MBZRXKK4GCT7F514&pf_rd_r=CPW0MBZRXKK4GCT7F514&pf_rd_p=cd818f9c-142a-4b42-ad2c-f0421857aaf5");

 
INSERT INTO productTypeHierarchy VALUES("17129","303","303-01");
INSERT INTO productTypeHierarchy VALUES("17129","303","303-02");



INSERT INTO productTypePropertyTypes VALUES("17129","Material","which material is used?");
INSERT INTO productTypePropertyTypes VALUES("17129","brand","band of keyboard");
INSERT INTO productTypePropertyTypes VALUES("17129","shape","which shapes are available");
INSERT INTO productTypePropertyTypes VALUES("17129","elegance","yes/no");
INSERT INTO productTypePropertyTypes VALUES("17129","isbasic","yes/no");
INSERT INTO productTypePropertyTypes VALUES("17129","isextended","yes/no");
INSERT INTO productTypePropertyTypes VALUES("17129","warranty","warranty of keybard");
INSERT INTO productTypePropertyTypes VALUES("17129","modelNO","keyboard modelno");
INSERT INTO productTypePropertyTypes VALUES("17129","modelYear","year of manufacture");
INSERT INTO productTypePropertyTypes VALUES("17129","isslime","yes/no");
INSERT INTO productTypePropertyTypes VALUES("17129","isWaterProof","yes/no");
INSERT INTO productTypePropertyTypes VALUES("17129","isSmart","yes/no");
INSERT INTO productTypePropertyTypes VALUES("17129","keysAvail","how many keys avail");
INSERT INTO productTypePropertyTypes VALUES("17129","waterResistanceDepth","no of progarmmable buttons");
INSERT INTO productTypePropertyTypes VALUES("17129","hasMediaPlayer","yes/no");
INSERT INTO productTypePropertyTypes VALUES("17129","keysguard","yes/no");
INSERT INTO productTypePropertyTypes VALUES("17129","hasBluetooth","yes/no");
INSERT INTO productTypePropertyTypes VALUES("17129","keyboardFeature","");
INSERT INTO productTypePropertyTypes VALUES("17129","smartFeature","Feature");
INSERT INTO productTypePropertyTypes VALUES("17129","subInstruction-1","");
INSERT INTO productTypePropertyTypes VALUES("17129","subInstruction-2","");
INSERT INTO productTypePropertyTypes VALUES("17129","subInstruction-3","");
INSERT INTO productTypePropertyTypes VALUES("17129","subInstruction-3-01","");
INSERT INTO productTypePropertyTypes VALUES("17129","subInstruction-3-02","");
INSERT INTO productTypePropertyTypes VALUES("17129","specification","specification");


INSERT INTO productTypePropertyTypeContainer VALUES("17129","keyboardFeature","smartFeature","1");
INSERT INTO productTypePropertyTypeContainer VALUES ("17129","keyboardFeature","specification","2");
INSERT INTO productTypePropertyTypeContainer VALUES ("17129","specification","Material","2");
INSERT INTO productTypePropertyTypeContainer VALUES("17129","specification","shape","3");
INSERT INTO productTypePropertyTypeContainer VALUES("17129","specification","brand","4");
INSERT INTO productTypePropertyTypeContainer VALUES("17129","specification","warranty","5");
INSERT INTO productTypePropertyTypeContainer VALUES("17129","specification","modelNO","6");
INSERT INTO productTypePropertyTypeContainer VALUES("17129","specification","waterResistanceDepth","7");
INSERT INTO productTypePropertyTypeContainer VALUES ("17129","smartFeature","isSmart","1");
INSERT INTO productTypePropertyTypeContainer VALUES ("17129","smartFeature","hasBluetooth","2");
INSERT INTO productTypePropertyTypeContainer VALUES ("17129","smartFeature","hasMediaPlayer","3");
INSERT INTO productTypePropertyTypeContainer VALUES ("17129","instruction","subInstruction-1","1");
INSERT INTO productTypePropertyTypeContainer VALUES ("17129","instruction","subInstruction-2","2");
INSERT INTO productTypePropertyTypeContainer VALUES ("17129","instruction","subInstruction-3","3");
INSERT INTO productTypePropertyTypeContainer VALUES ("17129","subInstruction-3","subInstruction-01","1");
INSERT INTO productTypePropertyTypeContainer VALUES ("17129","subInstruction-3","subInstruction-02","2");







INSERT INTO requiredProductTypeProperties  VALUES( "17129","303","price","800.00");
INSERT INTO requiredProductTypeProperties  VALUES( "17129","303","dim1","42.2");
INSERT INTO requiredProductTypeProperties  VALUES( "17129","303","dim2","1.44");
INSERT INTO requiredProductTypeProperties  VALUES( "17129","303","dim3","11.7");
INSERT INTO requiredProductTypeProperties  VALUES( "17129","303","shape","large");

INSERT INTO requiredProductTypeProperties VALUES ("17129","303","Material","fiber");
INSERT INTO  requiredProductTypeProperties VALUES ("17129","303","Color"," black");
INSERT INTO  requiredProductTypeProperties VALUES ("17129","303","brand","iball");
INSERT INTO requiredProductTypeProperties VALUES ("17129","303","Weight","0.5");
INSERT INTO requiredProductTypeProperties VALUES ("17129","303","warranty","1");
INSERT INTO requiredProductTypeProperties VALUES ("17129","303","modelNO","KEY-117-BK ");
INSERT INTO requiredProductTypeProperties VALUES ("17129","303","modelYear","2015");
INSERT INTO requiredProductTypeProperties  VALUES ("17129","303","isWaterProof","yes");
INSERT INTO requiredProductTypeProperties  VALUES( "17129","303","isSmart","no");
INSERT INTO requiredProductTypeProperties VALUES( "17129","303","waterResistanceDepth","1");
INSERT INTO requiredProductTypeProperties  VALUES( "17129","303","isbasic","no");
INSERT INTO requiredProductTypeProperties VALUES( "17129","303","isextended","yes");
INSERT INTO  requiredProductTypeProperties VALUES ( "17129","303","hasBluetooth","no");
INSERT INTO requiredProductTypeProperties VALUES ("17129","303","url","https://www.amazon.in/b/ref=s9_acss_bw_en_Mss_d_1_2_w?_encoding=UTF8&ie=UTF8&node=1375419031&pf_rd_m=A1VBAL9TL5WCBF&pf_rd_s=merchandised-search-2&pf_rd_r=7844T5VS6J09N0V29K4B&pf_rd_r=7844T5VS6J09N0V29K4B&pf_rd_t=101&pf_rd_p=4eac7ea2-bb50-4c99-9ca1-78d7afec4f59&pf_rd_p=4eac7ea2-bb50-4c99-9ca1-78d7afec4f59&pf_rd_i=1375419031");
INSERT INTO  requiredProductTypeProperties VALUES( "17129","303","specification","");
INSERT INTO  requiredProductTypeProperties VALUES( "17129","303","keyboardFeature","");
INSERT INTO   requiredProductTypeProperties VALUES( "17129","303","smartFeature","");
INSERT INTO requiredProductTypeProperties VALUES ("17129","303","instructions","keyboard instructions");
INSERT INTO requiredProductTypeProperties VALUES ("17129","303","subInstruction-1","keep it out extream temprature");
INSERT INTO requiredProductTypeProperties VALUES ("17129","303","subInstruction-2","keep in dry place");
INSERT INTO requiredProductTypeProperties VALUES ("17129","303","subInstruction-3","cleaning of keyboard");
INSERT INTO requiredProductTypeProperties VALUES ("17129","303","subInstruction-3-01","use a damp microfiber cloth to clean the keys. ");
INSERT INTO requiredProductTypeProperties VALUES ("17129","303","subInstruction-3-02","use a damp microfiber cloth to clean the keys and keep at safe place");



INSERT INTO requiredProductTypeProperties  VALUES( "17129","303-01","price",649.00);
INSERT INTO requiredProductTypeProperties  VALUES( "17129","303-01","dim1",44.2);
INSERT INTO requiredProductTypeProperties  VALUES( "17129","303-01","dim2","2.44");
INSERT INTO requiredProductTypeProperties  VALUES( "17129","303-01","dim3","12.7");

INSERT INTO requiredProductTypeProperties VALUES ("17129","303-01","Material","hardfiber");
INSERT INTO  requiredProductTypeProperties VALUES ("17129","303-01","Color"," white");
INSERT INTO  requiredProductTypeProperties VALUES ("17129","303-01","shape","medium");
INSERT INTO  requiredProductTypeProperties VALUES ("17129","303-01","brand","dell");
INSERT INTO requiredProductTypeProperties VALUES ("17129","303-01","Weight","0.5");
INSERT INTO requiredProductTypeProperties VALUES ("17129","303-01","warranty",1);
INSERT INTO requiredProductTypeProperties VALUES ("17129","303-01","modelNO","KEY-116-BK ");
INSERT INTO requiredProductTypeProperties VALUES ("17129","303-01","modelYear",2016);

INSERT INTO requiredProductTypeProperties  VALUES ("17129","303-01","isWaterProof","");
INSERT INTO requiredProductTypeProperties  VALUES( "17129","303-01","isSmart","no");
INSERT INTO requiredProductTypeProperties VALUES( "17129","303-01","waterResistanceDepth","1");
INSERT INTO requiredProductTypeProperties  VALUES( "17129","303-01","isbasic","yes");
INSERT INTO requiredProductTypeProperties VALUES( "17129","303-01","isextended","no");
INSERT INTO  requiredProductTypeProperties VALUES ( "17129","303-01","hasBluetooth","no");
INSERT INTO requiredProductTypeProperties VALUES ("17129","303-01","url","://www.amazon.in/Logitech-K230-Wireless-Keyboard/dp/B01267B9CW?tag=googinhydr18418-21&tag=googinkenshoo-21&ascsubtag=ee7e37f0-bf00-41b7-94a9-4b88f4eb2ddd");

INSERT INTO  requiredProductTypeProperties VALUES( "17129","303-01","specification","");
INSERT INTO  requiredProductTypeProperties VALUES( "17129","303-01","keyboardFeature","");
INSERT INTO   requiredProductTypeProperties VALUES( "17129","303-01","smartFeature","");


INSERT INTO requiredProductTypeProperties  VALUES( "17129","303-02","price",454.00);
INSERT INTO requiredProductTypeProperties  VALUES( "17129","303-02","dim1",39.4);
INSERT INTO requiredProductTypeProperties  VALUES( "17129","303-02","dim2","3.2");
INSERT INTO requiredProductTypeProperties  VALUES( "17129","303-02","dim3","13.2");

INSERT INTO requiredProductTypeProperties VALUES ("17129","303-02","Material","softfiber");
INSERT INTO  requiredProductTypeProperties VALUES ("17129","303-02","Color"," blue");
INSERT INTO  requiredProductTypeProperties VALUES ("17129","303-02","shape","large");
INSERT INTO  requiredProductTypeProperties VALUES ("17129","303-02","brand","logitech");
INSERT INTO requiredProductTypeProperties VALUES ("17129","303-02","Weight","0.5");
INSERT INTO requiredProductTypeProperties VALUES ("17129","303-02","warranty","1");
INSERT INTO requiredProductTypeProperties VALUES ("17129","303-02","modelNO","KEY-114-BK ");
INSERT INTO requiredProductTypeProperties VALUES ("17129","303-02","modelYear",2016);

INSERT INTO requiredProductTypeProperties  VALUES ("17129","303-02","isWaterProof","");
INSERT INTO requiredProductTypeProperties  VALUES( "17129","303-02","isSmart","yes");
INSERT INTO requiredProductTypeProperties VALUES( "17129","303-02","waterResistanceDepth","1");
INSERT INTO requiredProductTypeProperties  VALUES( "17129","303-02","isbasic","no");
INSERT INTO requiredProductTypeProperties VALUES( "17129","303-02","isextended","yes");
INSERT INTO  requiredProductTypeProperties VALUES ( "17129","303-02","hasBluetooth","yes");
INSERT INTO requiredProductTypeProperties VALUES ("17129","303-02","url","https://www.amazon.in/dp/B00ZYLMQH0/ref=pd_lpo_sbs_dp_ss_2?pf_rd_p=cd818f9c-142a-4b42-ad2c-																															f0421857aaf5&pf_rd_s=lpo-top-stripe&pf_rd_t=201&pf_rd_i=B01267B9CW&pf_rd_m=A1VBAL9TL5WCBF&pf_rd_r=CPW0MBZRXKK4GCT7F514&pf_rd_r=CPW0MBZRXKK4GCT7F514&pf_rd_p=cd818f9c-142a-4b42-ad2c-f0421857aaf5");
INSERT INTO  requiredProductTypeProperties VALUES( "17129","303-02","specification","");
INSERT INTO  requiredProductTypeProperties VALUES( "17129","303-02","keyboardFeature","");
INSERT INTO   requiredProductTypeProperties VALUES( "17129","303-02","smartFeature","");




INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-price","range","float");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-dim1","range","float");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-dim2","range","float");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-dim3","range","float");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-brand","list","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-shape","list","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-material","list","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-Color","list","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-Weight","range","float");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-warranty","list","int");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-modelNO","list","int");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-yesNoTypeValRule","list","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-waterResistanceDepth","list","int");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-url","list","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-specification","container","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-keyboardFeature","container","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-smartFeature","container","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-instructionValRule","container","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-subInstructionsValRule","list","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-00-subInstructionsValRule","container","string");



INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-01-dim1","range","float");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-01-dim2","range","float");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-01-dim3","range","float");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-01-price","range","float");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-01-brand","list","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-01-shape","list","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-01-material","list","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-01-Color","list","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-01-Weight","range","float");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-01-warranty","list","int");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-01-modelNO","list","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-01-yesNoTypeValRule","list","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-01-waterResistanceDepth","list","int");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-01-url","list","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-01-specification","container","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-01-keyboardFeature","container","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-01-smartFeature","container","string");


INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-02-dim1","range","float");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-02-dim2","range","float");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-02-dim3","range","float");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-02-price","range","float");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-02-brand","list","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-02-shape","list","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-02-material","list","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-02-Color","list","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-02-Weight","range","float");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-02-warranty","list","int");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-02-modelNO","list","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-02-yesNoTypeValRule","list","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-02-waterResistanceDepth","list","int");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-02-url","list","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-02-specification","container","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-02-keyboardFeature","container","string");
INSERT INTO productTypePropertyTypeValidationTypes VALUES ("17129","17129-303-02-smartFeature","container","string");





INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","price","Rs.","17129-303-price");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","dim1","cm","17129-303-dim1");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","dim2","cm","17129-303-dim2");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","dim3","cm","17129-303-dim3");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","Material","","17129-303-material");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","Color","","17129-303-Color");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","brand","","17129-303-brand");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","Weight","grams","17129-303-Weight");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","warranty","year","17129-303-warranty");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","modelNO","","17129-303-modelNO");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","isWaterProof","","17129-303-yesNoTypeValRule");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","waterResistanceDepth","","17129-303-waterResistanceDepth");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","shape","","17129-303-shape");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","isSmart","","17129-303-yesNoTypeValRule");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","url","","17129-303-url");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","specification","","17129-303-specification");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","keyboardFeature","","17129-303-keyboardFeature");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","smartFeature","","17129-303-smartFeature");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","instructions","","17129-303-instructionValRule");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","subInstruction-1","","17129-303-subInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","subInstruction-2","","17129-303-subInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","subInstruction-3","","17129-303-subInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","subInstruction-3-01","","17129-303-subInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303","subInstruction-3-02","","17129-303-subInstructionsValRule");

INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","price","Rs.","17129-303-01-price");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","dim1","cm","17129-303-01-dim1");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","dim2","cm","17129-303-01-dim2");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","dim3","cm","17129-303-01-dim3");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","Material","","17129-303-01-material");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","Color","","17129-303-01-Color");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","brand","","17129-303-01-brand");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","Weight","grams","17129-303-01-Weight");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","warranty","year","17129-303-01-warranty");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","modelNO","","17129-303-01-modelNO");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","isWaterProof","","17129-303-01-yesNoTypeValRule");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","waterResistanceDepth","","17129-303-01-waterResistanceDepth");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","shape","","17129-303-01-shape");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","isSmart","","17129-303-01-yesNoTypeValRule");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","url","","17129-303-01-url");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","specification","","17129-303-01-specification");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","keyboardFeature","","17129-303-01-keyboardFeature");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","smartFeature","","17129-303-01-smartFeature");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","instructions","","17129-303-01-instructionValRule");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","subInstruction-1","","17129-303-01-subInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","subInstruction-2","","17129-303-01-subInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","subInstruction-3","","17129-303-01-subInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","subInstruction-3-01","","17129-303-01-subInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-01","subInstruction-3-02","","17129-303-01-subInstructionsValRule");

INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","price","Rs.","17129-303-02-price");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","dim1","cm","17129-303-02-dim1");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","dim2","cm","17129-303-02-dim2");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","dim3","cm","17129-303-02-dim3");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","Material","","17129-303-02-material");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","Color","","17129-303-02-Color");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","brand","","17129-303-02-brand");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","Weight","grams","17129-303-02-Weight");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","warranty","year","17129-303-02-warranty");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","modelNO","","17129-303-02-modelNO");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","isWaterProof","","17129-303-02-yesNoTypeValRule");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","waterResistanceDepth","","17129-303-02-waterResistanceDepth");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","shape","","17129-303-02-shape");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","isSmart","","17129-303-02-yesNoTypeValRule");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","url","","17129-303-02-url");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","specification","","17129-303-02-specification");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","keyboardFeature","","17129-303-02-keyboardFeature");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","smartFeature","","17129-303-02-smartFeature");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","instructions","","17129-303-02-instructionValRule");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","subInstruction-1","","17129-303-02-subInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","subInstruction-2","","17129-303-02-subInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","subInstruction-3","","17129-303-02-subInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","subInstruction-3-01","","17129-303-02-subInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes VALUES ("17129","303-02","subInstruction-3-02","","17129-303-02-subInstructionsValRule");










INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-price","1","800.00");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-price","2","649.00");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-price","3","454.00");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-material","1","fiber");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-material","2","hardfiber");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-material","3","softfiber");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-Color","1","black");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-Color","2","white");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-Color","3","blue");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-dim1","1","42.2");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-dim1","2","44.2");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-dim1","3","39.4");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-dim2","1","3.2");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-dim2","2","1.44");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-dim2","3","2.44");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-dim3","1","11.7");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-dim3","2","12.7");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-dim3","3","13.2");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-brand","1","dell");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-brand","2","iball");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-brand","3","logitech");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-Weight","1","0.5");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-Weight","2","0.5");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-Weight","3","0.5");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-warranty","1","1");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-warranty","2","1");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-Warranty","3","1");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-modelNO","1","KEY-117-BK");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-modelNO","2","KEY-116-BK");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-modelNO","3","KEY-114-BK");

INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-yesNoTypeValRule","1","yes");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-yesNoTypeValRule","2","no");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-specification","1","");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-keyboardFeature","1","");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-smartFeature","1","");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-shape","1","");




INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-price","1","800.00");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-price","2","649.00");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-price","3","454.00");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-material","1","fiber");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-material","2","hardfiber");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-material","3","softfiber");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-Color","1","black");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-Color","2","white");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-Color","3","blue");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-dim1","1","42.2");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-dim1","2","44.2");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-dim1","3","39.4");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-dim2","1","3.2");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-dim2","2","1.44");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-dim2","3","2.44");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-dim3","1","11.7");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-dim3","2","12.7");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-dim3","3","13.2");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-brand","1","dell");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-brand","2","iball");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-brand","3","logitech");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-Weight","1","0.5");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-Weight","2","0.5");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-Weight","3","0.5");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-warranty","1","1");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-warranty","2","1");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-Warranty","3","1");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-modelNO","1","KEY-117-BK");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-modelNO","2","KEY-116-BK");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-modelNO","3","KEY-114-BK");

INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-yesNoTypeValRule","1","yes");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-yesNoTypeValRule","2","no");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-specification","1","");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-keyboardFeature","1","");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-smartFeature","1","");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-shape","1","large");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-01-shape","2","medium");

INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-price","1","800.00");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-price","2","649.00");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-price","3","454.00");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-material","1","fiber");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-material","2","hardfiber");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-material","3","softfiber");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-Color","1","black");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-Color","2","white");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-Color","3","blue");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-dim1","1","42.2");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-dim1","2","44.2");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-dim1","3","39.4");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-dim2","1","3.2");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-dim2","2","1.44");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-dim2","3","2.44");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-dim3","1","11.7");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-dim3","2","12.7");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-dim3","3","13.2");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-brand","1","dell");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-brand","2","iball");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-brand","3","logitech");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-Weight","1","0.5");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-Weight","2","0.5");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-Weight","3","0.5");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-warranty","1","1");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-warranty","2","1");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-Warranty","3","1");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-modelNO","1","KEY-117-BK");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-modelNO","2","KEY-116-BK");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-modelNO","3","KEY-114-BK");

INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-yesNoTypeValRule","1","yes");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-yesNoTypeValRule","2","no");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-specification","1","");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-keyboardFeature","1","");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-smartFeature","1","");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-shape","1","large");
INSERT INTO productTypePropertyTypeValidationValues VALUES ("17129","17129-303-02-shape","2","medium");


      
