INSERT INTO productTypes VALUES(17151,2775,'Flour','https://en.wikipedia.org/wiki/flour');
INSERT INTO productTypes VALUES(17151,2775-1,'Aashirvaad Wheat Flour','https://www.bigbasket.com/pd/126906/aashirvaad-atta-whole-wheat-10-kg-pouch/?utm_source=google&utm_medium=cpc&utm_term=&utm_content=188931034790&utm_campaign=Dynamic+Ads+Pune&gclid=EAIaIQobChMImLSqm9Cl1wIVxiMrCh2NvgysEAAYASAAEgIA0PD_BwE');
INSERT INTO productTypes VALUES(17151,2775-2,'24 Mantra Organic Wholewheat Atta Flour','https://www.amazon.in/24-Mantra-Organic-Wholewheat-Premium/dp/B01CHV0AGS/ref=sr_1_1?srs=9574332031&ie=UTF8&qid=1509826220&sr=8-1&keywords=wheat+floor');
INSERT INTO productTypeHierarchy VALUES(17151,2775,2775-1);
INSERT INTO productTypeHierarchy VALUES(17151,2775,2775-2);
INSERT INTO productTypePropertyTypes VALUES (17151,'flour_material','the material used to make the flour');
INSERT INTO productTypePropertyTypes VALUES (17151,'quantity', 'Flour comes in 3 or more quantity  1kg/5kg/10kg');
INSERT INTO productTypePropertyTypes VALUES (17151,'flour_nutrition_facts','Nutrition facts of the flour');
INSERT INTO productTypePropertyTypes VALUES (17151,'mfg_name','Name of the manufacture');
INSERT INTO productTypePropertyTypes VALUES (17151,'Expiry_date','Date of Expiry');
INSERT INTO productTypePropertyTypes VALUES (17151,'Flour_Quality','fssai improved');
INSERT INTO productTypePropertyTypes VALUES (17151,'Flour_features1','Feature1 of the flour');
INSERT INTO productTypePropertyTypes VALUES (17151,'Flour_features2','Feature2 of the flour');
INSERT INTO productTypePropertyTypes VALUES (17151,'Flour_features3','Feature3 of the flour ');
INSERT INTO productTypePropertyTypes VALUES (17151,'Flour_features4','Feature4 of the flour');
INSERT INTO productTypePropertyTypes VALUES (17151,'instruction','Flour instruction');
INSERT INTO productTypePropertyTypes VALUES (17151,'subinstruction1',' Instruction to be followed1');
INSERT INTO productTypePropertyTypes VALUES (17151,'subinstruction2','Instruction to be followed2');
INSERT INTO productTypePropertyTypes VALUES (17151,'subinstruction3','Instruction to be followed3');
INSERT INTO productTypePropertyTypes VALUES (17151,'subinstruction4','Instruction to be followed4');
INSERT INTO productTypePropertyTypes VALUES (17151,'subinstruction5','Instruction to be followed5');
INSERT INTO productTypePropertyTypes VALUES (17151,'subinstruction5-01','Instruction to be followed5-01');
INSERT INTO productTypePropertyTypes VALUES (17151,'subinstruction6','Instruction to be followed6');
INSERT INTO productTypePropertyTypes VALUES (17151,'starchcontent','Flour protein damage');
INSERT INTO productTypePropertyTypes VALUES (17151,'particlesize','Flour hydraion property');
INSERT INTO productTypePropertyTypes VALUES (17151,'doughproperties','Flour property');
INSERT INTO productTypePropertyTypes VALUES (17151,'oilabsorption','Flour  property');
INSERT INTO productTypePropertyTypes VALUES (17151,'Url','web addresss');
INSERT INTO productTypePropertyTypes VALUES (17151,'Flour_protein_damage','');
INSERT INTO productTypePropertyTypes VALUES (17151,'Flour_hydraion_property','');
INSERT INTO productTypePropertyTypes VALUES (17151,'Flour_property','');
INSERT INTO  productTypePropertyTypeContainer VALUES (17151,'starchcontent','Flour protein damage',1);
INSERT INTO  productTypePropertyTypeContainer VALUES (17151,'particlesize','Flour hydraion property',2 );
INSERT INTO  productTypePropertyTypeContainer VALUES (17151,'doughproperties','Flour property',3 );
INSERT INTO  productTypePropertyTypeContainer VALUES (17151,'oilabsorption','Flour property',4);
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'flour_material','Wheat');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'flour_material','Wheat');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'flour_material','Wheat');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'quantity', '5');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'quantity', '10');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'quantity', '5');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'flour_nutrition_facts','calories,protein sugar,fiber,fat,carbs');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'flour_nutrition_facts','vitamin B1,vitaminB3,vitaminB2,folic acid,calcium,phosphorus,fiber,iron,zinc');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'Flour nutrition facts','Energy,protein,fat,iron,magnesium,phosphorus,fiber');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'color','white');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'color','white');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'color','white');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'mfg_name','ITC Limited');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'mfg_name','ITC Limited');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'mfg_name','24 Mantra');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'mfgDate','10/08/2017');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'mfgDate','10/08/2017');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'mfgDate','10/10/2016');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'price','299.0');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'price','299.0');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'price','219.0');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'Expiry_date','10/02/2018');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'Expiry_date','10/02/2018');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'Expiry_date','15/04/2017');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'Flour_Quality','fssai impoved');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'Flour_Quality','fssai impoved');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'Flour_Quality','fssai impoved');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'Flour_features1','wheat atta is made of zero per cent maida and 100% atta');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'Flour_features1','wheat atta is made of zero per cent maida and 100% atta');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'Flour_features1','wheat atta is made of zero per cent maida and 100% atta');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'Flour_features2','Contains many nutrition facts');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'Flour_features2','Contains many nutrition facts');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'Flour_features2','Contains many nutrition facts');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'Flour_features3','Makes fluffy and soft rotis');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'Flour_features3','Makes fluffy and soft rotis');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'Flour_features3','Makes fluffy and soft rotis');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'Flour_features4','Many disorders which are linked with nurishment like mineral deficiencies,anaemia,obesity are cured by consuming whole wheat');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'Flour_features4','Many disorders which are linked with nurishment like mineral deficiencies,anaemia,obesity are cured by consuming whole wheat');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'Flour_features4','Many disorders which are linked with nurishment like mineral deficiencies,anaemia,obesity are cured by consuming whole wheat');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'instruction','Flour instruction ');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'instruction','Flour instruction ');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'instruction','Flour instruction ');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'subinstruction1','place flour in dry place');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'subinstruction1','place flour in dry place');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'subinstruction1','place flour in dry place');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'subinstruction2','Beware of the bugs in the Flour');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'subinstruction2','Beware of the bugs in the Flour');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'subinstruction2','Beware of the bugs in the Flour');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'subinstruction3','Store the Flour in the air tight container');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'subinstruction3','Store the Flour in the air tight container');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'subinstruction3','Store the Flour in the air tight container');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'subinstruction4','put the Flour in the freezer to freeze the larvae of bugs');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'subinstruction4','put the Flour in the freezer to freeze the larvae of bugs');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'subinstruction4','put the Flour in the freezer to freeze the larvae of bugs');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'subinstruction5','Use colander for the Flour');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'subinstruction5','Use colander for the Flour');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'subinstruction5','Use colander for the Flour');
INSERT INTO requiredProductTypeProperties VALUES(17151,2775,'subinstruction5-01','Use Small holes colander for the Flour');
INSERT INTO requiredProductTypeProperties VALUES(17151,2775-1,'subinstruction5-01','Use Small holes colander for the Flour');
INSERT INTO requiredProductTypeProperties VALUES(17151,2775-2,'subinstruction5-01','Use Small holes colander for the Flour');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'subinstruction6','Keep your storage area clean to prevent insect infestation');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'subinstruction6','Keep your storage area clean to prevent insect infestation');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'subinstruction6','Keep your storage area clean to prevent insect infestation');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'starchcontent','Flour protein damage');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'starchcontent','Flour protein damage');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'starchcontent','Flour protein damage');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'particlesize','Flour hydraion property');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'particlesize','Flour hydraion property');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'particlesize','Flour hydraion property');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'doughproperties','Flour property');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'doughproperties','Flour property');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'doughproperties','Flour property');
INSERT INTO requiredProductTypeProperties  VALUES (17151,2775,'oilabsorption','Flour  property');
INSERT INTO requiredProductTypeProperties  VALUES (17151,2775-1,'oilabsorption','Flour  property');
INSERT INTO requiredProductTypeProperties  VALUES (17151,2775-2,'oilabsorption','Flour  property');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775,'Url','https://en.wikipedia.org/wiki/flour');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-1,'Url','https://www.bigbasket.com/pd/126906/aashirvaad-atta-whole-wheat-10-kg-pouch/?utm_source=google&utm_medium=cpc&utm_term=&utm_content=188931034790&utm_campaign=Dynamic+Ads+Pune&gclid=EAIaIQobChMImLSqm9Cl1wIVxiMrCh2NvgysEAAYASAAEgIA0PD_BwE');
INSERT INTO requiredProductTypeProperties VALUES (17151,2775-2,'Url','https://www.amazon.in/24-Mantra-Organic-Wholewheat-Premium/dp/B01CHV0AGS/ref=sr_1_1?srs=9574332031&ie=UTF8&qid=1509826220&sr=8-1&keywords=wheat+floor');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-Flour_material','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-quantity','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-flour_nutrition_facts','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-color','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-mfg_name','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-mfgDate','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-Expiry_date','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-price','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-Flour_quantity','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-Flour_features1','container','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-Flour_features2','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-Flour_features3','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-Flour_features4','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-instructions','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-subinstruction1','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-subinstruction2','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-subinstruction3','container', 'string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-subinstruction4','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-subinstruction5','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-subinstruction5-01','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-subinstruction6','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES (17151,'17151-2775-starchcontent','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES (17151,'17151-2775-particlesize','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES (17151,'17151-2775-doughproperties','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES (17151,'17151-2775-oilabsorption','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-url','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-1-Flour_material','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-1-Quantty','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-1-flour_nutrition_facts','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-1-color','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-1-mfg_name','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-1-mfgdate','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-1-Expiry_date','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-1-price','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-1-Flour_quantity','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-1-Flour_features1','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-1-Flour_features2','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-1-Flour_features3','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-1-Flour_features4','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-1-instructions','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-1-subinstruction1','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-1-subinstruction2','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-1-subinstruction3','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-1-subinstruction4','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-1-subinstruction5','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-1-subinstruction5-01','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-1-subinstruction6','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES (17151,'17151-2775-1-starchcontent','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES (17151,'17151-2775-1-particlesize','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES (17151,'17151-2775-1-doughproperties','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES (17151,'17151-2775-1-oilabsorption','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-1-url','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-2-Flour_material','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-2-quantity','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-2-flour_nutrition_facts','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-2-color','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-2-mfg_name','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-2-mfgdate','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-2-Expiry_date','list','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-2-price','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-2-Flour_quantity','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-2-Flour_features1','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-2-Flour_features2','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-2-Flour_features3','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-2-Flour_features4','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-2-instructions','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-2-subinstruction1','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-2-subinstruction2','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-2-subinstruction3','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-2-subinstruction4','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-2-subinstruction5','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-2-subinstruction5-01','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-2-subinstruction6','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES (17151,'17151-2775-2-starchcontent','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES (17151,'17151-2775-2-particlesize','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES (17151,'17151-2775-2-doughproperties','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES (17151,'17151-2775-2-oilabsorption','container','string');
INSERT INTO productTypePropertyTypeValidationTypes VALUES('17151','17151-2775-2-url','list','string');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775,'flour_material','mtype','17151-2775-flour_material');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775,'quantity','kg', '17151-2775-quantity');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775,'flour_nutrition_facts','ntype','17151-2775-flour_nutrition_facts');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775,'color','colortype','17151-2775-color');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775,'mfg_name','Mname','17151-2775-mfg name');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775,'mfgDate','Mdate','17151-2775-mfgDate');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775,'price','Rs','17151-2775-price');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775,'Expiry_date','Edate','17151-2775-Expiry Date');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775,'Flour_Quality','qtype','17151-2775-flour quantity');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775,'Flour_features1','ftype','17151-2775-flour feature1');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775,'Flour_features2','ftype','17151-2775-flour feature2');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775,'Flour_features3','ftype','17151-2775-flour feature3');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775,'Flour_features4','ftype','17151-2775-flour feature4');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775,'instruction','Itype','17151-2775-instructions');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775,'subinstruction1','Itype','17151-2775-subinstruction1');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775,'subinstruction2','Itype','17151-2775-subinstruction2');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775,'subinstruction3','Itype','17151-2775-subinstruction3');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775,'subinstruction4','Itype','17151-2775-subinstruction4');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775,'subinstruction5','Itype','17151-2775-subinstruction5');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775,'subinstruction5-01','Itype','17151-2775-subinstruction5-01');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775,'subinstruction6','Itype','17151-2775-subinstruction6');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775,'starchcontent','Ptype','17151-2775-starchcontent');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775,'particlesize','Ptype','17151-2775-particlesize');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775,'doughproperties','Ptype','17151-2775-doughproperties');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775,'oilabsorption','Ptype','17151-2775-oilabsorption');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775-1,'flour_material','mtype','17151-2775-1-Flour material');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775-1,'quantity','kg', '17151-2775-1-quantity');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775-1,'flour_nutrition_facts','ntype','17151-2775-1-flour_nutrition_facts');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775-1,'color','colortype','17151-2775-1-color');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775-1,'mfg_name','Mname','17151-2775-1-mfgname');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-1,'mfgDate','Mdate','17151-2775-1-mfgdate');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-1,'price','Rs','17151-2775-1-price');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775-1,'Expiry_date','Edate','17151-2775-1-expirydate');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775-1,'Flour_Quality','qtype','17151-2775-1-flour quantity');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-1,'Flour_features1','ftype','17151-2775-1-Flour feature1');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-1,'Flour_features2','ftype','17151-2775-1-Flour feature2');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-1,'Flour_features3','ftype','17151-2775-1-Flour feature3');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-1,'Flour_features4','ftype','17151-2775-1-Flour feature4');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775-1,'instruction','Itype','17151-2775-1-instructions');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775-1,'subinstruction1','Itype','17151-2775-1-subinstruction1');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-1,'subinstruction2','Itype','17151-2775-1-subinstruction2');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-1,'subinstruction3','Itype','17151-2775-1-subinstruction3');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-1,'subinstruction4','Itype','17151-2775-1-subinstruction4');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-1,'subinstruction5','Itype','17151-2775-1-subinstruction5');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-1,'subinstruction5-01','Itype','17151-2775-1-subinstruction5-01');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-1,'subinstruction6','Itype','17151-2775-1-subinstruction6');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-1,'starchcontent','Ptype','17151-2775-1-starchcontent');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-1,'particlesize','Ptype','17151-2775-1-particlesize');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-1,'doughproperties','Ptype','17151-2775-1-doughproperties');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-1,'oilabsorption','Ptype','17151-2775-1-oilabsorption');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775-2,'flour_material','mtype','17151-2775-2-Flour material');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775-2,'quantity','kg', '17151-2775-2-quantity');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775-2,'flour_nutrition_facts','ntype','17151-2775-2-flour_nutrition_facts');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775-2,'color','colortype','17151-2775-2-color');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775-2,'mfg_name','Mname','17151-2775-2-mfgname');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-2,'mfgDate','Mdate','17151-2775-2-mfgdate');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-2,'price','Rs','17151-2775-2-price');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775-2,'Expiry_date','Edate','17151-2775-2-expirydate');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775-2,'Flour_Quality','qtype','17151-2775-2-flour quantity');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-2,'Flour_features1','ftype','17151-2775-2-flour feature1');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-2,'Flour_features2','ftype','17151-2775-2-flour feature2');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-2,'Flour_features3','ftype','17151-2775-2-flour feature3');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-2,'Flour_features4','ftype','17151-2775-2-flour feature4');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775-2,'instruction','Itype','17151-2775-2-instructions');
INSERT INTO productTypePropertyTypeValueTypes  VALUES (17151,2775-2,'subinstruction1','Itype','17151-2775-2-subinstruction1');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-2,'subinstruction2','Itype','17151-2775-2-subinstruction2');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-2,'subinstruction3','Itype','17151-2775-2-subinstruction3');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-2,'subinstruction4','Itype','17151-2775-2-subinstruction4');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-2,'subinstruction5','Itype','17151-2775-2-subinstruction5');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-2,'subinstruction5-01','Itype','17151-2775-2-subinstruction5-01');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-2,'subinstruction6','Itype','17151-2775-2-subinstruction6');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-2,'starchcontent','Ptype','17151-2775-2-starchcontent');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-2,'particlesize','Ptype','17151-2775-2-particlesize');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-2,'doughproperties','Ptype','17151-2775-2-doughproperties');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-2,'oilabsorption','Ptype','17151-2775-2-oilabsorption');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775,'Url','urltype','17151-2775-url');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-1,'Url','urltype','17151-2775-1-url');
INSERT INTO productTypePropertyTypeValueTypes VALUES (17151,2775-2,'Url','urltype','17151-2775-2-url');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-flour_material','1','wheat');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-quantity','1','10');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-flour_nutrition_facts','1','vitamin B1,vitaminB3,vitaminB2,folic acid,calcium,phosphorus,fiber,iron,zinc');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-color','1','white');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-mfg_name','1','ITC Limited');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-mfgDate','1','10/10/2017');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-Expiry_date','1','25/08/2017');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-price','1','299.0');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-flour quantity','1','fssai improved');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-Flour_features1','1','wheat atta is made of zero per cent maida and 100% atta');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-Flour_features2','2','Contains many nutrition facts');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-Flour_features3','3','Makes fluffy and soft rotis');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-Flour_features4','4','Many disorders which are linked with nurishment like mineral deficiencies,anaemia,obesity are cured by consuming whole wheat');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-instructions','1','flour instruction');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-subinstruction1','2','place Flour  in dry place');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-subinstruction2','3','Beware of the bugs in the Flour');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-subinstruction3','4','Store the Flour in the air tight container');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-subinstruction4','5','Put the flour in the freezer to freeze the larvae of bugs');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-subinstruction5','6','Use colander for the Flour');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-subinstruction5-01','6','Use Small holes colander for the Flour');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-subinstruction6','7','Keep your storage area clean to prevent insect infestation');
INSERT INTO productTypePropertyTypeValidationValues  VALUES (17151,'17151-2775-starchcontent','1','Flour protein damage');
INSERT INTO productTypePropertyTypeValidationValues VALUES (17151,'17151-2775-particlesize','2','Flour hydraion property');
INSERT INTO productTypePropertyTypeValidationValues VALUES (17151,'17151-2775-doughproperties','3','Flour property');
INSERT INTO productTypePropertyTypeValidationValues  VALUES (17151,'17151-2775-oilabsorption','4','Flour property');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-url','1','');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-1-Flour material','2','wheat');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-quantity','2','1.00');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-flour_nutrition_facts','2','calcium,protein,falic acid,zinc,magnesium,fiber');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-2-color','1','white');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-2-mfg_name','1','24 Mantra');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-2-mfgdate','1','10/10/2016');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-2-Expiry_date','1','15/04/2017');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-2-price','1','219.0');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-2-flour quantity','1','fssai improved');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-2-Flour_features1','1','wheat atta is made of zero per cent maida and 100% atta');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-2-Flour_features2','2','Contains many nutrition facts');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-2-Flour_features3','3','Makes fluffy and soft rotis');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-2-Flour_features4','4','Many disorders which are linked with nurishment like mineral deficiencies,anaemia,obesity are cured by consuming whole wheat');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-2-instructions','1','flour instruction');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-2-subinstruction1','2','place Flour  in dry place');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-2-subinstruction2','3','Beware of the bugs in the Flour');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-2-subinstruction3','4','Store the Flour in the air tight container');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-2-subinstruction4','5','Put the flour in the freezer to freeze the larvae of bugs');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-2-subinstruction5','6','Use colander for the Flour');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-2-subinstruction5-01','6','Use Small holes colander for the Flour');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-2-subinstruction6','7','Keep your storage area clean to prevent insect infestation');
INSERT INTO productTypePropertyTypeValidationValues  VALUES (17151,'17151-2775-2-starchcontent','1','Flour protein damage');
INSERT INTO productTypePropertyTypeValidationValues VALUES (17151,'17151-2775-2-particlesize','2','Flour hydraion property');
INSERT INTO productTypePropertyTypeValidationValues VALUES (17151,'17151-2775-2-doughproperties','3','Flour property');
INSERT INTO productTypePropertyTypeValidationValues  VALUES (17151,'17151-2775-2-oilabsorption','4','Flour property');
INSERT INTO productTypePropertyTypeValidationValues VALUES('17151','17151-2775-2-url','1','');
