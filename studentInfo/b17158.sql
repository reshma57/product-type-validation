INSERT INTO productTypes Values("17158","201","chopstick","https://en.wikipedia.org/wiki/Chopsticks");
INSERT INTO productTypes Values("17158","201-01","bamboo-chopstick","https://www.ubuy.co.in/catalog/product/view/id/5204700/s/?sku=B01985K8I6&store=US&p-key=0511");
INSERT INTO productTypes Values("17158","201-02","plastic-chopstick","https://www.flipkart.com/xeekart-eating-plastic-japanese-chopstick/p/itmexetdtfhbdwnz?pid=COPEXETDZDR4SVF7&srno=s_1_20&otracker=search&lid=LSTCOPEXETDZDR4SVF7QFWLWL&fm=SEARCH&iid=763dfb81-e59f-4362-aa1e-f65e56d4d95a.COPEXETDZDR4SVF7.SEARCH&qH=7a037d76094db3db");




INSERT INTO productTypeHierarchy Values("17158","201","201-01");
INSERT INTO productTypeHierarchy Values("17158","201","201-02");




INSERT INTO productTypePropertyTypes Values("17158","Material","which material used to better fit and comfort?");
INSERT INTO productTypePropertyTypes Values("17158","WaterProof","chopstick are waterproof or not");
INSERT INTO productTypePropertyTypes Values("17158","Warranty","warranty of chopstick");
INSERT INTO productTypePropertyTypes Values("17158","Portable","Is chopstick portable or not");
INSERT INTO productTypePropertyTypes Values("17158","Length","What is the length of chopstick?");
INSERT INTO productTypePropertyTypes Values("17158","Versatile","is chopstick versatile or not?");
INSERT INTO productTypePropertyTypes Values("17158","Subinstruction-1","");
INSERT INTO productTypePropertyTypes Values("17158","Subinstruction-2","");
INSERT INTO productTypePropertyTypes Values("17158","Subinstruction-3","");
INSERT INTO productTypePropertyTypes Values("17158","Features","");
INSERT INTO productTypePropertyTypes Values("17158","Quantity","maximum set of chopstick");
INSERT INTO productTypePropertyTypes Values("17158","Size","It can be a two types i.e. S or M ");
INSERT INTO productTypePropertyTypes Values("17158","Specification","Specification");
INSERT INTO productTypePropertyTypes Values("17158","Smartfeature","Feature");
INSERT INTO productTypePropertyTypes Values("17158","isstylish","yes/no");
INSERT INTO productTypePropertyTypes Values("17158","issleek","yes/no");
INSERT INTO productTypePropertyTypes Values("17158","Subinstruction-3-01","");
INSERT INTO productTypePropertyTypes Values("17158","Subinstruction-3-02","");
INSERT INTO productTypePropertyTypes Values("17158","issmart","yes/no");
INSERT INTO productTypePropertyTypes Values("17158","Shape","which shape are availabel or not");




INSERT INTO productTypePropertyTypeContainer Values("17158","Features","Smartfeature","1");
INSERT INTO productTypePropertyTypeContainer Values("17158","Features","Specification","2");
INSERT INTO productTypePropertyTypeContainer Values("17158","Instruction","Subinstruction-1","1");
INSERT INTO productTypePropertyTypeContainer Values("17158","Instruction","Subinstruction-2","2");
INSERT INTO productTypePropertyTypeContainer Values("17158","Instruction","Subinstruction-3","3");
INSERT INTO productTypePropertyTypeContainer Values("17158","Subinstruction-3","Subinstruction-01","1");
INSERT INTO productTypePropertyTypeContainer Values("17158","subinstruction-3","Subinstruction-02","2");
INSERT INTO productTypePropertyTypeContainer Values("17158","Specification","Material","1");
INSERT INTO productTypePropertyTypeContainer Values("17158","Specification","Warranty","2");
INSERT INTO productTypePropertyTypeContainer Values("17158","Specification","Shape","3");
INSERT INTO productTypePropertyTypeContainer Values("17158","Specification","Length","4");
INSERT INTO productTypePropertyTypeContainer Values("17158","Specification","Portable","5");
INSERT INTO productTypePropertyTypeContainer Values("17158","Smartfeature","issmart","1");
INSERT INTO productTypePropertyTypeContainer Values("17158","Smartfeature","isstylish","2");
INSERT INTO productTypePropertyTypeContainer Values("17158","Smartfeature","issleek","3");
INSERT INTO productTypePropertyTypeContainer Values("17158","Smartfeature","Versatile","3");




INSERT INTO requiredProductTypeProperties Values("17158","201","Material","Bamboo and Plastic");
INSERT INTO requiredProductTypeProperties Values("17158","201","Color"," white");
INSERT INTO requiredProductTypeProperties Values("17158","201","WaterProof","Yes");
INSERT INTO requiredProductTypeProperties Values("17158","201","Portable","Yes");
INSERT INTO requiredProductTypeProperties Values("17158","201","Pattern","no");
INSERT INTO requiredProductTypeProperties Values("17158","201","Weight","159.0gm");
INSERT INTO requiredProductTypeProperties Values("17158","201","Length","27 .0cm");
INSERT INTO requiredProductTypeProperties Values("17158","201","Price","765.00");
INSERT INTO requiredProductTypeProperties Values("17158","201","Url","https://www.ubuy.co.in/catalog/product/view/id/5204700/s/?sku=B01985K8I6&store=US&p-key=0511");
INSERT INTO requiredProductTypeProperties Values("17158","201","MfDate","12-12-2017");
INSERT INTO requiredProductTypeProperties Values("17158","201","Versatile","yes");
INSERT INTO requiredProductTypeProperties Values("17158","201","Instructions","Chopstick Instructions");
INSERT INTO requiredProductTypeProperties Values("17158","201","Subinstruction-1","Keep away chopstick from fire ");
INSERT INTO requiredProductTypeProperties Values("17158","201","Subinstruction-2","keep in dry place");
INSERT INTO requiredProductTypeProperties Values("17158","201","Subinstruction-3","cleaning of chopstick");
INSERT INTO requiredProductTypeProperties Values("17158","201","Subinstruction-3-01","use powder to clean chopstick");
INSERT INTO requiredProductTypeProperties Values("17158","201","Subinstruction-3-02","use powder to clean and keep at safe place");
INSERT INTO requiredProductTypeProperties Values("17158","201","Features","");
INSERT INTO requiredProductTypeProperties Values("17158","201","Quantity","10");
INSERT INTO requiredProductTypeProperties Values("17158","201","Size","S");
INSERT INTO requiredProductTypeProperties Values("17158","201","Dimention-1","26.0cm");
INSERT INTO requiredProductTypeProperties Values("17158","201","Dimention-2","1.1cm");
INSERT INTO requiredProductTypeProperties Values("17158","201","Dimention-3","27.0cm");
INSERT INTO requiredProductTypeProperties Values("17158","201","ManufactureName","maccine");
INSERT INTO requiredProductTypeProperties Values("17158","201","Shape","large");
INSERT INTO requiredProductTypeProperties Values("17158","201","Warranty","1.0");
INSERT INTO requiredProductTypeProperties Values("17158","201","Smartfeature","");
INSERT INTO requiredProductTypeProperties Values("17158","201","isstylish","yes");
INSERT INTO requiredProductTypeProperties Values("17158","201","issmart","yes");
INSERT INTO requiredProductTypeProperties Values("17158","201","Specification","");
INSERT INTO requiredProductTypeProperties Values("17158","201","issleek","yes");




INSERT INTO requiredProductTypeProperties Values("17158","201-01","Material","Bamboo");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","Color","golden");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","ManufactureName","maccine");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","Price","765.00");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","Url","https://www.ubuy.co.in/catalog/product/view/id/5204700/s/?sku=B01985K8I6&store=US&p-key=0511");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","Size","M");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","WaterProof","YES");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","Portable","yes");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","Pattern","NO");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","Weight","120.0gm");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","Length","20.0cm");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","Versatile","Yes");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","MfDate","3-4-2017");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","Features","These natural chopstick are very light in weight");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","Quantity","10");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","Dimention-1","20.0cm");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","Dimention-2","1.4cm");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","Dimention-3","20.0cm");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","Shape","large");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","Warranty","1.0");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","Smartfeature","");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","isstylish","yes");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","issmart","yes");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","Specification","");
INSERT INTO requiredProductTypeProperties Values("17158","201-01","issleek","yes");


 







INSERT INTO requiredProductTypeProperties Values("17158","201-02","Material","Plastic ");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","Length","22.2-23.0 cm");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","Color","yellow");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","ManufactureName","maccine");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","Price","329.00-330.00");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","Url","https://www.flipkart.com/xeekart-eating-plastic-japanese-chopstick/p/itmexetdtfhbdwnz?pid=COPEXETDZDR4SVF7&srno=s_1_20&otracker=search&lid=LSTCOPEXETDZDR4SVF7QFWLWL&fm=SEARCH&iid=763dfb81-e59f-4362-aa1e-f65e56d4d95a.COPEXETDZDR4SVF7.SEARCH&qH=7a037d76094db3db");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","WaterProof","YES");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","Portable","YES");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","Pattern","YES");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","Features"," sharp grip ");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","Quantity","2");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","Weight","129.0gm");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","Versatile","YES");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","MfDate","12-12-2017");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","Size","S");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","Dimention-1","20.0cm");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","Dimention-2","1.2cm");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","Dimention-3","22.ocm");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","Shape","medium");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","Warranty","1.0");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","Smartfeature","");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","isstylish","yes");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","issmart","yes");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","Specification","");
INSERT INTO requiredProductTypeProperties Values("17158","201-02","issleek","yes");



 






INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-Length","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-Children","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-Youth","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-Men","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-Color","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-ManufactureName","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-MfDate","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-Price","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-InstructionValRule","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-SubInstructionsValRule","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-Material","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-YesNoTypeValRule","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-Features","container","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-Size","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-Url","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-Quantity","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-Dimention-1","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-Dimention-2","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-Dimention-3","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-Weight","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-Shape","container","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-Warranty","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-Smartfeature","container","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-Specification","container","string");


                                   



INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-Length","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-Boys","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-Youth","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-Men","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-Color","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-ManufactureName","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-MfDate","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-Price","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-InstructionValRule","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-SubInstructionsValRule","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-Material","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-YesNoTypeValRule","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-Features","container","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-Size","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-Url","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-Quantity","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-Dimention-1","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-Dimention-2","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-Dimention-3","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-Weight","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-Warranty","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-Shape","container","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-Smartfeature","container","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-01-Specification","container","string");




INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-Length","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-Boys","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-Youth","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-Men","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-Color","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-ManufactureName","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-MfDate","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-Price","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-InstructionValRule","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-SubInstructionsValRule","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-Material","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-YesNoTypeValRule","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-Features","container","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-Size","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-Url","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-Quantity","list","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-Dimention-1","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-Dimention-2","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-Dimention-3","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-Weight","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-Specification","container","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-Warranty","range","float");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-Shape","container","string");
INSERT INTO productTypePropertyTypeValidationTypes Values ("17158","17158-201-02-Smartfeature","container","string");





INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Length","cm","17158-201-Length");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Color","","17158-201-Color");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","ManufactureName","","17158-201-ManufactureName");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","MfDate","","17158-201-MfDate");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Price","Rs.","17158-201-Price");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Instructions","","17158-201-InstructionValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Subinstruction-1","","17158-201-SubInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Subinstruction-2","","17158-201-SubInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Subinstruction-3","","17158-201-SubInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Subinstruction-3-01","","17158-201-SubInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Subinstruction-3-02","","17158-201-SubInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Material","","17158-201-Material");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","WaterProof","","17158-201-YesNoTypeValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Portable","","17158-201-YesNoTypeValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Pattern","","17158-201-YesNoTypeValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Features","","17158-201-Features");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Size","","17158-201-Size");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Url","","17158-201-Url");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Weight","gm","17158-201-Weight");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Versatile","","17158-201-YesNoTypeValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Quantity","","17158-201-Quantity");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Dimention-1","cm","17158-201-Dimention-1");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Dimention-2","cm","17158-201-Dimention-2");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Dimention-3","cm","17158-201-Dimention-3");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Specification","","17158-201-Specification");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Smartfeature","","17158-201-Smartfeature");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","issmart","","17158-201-YesNotypeValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Warranty","year","17158-201-Warranty");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201","Shape","","17158-201-Shape");




INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Length","cm","17158-201-01-Length");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Color","","17158-201-01-Color");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","ManufactureName","","17158-201-01-ManufactureName");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","MfDate","","17158-201-01-MfDate");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Price","Rs.","17158-201-01-Price");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Instructions","","17158-201-01-InstructionValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Subinstruction-1","","17158-201-01-SubInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Subinstruction-2","","17158-201-01-SubInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Subinstruction-3","","17158-201-01-SubInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Subinstruction-3-01","","17158-201-01-SubInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Subinstruction-3-02","","17158-201-01-SubInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Material","","17158-201-01-Material");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","WaterProof","","17158-201-01-YesNoTypeValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Portable","","17158-201-01-YesNoTypeValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Pattern","","17158-201-01-YesNoTypeValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Dimention-1","cm","17158-201-01-Dimention-1");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Dimention-2","cm","17158-201-01-Dimention-2");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Dimention-3","cm","17158-201-01-Dimention-3");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Url","","17158-201-01-Url");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Versatile","","17158-201-01-YesNoTypeValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Features","","17158-201-01-Features");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Quantity","","17158-201-01-Quantity");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Size","","17158-201-01-Size");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Weight","gm","17158-201-01-Weight");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Specification","","17158-201-01-Specification");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Smartfeature","","17158-201-01-Smartfeature");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","issmart","","17158-201-01-YesNotypeValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Warranty","year","17158-201-01-Warranty");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-01","Shape","","17158-201-01-Shape");







                                                                                                                                              
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Length","cm","17158-201-02-Length");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Color","","17158-201-02-Color");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","ManufactureName","","17158-201-02-ManufactureName");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","MfDate","","17158-201-02-MfDate");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Price","Rs.","17158-201-02-Price");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Instructions","","17158-201-02-InstructionValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Subinstruction-1","","17158-201-02-SubInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Subinstruction-2","","17158-201-02-SubInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Subinstruction-3","","17158-201-02-SubInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Subinstruction-3-01","","17158-201-02-SubInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Subinstruction-3-02","","17158-201-02-SubInstructionsValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Material","","17158-201-02-Material");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","WaterProof","","17158-201-02-YesNoTypeValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Portable","","17158-201-02-YesNoTypeValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Pattern","","17158-201-02-YesNoTypeValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Features","","17158-201-02-Features");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Size","","17158-201-02-Size");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Versatile","","17158-201-02-YesNoTypeValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Weight","gm","17158-201-02-Weight");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Url","","17158-201-02-Url");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Quantity","","17158-201-02-Quantity");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Dimention-1","cm","17158-201-02-Dimention-1");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Dimention-2","cm","17158-201-02-Dimention-2");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Dimention-3","cm","17158-201-02-Dimention-3");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Specification","","17158-201-02-Specification");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Smartfeature","","17158-201-02-Smartfeature");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","issmart","","17158-201-02-YesNotypeValRule");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Warranty","year","17158-201-02-Warranty");
INSERT INTO productTypePropertyTypeValueTypes Values ("17158","201-02","Shape","","17158-201-02-Shape");









INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Size","1","S"); 
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Size","2","M"); 
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Color","1","White");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Color","2","yellow"); 
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Color","3","golden");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Dimention-1","1","20.22");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Dimention-1","2","20.0");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Dimention-1","3","26");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Dimention-2","1","1.1");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Dimention-2","2","1.2");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Dimention-2","3","1.4");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Dimention-3","1","22.0");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Dimention-3","2","27.0");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Dimention-3","3","20.0");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Weight","1","129");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Weight","2","120");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-weight","3","159");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-ManufactureName","1","maccine");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-ManufactureName","2","maccine");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-MfDate","1","12-12-2017");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-MfDate","2","8-1-2017");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-MfDate","3","3-4-2017");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Price","1","765.00");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Price","2","765.00");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Price","3","329.00");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Material","1","Bamboo");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Material","2","Bamboo and plastic");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Material","3","Plastic");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-YesNoTypeValRule","1","yes");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-YesNoTypeValRule","2","no");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Specification","1","");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Smartfeatures","1","");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Warranty","1","1");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Warranty","2","1");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Warranty","3","1");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Shape","1","large");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-Shape","2","medium");


INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Size","1","S");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Size","2","M");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Color","1","White");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Color","2","yellow");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Color","3","golden");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Dimention-1","1","20.22");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Dimention-1","2","20.0");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Dimention-1","3","26");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Dimention-2","1","1.1");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Dimention-2","2","1.2");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Dimention-2","3","1.4");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Dimention-3","1","22.0");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Dimention-3","2","27.0");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Dimention-3","3","20.0");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Weight","1","129");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Weight","2","120");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-weight","3","159");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-ManufactureName","1","maccine");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-ManufactureName","2","maccine");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-MfDate","1","12-12-2017");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-MfDate","2","8-1-2017");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-MfDate","3","3-4-2017");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Price","1","765.00");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Price","2","765.00");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Price","3","329.00");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Material","1","Bamboo");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Material","2","Bamboo and plastic");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Material","3","Plastic");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-YesNoTypeValRule","1","yes");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-YesNoTypeValRule","2","no");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Specification","1","");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Smartfeatures","1","");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Warranty","1","1");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Warranty","2","1");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Warranty","3","1");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Shape","1","large");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-01-Shape","2","medium");

INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Size","1","S");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Size","2","M");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Color","1","White");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Color","2","yellow");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Color","3","golden");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Dimention-1","1","20.22");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Dimention-1","2","20.0");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Dimention-1","3","26");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Dimention-2","1","1.1");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Dimention-2","2","1.2");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Dimention-2","3","1.4");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Dimention-3","1","22.0");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Dimention-3","2","27.0");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Dimention-3","3","20.0");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Weight","1","129");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Weight","2","120");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-weight","3","159");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-ManufactureName","1","maccine");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-ManufactureName","2","maccine");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-MfDate","1","12-12-2017");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-MfDate","2","8-1-2017");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-MfDate","3","3-4-2017");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Price","1","765.00");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Price","2","765.00");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Price","3","329.00");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Material","1","Bamboo");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Material","2","Bamboo and plastic");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Material","3","Plastic");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-YesNoTypeValRule","1","yes");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-YesNoTypeValRule","2","no");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Specification","1","");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Smartfeatures","1","");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Warranty","1","1");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Warranty","2","1");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Warranty","3","1");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Shape","1","large");
INSERT INTO productTypePropertyTypeValidationValues Values ("17158","17158-201-02-Shape","2","medium");











