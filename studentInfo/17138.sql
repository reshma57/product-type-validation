INSERT INTO productTypes VALUES('17138','524','Eyeglasses','https://www.amazon.in/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=eyeglasses');
INSERT INTO productTypes VALUES('17138','524-01','Titan Eyeglasses','https://eyeplus.titan.co.in/t2211c1a1-from-titan.html');
INSERT INTO productTypes VALUES('17138','524-02','Ray-Ban Aviator Sunglasses','https://www.amazon.in/Ray-Ban-Gradient-Sunglasses-millimeters-Multi-Coloured/dp/B00M3U924A/ref=sr_1_1?ie=UTF8&qid=1509272217&sr=8-1&keywords=rayban+sunglasses');


INSERT INTO productTypeHierarchy VALUES ('17138','524','524-01');
INSERT INTO productTypeHierarchy VALUES ('17138','524','524-02');

INSERT INTO productTypePropertyTypes VALUES ('17138','eyeglassFeatures',"");
INSERT INTO productTypePropertyTypes VALUES ('17138','frameShape',"");
INSERT INTO productTypePropertyTypes VALUES ('17138','protection',"");
INSERT INTO productTypePropertyTypes VALUES ('17138','lensFeatures',"");
INSERT INTO productTypePropertyTypes VALUES ('17138','rimType',"");
INSERT INTO productTypePropertyTypes VALUES ('17138','frameMaterial',"the material from which frame is made of");


INSERT INTO productTypePropertyTypes VALUES ('17138','oval',"is frameShape oval (yes/no)");
INSERT INTO productTypePropertyTypes VALUES ('17138','rectangle',"is frameShape rectangle (yes/no)");
INSERT INTO productTypePropertyTypes VALUES ('17138','round',"is frameShape round (yes/no)");
INSERT INTO productTypePropertyTypes VALUES ('17138','aviator',"is frameShape aviator (yes/no)");
INSERT INTO productTypePropertyTypes VALUES ('17138','UV_Protection',"are glasses UV protected (yes/no)");
INSERT INTO productTypePropertyTypes VALUES ('17138','antiGlare',"are glasses antiGlare (yes/no)");
INSERT INTO productTypePropertyTypes VALUES ('17138','polarized',"are glasses polarized (yes/no)");
INSERT INTO productTypePropertyTypes VALUES ('17138','blueReflector',"are glasses blueReflector (yes/no)");
INSERT INTO productTypePropertyTypes VALUES ('17138','waterResistent',"are glasses waterResistent (yes/no)");
INSERT INTO productTypePropertyTypes VALUES ('17138','dustResistent',"are glasses dustResistent (yes/no)");
INSERT INTO productTypePropertyTypes VALUES ('17138','scratchResistent',"are glasses scratchResistent (yes/no)");
INSERT INTO productTypePropertyTypes VALUES ('17138','rimLess',"are eyeglasses rimless (yes/no)");
INSERT INTO productTypePropertyTypes VALUES ('17138','rimmed',"are eyeglasses rimmed (yes/no)");
INSERT INTO productTypePropertyTypes VALUES ('17138','semi-rimmed',"are eyeglasses semi-rimmed (yes/no)");


INSERT INTO productTypePropertyTypeContainer VALUES ('17138','eyeglassFeatures','frameShape','1');
INSERT INTO productTypePropertyTypeContainer VALUES ('17138','eyeglassFeatures','protection','2');
INSERT INTO productTypePropertyTypeContainer VALUES ('17138','eyeglassFeatures','lensFeatures','3');

INSERT INTO productTypePropertyTypeContainer VALUES ('17138','frameShape','oval','1');
INSERT INTO productTypePropertyTypeContainer VALUES ('17138','frameShape','round','2');
INSERT INTO productTypePropertyTypeContainer VALUES ('17138','frameShape','aviator','3');
INSERT INTO productTypePropertyTypeContainer VALUES ('17138','protection','UV_Protection','1');
INSERT INTO productTypePropertyTypeContainer VALUES ('17138','protection','antiGlare','2');
INSERT INTO productTypePropertyTypeContainer VALUES ('17138','protection','polarized','3');
INSERT INTO productTypePropertyTypeContainer VALUES ('17138','protection','blueReflector','4');
INSERT INTO productTypePropertyTypeContainer VALUES ('17138','lensFeatures','waterResistent','1');
INSERT INTO productTypePropertyTypeContainer VALUES ('17138','lensFeatures','dustResistent','2');
INSERT INTO productTypePropertyTypeContainer VALUES ('17138','lensFeatures','scratchResistent','3');



/* you have not insert all productTypeId from the productTypes  and define container in productTypePropertyTypeValidationTypes */
INSERT INTO requiredProductTypeProperties VALUES ('17138','524','weight','77');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524','dim1','130');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524','dim2','50');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524','dim3','45');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524','color','Black ');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524','manufacturer','Peter Jones');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524','mfgDate','06/07/2016');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524','price','399');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524','instructions','eyeglasses instructions');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524','subInstruction1','wipe lenses with soft cotton cloth only');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524','subInstruction2','keep eyeglasses in protective cover');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524','url','https://www.amazon.in/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=eyeglasses');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524','UV_Protection','(yes/no)');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524','antiGlare','(yes/no)');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524','polarized','(yes/no)');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524','blueReflector','(yes/no)');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524','waterResistent','(yes/no)');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524','dustResistent','(yes/no)');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524','scratchResistent','(yes/no)');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524','frameMaterial','plastic');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524','frameShape','round');

INSERT INTO requiredProductTypeProperties VALUES ('17138','524-01','weight','180');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-01','dim1','140');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-01','dim2','52');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-01','dim3','43');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-01','color','Demi Black ');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-01','manufacturer','Titan');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-01','mfgDate','14/05/2017');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-01','price','3645');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-01','instructions','eyeglasses instructions');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-01','subInstruction1','wipe lenses with soft cotton cloth only');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-01','subInstruction2','keep eyeglasses in protective cover');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-01','url','https://eyeplus.titan.co.in/t2211c1a1-from-titan.html');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-01','UV_Protection','no');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-01','antiGlare','no');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-01','polarized','no');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-01','blueReflector','no');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-01','waterResistent','no');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-01','dustResistent','no');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-01','scratchResistent','no');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-01','frameMaterial','plastic');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-01','frameShape','round');

INSERT INTO requiredProductTypeProperties VALUES ('17138','524-02','weight','358');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-02','dim1','150');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-02','dim2','90');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-02','dim3','55');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-02','color','Gold');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-02','manufacturer','Ray-Ban');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-02','mfgDate','12/04/2014');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-02','price','5091');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-02','instructions','eyeglasses instructions');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-02','subInstruction1','wipe lenses with soft cotton cloth only');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-02','subInstruction2','keep eyeglasses in protective cover');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-02','url','https://www.amazon.in/Ray-Ban-Gradient-Sunglasses-millimeters-Multi-Coloured/dp/B00M3U924A/ref=sr_1_1?ie=UTF8&qid=1509272217&sr=8-1&keywords=rayban+sunglasses');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-02','UV_Protection','yes');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-02','antiGlare','yes');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-02','polarized','no');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-02','blueReflector','no');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-02','waterResistent','no');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-02','dustResistent','no');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-02','scratchResistent','no');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-02','frameMaterial','metal');
INSERT INTO requiredProductTypeProperties VALUES ('17138','524-02','frameShape','aviator');



INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-weight','Range','Float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-dim1','Range','Float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-dim2','Range','Float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-dim3','Range','Float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-manufacturer','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-mfgDate','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-price','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-instructions','container','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-subInstruction1','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-subInstruction2','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-url','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-eyeglassFeatures','container','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-frameShape','container','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-protection','container','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-lensFeatures','container','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-rimType','container','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-UV_Protection','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-antiGlare','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-polarized','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-blueReflector','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-waterResistent','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-dustResistent','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-scratchResistent','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-frameMaterial','List','String');


INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-weight','Range','Float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-dim1','Range','Float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-dim2','Range','Float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-dim3','Range','Float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-manufacturer','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-mfgDate','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-price','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-instructions','container','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-subInstruction1','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-subInstruction2','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-url','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-eyeglassFeatures','container','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-frameShape','container','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-protection','container','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-lensFeatures','container','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-rimType','container','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-UV_Protection','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-antiGlare','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-polarized','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-blueReflector','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-waterResistent','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-dustResistent','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-scratchResistent','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-01-frameMaterial','List','String');


INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-weight','Range','Float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-dim1','Range','Float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-dim2','Range','Float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-dim3','Range','Float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-manufacturer','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-mfgDate','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-price','range','float');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-instructions','container','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-subInstruction1','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-subInstruction2','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-url','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-eyeglassFeatures','container','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-frameShape','container','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-protection','container','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-lensFeatures','container','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-rimType','container','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-UV_Protection','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-antiGlare','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-polarized','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-blueReflector','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-waterResistent','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-dustResistent','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-scratchResistent','List','String');
INSERT INTO productTypePropertyTypeValidationTypes VALUES ('17138','17138-524-02-frameMaterial','List','String');



INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524','weight','gm','17138-524-weight');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524','dim1','mm','17138-524-dim1');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524','dim2','mm','17138-524-dim2');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524','dim3','mm','17138-524-dim3');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524','color',"",'17138-524-color');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524','manufacturer',"",'17138-524-manufacturer');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524','mfgDate',"",'17138-524-mfgDate');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524','price','rs','17138-524-price');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524','instructions',"",'17138-524-instructions');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524','subInstruction1',"",'17138-524-subInstruction1');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524','subInstruction2',"",'17138-524-subInstruction2');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524','url',"",'17138-524-url');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524','UV_Protection',"",'17138-524-UV_Protection');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524','antiGlare',"",'17138-524-antiGlare');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524','polarized',"",'17138-524-polarized');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524','blueReflector',"",'17138-524-blueReflector');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524','waterResistent',"",'17138-524-waterResistent');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524','dustResistent',"",'17138-524-dustResistent');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524','scratchResistent',"",'17138-524-scratchResistent');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524','frameMaterial',"",'17138-524-frameMaterial');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524','frameShape',"",'17138-524-frameShape');

INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-01','weight','gm','17138-524-01-weight');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-01','dim1','mm','17138-524-01-dim1');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-01','dim2','mm','17138-524-01-dim2');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-01','dim3','mm','17138-524-01-dim3');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-01','color',"",'17138-524-01-color');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-01','manufacturer',"",'17138-524-01-manufacturer');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-01','mfgDate',"",'17138-524-01-mfgDate');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-01','price','rs','17138-524-01-price');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-01','instructions',"",'17138-524-01-instructions');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-01','subInstruction1',"",'17138-524-01-subInstruction1');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-01','subInstruction2',"",'17138-524-01-subInstruction2');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-01','url',"",'17138-524-01-url');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-01','UV_Protection',"",'17138-524-01-UV_Protection');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-01','antiGlare',"",'17138-524-01-antiGlare');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-01','polarized',"",'17138-524-01-polarized');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-01','blueReflector',"",'17138-524-01-blueReflector');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-01','waterResistent',"",'17138-524-01-waterResistent');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-01','dustResistent',"",'17138-524-01-dustResistent');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-01','scratchResistent',"",'17138-524-01-scratchResistent');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-01','frameMaterial',"",'17138-524-01-frameMaterial');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-01','frameShape',"",'17138-524-01-frameShape');

INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-02','weight','gm','17138-524-02-weight');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-02','dim1','mm','17138-524-02-dim1');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-02','dim2','mm','17138-524-02-dim2');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-02','dim3','mm','17138-524-02-dim3');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-02','color',"",'17138-524-02-color');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-02','manufacturer',"",'17138-524-02-manufacturer');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-02','mfgDate',"",'17138-524-02-mfgDate');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-02','price','rs','17138-524-02-price');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-02','instructions',"",'17138-524-02-instructions');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-02','subInstruction1',"",'17138-524-02-subInstruction1');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-02','subInstruction2',"",'17138-524-02-subInstruction2');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-02','url',"",'17138-524-02-url');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-02','UV_Protection',"",'17138-524-02-UV_Protection');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-02','antiGlare',"",'17138-524-02-antiGlare');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-02','polarized',"",'17138-524-02-polarized');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-02','blueReflector',"",'17138-524-02-blueReflector');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-02','waterResistent',"",'17138-524-02-waterResistent');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-02','dustResistent',"",'17138-524-02-dustResistent');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-02','scratchResistent',"",'17138-524-02-scratchResistent');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-02','frameMaterial',"",'17138-524-02-frameMaterial');
INSERT INTO productTypePropertyTypeValueTypes VALUES ('17138','524-02','frameShape',"",'17138-524-02-frameShape');


INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-weight','1','110');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-weight','2','115');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-weight','3','105');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-dim1','1','130');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-dim1','2','128');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-dim2','1','50');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-dim3','1','45');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-dim3','2','43');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-color','1','Black');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-color','2','Matt Black');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-color','3','Red');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-color','4','Blue');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-manufacturer','1','Peter Jones');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-manufacturer','2','Titan');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-mfgDate','1','06/07/2016');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-price','1','399');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-instructions','1','eyeglasses instructions');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-subInstruction1','1','wipe lenses with soft cotton cloth only');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-subInstruction2','1','keep eyeglasses in protective cover');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-url','1','https://www.amazon.in/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=eyeglasses');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-UV_Protection','1','(yes/no)');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-antiGlare','1','(yes/no)');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-polarized','1','(yes/no)');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-blueReflector','1','(yes/no)');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-waterResistent','1','(yes/no)');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-dustResistent','1','(yes/no)');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-scratchResistent','1','(yes/no)');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-frameMaterial','1','plastic');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-frameShape','1','round');

INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-weight','1','180');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-weight','2','182');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-weight','3','189');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-dim1','1','140');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-dim2','1','52');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-dim2','2','59');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-dim3','1','43');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-color','1','Black');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-manufacturer','1','Titan');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-mfgDate','1','14/05/2017');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-price','1','3645');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-price','2','3764');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-instructions','1','eyeglasses instructions');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-subInstruction1','1','wipe lenses with soft cotton cloth only');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-subInstruction2','1','keep eyeglasses in protective cover');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-url','1','https://eyeplus.titan.co.in/t2211c1a1-from-titan.html');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-UV_Protection','1','no');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-antiGlare','1','no');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-polarized','1','no');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-blueReflector','1','no');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-waterResistent','1','no');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-dustResistent','1','no');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-scratchResistent','1','no');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-frameMaterial','1','plastic');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-01-frameShape','1','round');


INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-02-weight','1','358');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-02-dim1','1','150');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-02-dim2','1','90');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-02-dim3','1','55');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-02-color','1','Gold');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-02-manufacturer','1','Ray-Ban');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-02-mfgDate','1','12/04/2014');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-02-price','1','5091');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-02-instructions','1','eyeglasses instructions');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-02-subInstruction1','1','wipe lenses with soft cotton cloth only');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-02-subInstruction2','1','keep eyeglasses in protective cover');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-02-url','1','https://www.amazon.in/Ray-Ban-Gradient-Sunglasses-millimeters-Multi-Coloured/dp/B00M3U924A/ref=sr_1_1?ie=UTF8&qid=1509272217&sr=8-1&keywords=rayban+sunglasses');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-02-UV_Protection','1','yes');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-02-antiGlare','1','yes');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-02-polarized','1','no');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-02-blueReflector','1','no');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-02-waterResistent','1','no');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-02-dustResistent','1','no');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-02-scratchResistent','1','no');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-02-frameMaterial','1','metal');
INSERT INTO productTypePropertyTypeValidationValues VALUES ('17138','17138-524-02-frameShape','1','aviator');



















