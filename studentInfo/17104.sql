insert into productTypes values('17104','204','Pants','www.amazon.com');


insert into productTypes values('17104','204-01','Bell Bottom','http://www.shein.in/Raw-Hem-Bell-Bottoms-Jeans-p-363915-cat-1934.html?url_from=inadplapants170607201L&gclid=Cj0KCQjwg7HPBRDUARIsAMeR_0gfjk6cdMAcBEF-_tyVADIBg-tttCaSdKsdsN9_1FEdjWPNTgQHmaEaAhFBEALw_wcB');

insert into productTypes values('17104','204-02','Baggy Pants','http://www.shein.in/Tie-Waist-Wide-Leg-Pants-p-373234-cat-1740.html?url_from=inadplapants170717130o&gclid=Cj0KCQjwg7HPBRDUARIsAMeR_0j4-ZZB2MkRcI5pZV-dUo-sq3mckVukb0ZCUw2NBhH6QYnhngRD_5QaAqqpEALw_wcB');






insert into productTypeHierarchy values('17104','204','204-01');
insert into productTypeHierarchy values('17104','204','204-02');


insert into productTypePropertyTypes values('17104','dustproofPants','Is the pant dustproof or not?(yes/no)');
insert into productTypePropertyTypes values('17104','Flexibility','Is the Pant flexible or not?(yes/no)');
insert into productTypePropertyTypes values('17104','SkinProtection','');
insert into productTypePropertyTypes values('17104','Protection','');
insert into productTypePropertyTypes values('17104','Comfort','');
insert into productTypePropertyTypes values('17104','subinstructions01','');
insert into productTypePropertyTypes values('17104','subinstructions02','');
insert into productTypePropertyTypes values('17104','subinstructions03','');
insert into productTypePropertyTypes values('17104','subinstructions04','');
insert into productTypePropertyTypes values('17104','subinstructions05','');
insert into productTypePropertyTypes values('17104','subinstructions06','');
insert into productTypePropertyTypes values('17104','Features01','');
insert into productTypePropertyTypes values('17104','Features02','');
insert into productTypePropertyTypes values('17104','PantMaterial01','');
insert into productTypePropertyTypes values('17104','PantMaterial02','');
insert into productTypePropertyTypes values('17104','PantMaterial03','');
insert into productTypePropertyTypes values('17104','Design01','');
insert into productTypePropertyTypes values('17104','Design02','');

insert into productTypePropertyTypes values('17104','size01','');
insert into productTypePropertyTypes values('17104','size02','');








insert into productTypePropertyTypes values('17104','Brand01','');
insert into productTypePropertyTypes values('17104','Brand02','');

insert into productTypePropertyTypes values('17104','NumberOfPockets','');


insert into productTypePropertyTypes values('17104','MatchingBelts','');
insert into productTypePropertyTypes values('17104','MatchingShoes','');
insert into productTypePropertyTypes values('17104','Buttons','');
insert into productTypePropertyTypes values('17104','Stylish','');
insert into productTypePropertyTypes values('17104','QualityOfMaterial','');




insert into productTypePropertyTypeContainer values('17104','Features01','Flexibility','1');
insert into productTypePropertyTypeContainer values('17104','Features01','Stylish','2');
insert into productTypePropertyTypeContainer values('17104','Features01','Comfort','3');
insert into productTypePropertyTypeContainer values('17104','Features01','Buttons','4');
insert into productTypePropertyTypeContainer values('17104','Features01','MatchingShoes','5');

insert into productTypePropertyTypeContainer values('17104','Features02','Protection','1');
insert into productTypePropertyTypeContainer values('17104','Features02','Stylish','2');
insert into productTypePropertyTypeContainer values('17104','Features02','Buttons','3');
insert into productTypePropertyTypeContainer values('17104','Features02','MatchingShoes','4');

insert into productTypePropertyTypeContainer values('17104','Protection','SkinProtection','1');
insert into productTypePropertyTypeContainer values('17104','Protection','dustproofPants','2');
insert into productTypePropertyTypeContainer values('17104','Protection','Buttons','3');
insert into productTypePropertyTypeContainer values('17104','Protection','QualityOfMaterial','4');


insert into productTypePropertyTypeContainer values('17104','Flexibility','Comfort','1');
insert into productTypePropertyTypeContainer values('17104','Flexibility','Stylish','2');
insert into productTypePropertyTypeContainer values('17104','Flexibility','Buttons','3');
insert into productTypePropertyTypeContainer values('17104','Flexibility','QualityOfMaterial','4');
insert into productTypePropertyTypeContainer values('17104','Flexibility','SkinProtection','5');

insert into productTypePropertyTypeContainer values('17104','Buttons','Comfort','1');
insert into productTypePropertyTypeContainer values('17104','Buttons','Stylish','2');
insert into productTypePropertyTypeContainer values('17104','Buttons','QualityOfMaterial','3');
insert into productTypePropertyTypeContainer values('17104','Buttons','SkinProtection','4');

insert into productTypePropertyTypeContainer values('17104','QualityOfMaterial','Comfort','1');


insert into productTypePropertyTypeContainer values('17104','MatchingBelts','Stylish','1');
insert into productTypePropertyTypeContainer values('17104','MatchingShoes','Stylish','2');

insert into productTypePropertyTypeContainer values('17104','Stylish','QualityOfMaterial','1');

insert into productTypePropertyTypeContainer values('17104','Brand01','Comfort','1');


insert into productTypePropertyTypeContainer values('17104','Features01','weight','1');
insert into productTypePropertyTypeContainer values('17104','Features02','color','2');




insert into requiredProductTypeProperties values('17104','204','dustproofPants','Is the pant dustproof or not?(yes/no)');
insert into requiredProductTypeProperties values('17104','204','Flexibility','Is the Pant flexible or not?(yes/no)');
insert into requiredProductTypeProperties values('17104','204','SkinProtection','Protects skin from the sun burn');
insert into requiredProductTypeProperties values('17104','204','instructions','Pant washing Instructions');
insert into requiredProductTypeProperties values('17104','204','subinstructions01','Do not tumble dry');
insert into requiredProductTypeProperties values('17104','204','subinstructions02','Dont use bleaching powder');
insert into requiredProductTypeProperties values('17104','204','subinstructions03','Turn inside out before drying');
insert into requiredProductTypeProperties values('17104','204','subinstructions04','Dont dry it in direct sunlight');
insert into requiredProductTypeProperties values('17104','204','subinstructions05','Use warm iron');
insert into requiredProductTypeProperties values('17104','204','subinstructions06','Wash seperately');
insert into requiredProductTypeProperties values('17104','204','Features01','comfort');
insert into requiredProductTypeProperties values('17104','204','Features02','Number of Pockets');
insert into requiredProductTypeProperties values('17104','204','PantMaterial01','cotton');
insert into requiredProductTypeProperties values('17104','204','PantMaterial02','tericot');
insert into requiredProductTypeProperties values('17104','204','PantMaterial03','teriline');
insert into requiredProductTypeProperties values('17104','204','Design01','Gucci');
insert into requiredProductTypeProperties values('17104','204','Design02','Armani');
insert into requiredProductTypeProperties values('17104','204','price','1300.0');
insert into requiredProductTypeProperties values('17104','204','weight','250.0');
insert into requiredProductTypeProperties values('17104','204','size01','32');
insert into requiredProductTypeProperties values('17104','204','size02','34');
insert into requiredProductTypeProperties values('17104','204','dim01','28.0');
insert into requiredProductTypeProperties values('17104','204','dim02','135.0');

insert into requiredProductTypeProperties values('17104','204','color','Black');

insert into requiredProductTypeProperties values('17104','204','Brand01','Armani');
insert into requiredProductTypeProperties values('17104','204','Brand02','Gucci');
insert into requiredProductTypeProperties values('17104','204','MfgDate','1/06/2015');
insert into requiredProductTypeProperties values('17104','204','NumberOfPockets','4');
insert into requiredProductTypeProperties values('17104','204','url','https://www.amazon.com');

insert into requiredProductTypeProperties values('17104','204-01','dustproofPants','Is the pant dustproof or not?(yes/no)');
insert into requiredProductTypeProperties values('17104','204-01','Flexibility','Is the Pant flexible or not?(yes/no)');
insert into requiredProductTypeProperties values('17104','204-01','SkinProtection','Protects skin from the sun burn');
insert into requiredProductTypeProperties values('17104','204-01','Features01','comfort');
insert into requiredProductTypeProperties values('17104','204-01','Features02','Number of Pockets');
insert into requiredProductTypeProperties values('17104','204-01','PantMaterial01','cotton');
insert into requiredProductTypeProperties values('17104','204-01','PantMaterial02','tericot');
insert into requiredProductTypeProperties values('17104','204-01','PantMaterial03','teriline');
insert into requiredProductTypeProperties values('17104','204-01','Design01','Gucci');
insert into requiredProductTypeProperties values('17104','204-01','Design02','Armani');
insert into requiredProductTypeProperties values('17104','204-01','price','1300.0');
insert into requiredProductTypeProperties values('17104','204-01','weight','250.0');
insert into requiredProductTypeProperties values('17104','204-01','size01','32');
insert into requiredProductTypeProperties values('17104','204-01','size02','34');
insert into requiredProductTypeProperties values('17104','204-01','dim01','28.0');
insert into requiredProductTypeProperties values('17104','204-01','dim02','135.0');
insert into requiredProductTypeProperties values('17104','204-01','color','Black');
insert into requiredProductTypeProperties values('17104','204-01','Brand01','Armani');
insert into requiredProductTypeProperties values('17104','204-01','Brand02','Gucci');
insert into requiredProductTypeProperties values('17104','204-01','MfgDate','1/06/2015');
insert into requiredProductTypeProperties values('17104','204-01','NumberOfPockets','4');
insert into requiredProductTypeProperties values('17104','204-01','url','https://www.amazon.com');

insert into requiredProductTypeProperties values('17104','204-02','dustproofPants','Is the pant dustproof or not?(yes/no)');
insert into requiredProductTypeProperties values('17104','204-02','Flexibility','Is the Pant flexible or not?(yes/no)');
insert into requiredProductTypeProperties values('17104','204-02','SkinProtection','Protects skin from the sun burn');
insert into requiredProductTypeProperties values('17104','204-02','Features01','comfort');
insert into requiredProductTypeProperties values('17104','204-02','Features02','Number of Pockets');
insert into requiredProductTypeProperties values('17104','204-02','PantMaterial01','cotton');
insert into requiredProductTypeProperties values('17104','204-02','PantMaterial02','tericot');
insert into requiredProductTypeProperties values('17104','204-02','PantMaterial03','teriline');
insert into requiredProductTypeProperties values('17104','204-02','Design01','Gucci');
insert into requiredProductTypeProperties values('17104','204-02','Design02','Armani');
insert into requiredProductTypeProperties values('17104','204-02','price','1300.0');
insert into requiredProductTypeProperties values('17104','204-02','weight','250.0');
insert into requiredProductTypeProperties values('17104','204-02','size01','32');
insert into requiredProductTypeProperties values('17104','204-02','size02','34');
insert into requiredProductTypeProperties values('17104','204-02','dim01','28.0');
insert into requiredProductTypeProperties values('17104','204-02','dim02','135.0');
insert into requiredProductTypeProperties values('17104','204-02','color','Black');
insert into requiredProductTypeProperties values('17104','204-02','Brand01','Armani');
insert into requiredProductTypeProperties values('17104','204-02','Brand02','Gucci');
insert into requiredProductTypeProperties values('17104','204-02','MfgDate','1/06/2015');
insert into requiredProductTypeProperties values('17104','204-02','NumberOfPockets','4');
insert into requiredProductTypeProperties values('17104','204-02','url','https://www.amazon.com');


insert into productTypePropertyTypeValidationTypes values('17104','17104-204-dustproofPants','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-Flexibility','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-SkinProtection','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-instructions','container','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-subinstructions01','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-subinstructions02','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-subinstructions03','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-subinstructions04','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-subinstructions05','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-subinstructions06','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-Features01','container','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-Features02','container','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-PantMaterial01','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-PantMaterial02','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-PantMaterial03','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-Design01','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-Design02','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-price','range','float');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-weight','range','float');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-size01','range','int');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-size02','range','int');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-dim01','range','float');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-dim02','range','float');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-color','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-Brand01','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-Brand02','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-MfgDate','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-NumberOfPockets','range','int');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-url','list','string');



insert into productTypePropertyTypeValidationTypes values('17104','17104-204-01-dustproofPants','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-01-Flexibility','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-01-SkinProtection','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-01-Features01','container','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-01-Features02','container','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-01-PantMaterial01','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-01-PantMaterial02','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-01-PantMaterial03','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-01-Design01','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-01-Design02','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-01-price','range','float');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-01-weight','range','float');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-01-size01','range','int');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-01-size02','range','int');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-01-dim01','range','float');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-01-dim02','range','float');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-01-color','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-01-Brand01','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-01-Brand02','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-01-MfgDate','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-01-NumberOfPockets','range','int');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-01-url','list','string');








insert into productTypePropertyTypeValidationTypes values('17104','17104-204-02-dustproofPants','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-02-Flexibility','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-02-SkinProtection','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-02-Features01','container','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-02-Features02','container','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-02-PantMaterial01','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-02-PantMaterial02','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-02-PantMaterial03','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-02-Design01','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-02-Design02','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-02-price','range','float');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-02-weight','range','float');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-02-size01','range','int');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-02-size02','range','int');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-02-dim01','range','float');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-02-dim02','range','float');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-02-color','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-02-Brand01','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-02-Brand02','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-02-MfgDate','list','string');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-02-NumberOfPockets','range','int');
insert into productTypePropertyTypeValidationTypes values('17104','17104-204-02-url','list','string');



insert into productTypePropertyTypeValueTypes values('17104','204','dustproofPants','','17104-204-dustproofPants');
insert into productTypePropertyTypeValueTypes values('17104','204','Flexibility','','17104-204-Flexibility');
insert into productTypePropertyTypeValueTypes values('17104','204','SkinProtection','','17104-204-SkinProtection');
insert into productTypePropertyTypeValueTypes values('17104','204','instructions','','17104-204-instructions');
insert into productTypePropertyTypeValueTypes values('17104','204','subinstructions01','','17104-204-subinstructions01');
insert into productTypePropertyTypeValueTypes values('17104','204','subinstructions02','','17104-204-subinstructions02');
insert into productTypePropertyTypeValueTypes values('17104','204','subinstructions03','','17104-204-subinstructions03');
insert into productTypePropertyTypeValueTypes values('17104','204','subinstructions04','','17104-204-subinstructions04');
insert into productTypePropertyTypeValueTypes values('17104','204','subinstructions05','','17104-204-subinstructions05');
insert into productTypePropertyTypeValueTypes values('17104','204','subinstructions06','','17104-204-subinstructions06');
insert into productTypePropertyTypeValueTypes values('17104','204','Features01','','17104-204-Features01');
insert into productTypePropertyTypeValueTypes values('17104','204','Features02','','17104-204-Features02');
insert into productTypePropertyTypeValueTypes values('17104','204','PantMaterial01','','17104-204-PantMaterial01');
insert into productTypePropertyTypeValueTypes values('17104','204','PantMaterial02','','17104-204-PantMaterial02');
insert into productTypePropertyTypeValueTypes values('17104','204','PantMaterial03','','17104-204-PantMaterial03');
insert into productTypePropertyTypeValueTypes values('17104','204','Design01','','17104-204-Design01');
insert into productTypePropertyTypeValueTypes values('17104','204','Design02','','17104-204-Design02');
insert into productTypePropertyTypeValueTypes values('17104','204','price','','17104-204-price');
insert into productTypePropertyTypeValueTypes values('17104','204','weight','gm','17104-204-weight');
insert into productTypePropertyTypeValueTypes values('17104','204','size01','cm','17104-204-size01');
insert into productTypePropertyTypeValueTypes values('17104','204','size02','cm','17104-204-size02');
insert into productTypePropertyTypeValueTypes values('17104','204','dim01','cm','17104-204-Dim01');
insert into productTypePropertyTypeValueTypes values('17104','204','dim02','cm','17104-204-dim02');
insert into productTypePropertyTypeValueTypes values('17104','204','color','','17104-204-color01');
insert into productTypePropertyTypeValueTypes values('17104','204','Brand01','','17104-204-Brand01');
insert into productTypePropertyTypeValueTypes values('17104','204','Brand02','','17104-204-Brand02');
insert into productTypePropertyTypeValueTypes values('17104','204','MfgDate','','17104-204-MfgDate');
insert into productTypePropertyTypeValueTypes values('17104','204','NumberOfPockets','','17104-204-NumberOfPockets');
insert into productTypePropertyTypeValueTypes values('17104','204','url','','17104-204-url');



insert into productTypePropertyTypeValidationValues values('17104','17104-204-dustproofPants','1','fabric');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-Flexibility','2','flexible');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-SkinProtection','3','Protects skin from the sun burn');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-instructions','4','Pant washing Instructions');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-subinstructions01','5','Do not tumble dry');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-subinstructions02','5','Dont use bleaching powder');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-subinstructions03','5','Turn inside out before drying');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-subinstructions04','5','Dont dry it in direct sunlight');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-subinstructions05','5','Use warm iron');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-subinstructions06','5','Wash seperately');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-Features01','6','comfort');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-Features02','6','Number of Pockets');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-PantMaterial01','7','cotton');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-PantMaterial02','7','tericot');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-PantMaterial03','7','teriline');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-Design01','8','Armani');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-Design02','8','Gucci');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-price','9','1300.0');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-weight','10','250.0');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-size01','11','32');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-size02','12','34');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-dim01','13','28.0');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-dim02','13','135.0');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-color','14','Black');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-Brand01','15','Armani');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-Brand02','15','Gucci');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-MfgDate','16','1/06/2015');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-NumberOfPockets','17','4');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-url','18','https://www.amazon.com');


insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-dustproofPants','1','fabric');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-Flexibility','2','flexible');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-SkinProtection','3','Protects skin from the sun burn');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-instructions','4','Pant washing Instructions');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-subinstructions01','5','Do not tumble dry');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-subinstructions02','5','Dont use bleaching powder');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-subinstructions03','5','Turn inside out before drying');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-subinstructions04','5','Dont dry it in direct sunlight');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-subinstructions05','5','Use warm iron');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-subinstructions06','5','Wash seperately');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-Features01','6','comfort');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-Features02','6','Number of Pockets');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-PantMaterial01','7','cotton');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-PantMaterial02','7','tericot');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-PantMaterial03','7','teriline');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-Design01','8','Armani');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-Design02','8','Gucci');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-price','9','1300.0');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-weight','10','250.0');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-size01','11','32');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-size02','11','34');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-dim01','12','28.0');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-dim02','12','135.0');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-color','13','Black');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-Brand01','14','Armani');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-Brand02','14','Gucci');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-MfgDate','15','1/06/2015');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-NumberOfPockets','17','4');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-01-url','18','https://www.amazon.com');



insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-dustproofPants','1','fabric');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-Flexibility','2','flexible');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-SkinProtection','3','Protects skin from the sun burn');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-instructions','4','Pant washing Instructions');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-subinstructions01','5','Do not tumble dry');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-subinstructions02','5','Dont use bleaching powder');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-subinstructions03','5','Turn inside out before drying');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-subinstructions04','5','Dont dry it in direct sunlight');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-subinstructions05','5','Use warm iron');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-subinstructions06','5','Wash seperately');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-Features01','6','comfort');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-Features02','6','Number of Pockets');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-PantMaterial01','7','cotton');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-PantMaterial02','7','tericot');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-PantMaterial03','7','teriline');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-Design01','8','Armani');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-Design02','8','Gucci');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-price','9','1300.0');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-weight','10','250.0');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-size01','11','32');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-size02','11','34');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-dim01','12','28.0');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-dim02','12','135.0');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-color','13','Black');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-Brand01','14','Armani');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-Brand02','14','Gucci');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-MfgDate','16','1/06/2015');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-NumberOfPockets','17','4');
insert into productTypePropertyTypeValidationValues values('17104','17104-204-02-url','18','https://www.amazon.com');









