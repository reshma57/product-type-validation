.read productTypeSchema.sql
.read limitationRule.sql
.read trigger/rejectedStudents.sql
.read trigger/validateStudentProductTypes.sql
.read trigger/validateStudentProductTypePropertyTypes.sql
.read trigger/checkHierarchyAndElemInTree.sql
.read trigger/validationOfProductTypeAndProductValues.sql
.read trigger/checkRequiredValuesByproductTypePropertyTypeValidationValuesForRange.sql
.read trigger/validateRequiredValuesByproductTypePropertyTypeValidationValuesForRange.sql
.read trigger/checkRequiredValuesByproductTypePropertyTypeValidationValuesForList.sql
.read trigger/validateRequiredValuesByproductTypePropertyTypeValidationValuesForList.sql
.read trigger/checkRequiredValuesByproductTypePropertyTypeValidationValuesForContainer.sql
.read trigger/validateRequiredValuesByproductTypePropertyTypeValidationValuesForContainer.sql
.read trigger/validationOfProductTypePropertyTypeSequenceNumber.sql
.read trigger/deleteRejectedStudentData.sql
/*insert into studentRollNoRecords(rollNo) values("00002");*/
commit;
PRAGMA synchronous=OFF;PRAGMA count_changes=OFF;PRAGMA journal_mode=PERSIST;PRAGMA temp_store=PERSIST;PRAGMA recursive_triggers = ON;


INSERT INTO [productTypes] VALUES(17136,2814,'Hair Color','17136-2184-url-HairColor.tar.xz');
INSERT INTO [productTypes] VALUES(17136,'2814-01','Herbal Hair Color','17136-2184-01-url-HerbahlairColor.tar.xz');
INSERT INTO [productTypes] VALUES(17136,'2814-02','Chemical Hair Color','17136-2184-02-url-ChemicalBasedHairColor.tar.xz');


INSERT INTO [productTypeHierarchy] VALUES (17136,2814,'2814-01');
INSERT INTO [productTypeHierarchy] ([rollNo],[superProductTypeId],[subProductTypeId]) VALUES (17136,2814,'2814-02');


INSERT INTO [productTypePropertyTypes]  VALUES(17136,'subInstruction3-01',"");
INSERT INTO [productTypePropertyTypes]  VALUES(17136,'subInstruction3-02',"");
INSERT INTO [productTypePropertyTypes]  VALUES(17136,'subInstruction5','');
INSERT INTO [productTypePropertyTypes]  VALUES(17136,'subInstruction5','');
INSERT INTO [productTypePropertyTypes]  VALUES(17136,'subInstruction5','');
INSERT INTO [productTypePropertyTypes]  VALUES(17136,'subInstruction5','');

INSERT INTO [productTypePropertyTypes]  VALUES(17136,'url2','');

INSERT INTO [productTypePropertyTypes]  VALUES(17136,'featuresOfHairColor',"");
INSERT INTO [productTypePropertyTypes]  VALUES(17136,'hairColorPreventFrom',""); 
INSERT INTO [productTypePropertyTypes]  VALUES(17136,'qualityOfHairColor',"");
INSERT INTO [productTypePropertyTypes]  VALUES(17136,'isPreventFromhairFall','Yes/No');
INSERT INTO [productTypePropertyTypes]  VALUES(17136,'isPreventFromDandruff','Yes/No');
INSERT INTO [productTypePropertyTypes]  VALUES(17136,'isProvideUVProtection','Yes/No');
INSERT INTO [productTypePropertyTypes]  VALUES(17136,'longLastingnessPeriod','how long hair color lasts?');
INSERT INTO [productTypePropertyTypes]  VALUES(17136,'isPHbalanced','Yes/No');
INSERT INTO [productTypePropertyTypes]  VALUES(17136,'grayCoveragePercentage','How much percentage of grey it covers');
INSERT INTO [productTypePropertyTypes]  VALUES(17136,'presenceOfHarmfulChemicals',"");
INSERT INTO [productTypePropertyTypes]  VALUES(17136,'isAmmoniaPresent','Yes/No');
INSERT INTO [productTypePropertyTypes]  VALUES(17136,'isPPDPresent','Yes/No');
INSERT INTO [productTypePropertyTypes]  VALUES(17136,'isHeavyMetalPresent','Yes/No');
INSERT INTO [productTypePropertyTypes]  VALUES(17136,'isHydrogenPeroxide','Yes/No');
INSERT INTO [productTypePropertyTypes]  VALUES(17136,'applicableSkinTone','hair color applicable to all/medium to deep/sensitive type of skin tone');
INSERT INTO [productTypePropertyTypes]  VALUES(17136,'formOfhairColor','hair color available in 3 forms Gel/Powder/Liquid');
INSERT INTO [productTypePropertyTypes]  VALUES(17136,'availablecolorShadeOfhairColor','A large number of color shades available light/rich/intense/dark');
INSERT INTO [productTypePropertyTypes]  VALUES(17136,'IdealFor','Men/Women');


INSERT INTO [productTypePropertyTypeContainer] VALUES(17136,'featuresOfHairColor','hairColorPreventFrom',1);
INSERT INTO [productTypePropertyTypeContainer] VALUES(17136,'featuresOfHairColor','qualityOfHairColor',2);
INSERT INTO [productTypePropertyTypeContainer] VALUES(17136,'hairColorPreventFrom','isPreventFromhairFall',1);
INSERT INTO [productTypePropertyTypeContainer] VALUES(17136,'hairColorPreventFrom','isPreventFromDandruff',2);
INSERT INTO [productTypePropertyTypeContainer] VALUES(17136,'hairColorPreventFrom','isProvideUVProtection',3);
INSERT INTO [productTypePropertyTypeContainer] VALUES(17136,'qualityOfHairColor','longLastingnessPeriod',1);
INSERT INTO [productTypePropertyTypeContainer] VALUES(17136,'qualityOfHairColor','isPHbalanced',2);
INSERT INTO [productTypePropertyTypeContainer] VALUES(17136,'qualityOfHairColor','grayCoveragePercentage',3);
INSERT INTO [productTypePropertyTypeContainer] VALUES(17136,'qualityOfHairColor','presenceOfHarmfulChemicals',4);
INSERT INTO [productTypePropertyTypeContainer] VALUES(17136,'presenceOfHarmfulChemicals','isAmmoniaPresent',1);
INSERT INTO [productTypePropertyTypeContainer] VALUES(17136,'presenceOfHarmfulChemicals','isPPDPresent',2);
INSERT INTO [productTypePropertyTypeContainer] VALUES(17136,'presenceOfHarmfulChemicals','isHeavyMetalPresent',3);
INSERT INTO [productTypePropertyTypeContainer] VALUES(17136,'presenceOfHarmfulChemicals','isHydrogenPeroxide',4);
INSERT INTO [productTypePropertyTypeContainer] VALUES(17136,'instructions','subInstruction1',1);
INSERT INTO [productTypePropertyTypeContainer] VALUES(17136,'instructions','subInstruction2',2);
INSERT INTO [productTypePropertyTypeContainer] VALUES(17136,'instructions','subInstruction3',3);
INSERT INTO [productTypePropertyTypeContainer] VALUES(17136,'instructions','subInstruction4',4);


INSERT INTO [productTypePropertyTypeContainer] VALUES(17136,'instructions','subInstruction5',5);
INSERT INTO [productTypePropertyTypeContainer] VALUES(17136,'subInstruction3','subInstruction3-01',1);
INSERT INTO [productTypePropertyTypeContainer] VALUES(17136,'subInstruction3','subInstruction3-02',2);



INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'weight',150);
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'dimension1',10);
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'dimension2',10);
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'dimension3',10);
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'color','Burgundy');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'manufacturerName',"Goodrej Hair Color");
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'mfgDate','4 October 2013');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'price',175.0);
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'instructions','Hair Color Instructions');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'subInstruction1','Keep it away from children reach');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'subInstruction2','Never apply it on dirty or oilly hair');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'subInstruction3','Do the allergy test prior to use');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'subInstruction3-01','Do it before ');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'url','https://prezi.com/m/beqkwpntdsz5/properties-of-hair-dyes/');


INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'url2','');

INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'subInstruction4','Use only on recommendation of Doctor & Dermatologist');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'subInstruction5','This item is non-returnable');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'featuresOfHairColor','Shinny Hair');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'hairColorPreventFrom','provide Prevention from side effect');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'qualityOfHairColor',"");

INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'isPreventFromhairFall','Yes');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'isPreventFromDandruff','Yes');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'isProvideUVProtection','Yes');

INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'longLastingnessPeriod',1);

INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'isPHbalanced','Yes');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'grayCoveragePercentage',100.0);
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'presenceOfHarmfulChemicals','less harmfull chemicals present');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'isPPDPresent','No');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'isHeavyMetalPresent','No');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'isHydrogenPeroxide','No');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'isAmmoniaPresent','No');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'applicableSkinTone','To all');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'formOfhairColor','Gel');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,2814,'availablecolorShadeOfhairColor','dark');
INSERT INTO [requiredProductTypeProperties]  VALUES(17136,2814,'IdealFor','Women');

INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','weight',113);
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','dimension1',8.4);
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','dimension2',7.4);
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','dimension3',7.4);
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','color','Medium Brown');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','manufacturerName','Rainbow Research');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','mfgDate','28 December 2016');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','price',554.82);
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','featuresOfHairColor','Shinny Hair');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','hairColorPreventFrom','provide Prevention from side effect');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','qualityOfHairColor',"");
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','isPreventFromhairFall','Yes');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','isPreventFromDandruff','Yes');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','isProvideUVProtection','Yes');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','longLastingnessPeriod','4');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','isPHbalanced','Yes');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','grayCoveragePercentage',20);
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','presenceOfHarmfulChemicals','no harmfull chemicals present');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','isAmmoniaPresent','No');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','isPPDPresent','No');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','isHeavyMetalPresent','No');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','isHydrogenPeroxide','No');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','applicableSkinTone','To all');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','formOfhairColor','Powder');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','availablecolorShadeOfhairColor','Medium Brown (Chestnut)');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','IdealFor','Men & Women');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-01','url','https://in.iherb.com/pr/Rainbow-Research-Henna-Hair-Color-and-Conditioner-Medium-Brown-Chestnut-4-oz-113-g/11613');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','weight',240);
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','dimension1',15);
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','dimension2',10);
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','dimension3',20);
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','color','Darkest Brown ');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','manufacturerName','LOreal Paris ');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','mfgDate','1 January 2017');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','price',600.00);
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','featuresOfHairColor','Shinny Hair');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','hairColorPreventFrom','provide Prevention from side effect');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','qualityOfHairColor',"");
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','isPreventFromhairFall','Yes');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','isPreventFromDandruff','Yes');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','isProvideUVProtection','Yes');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','longLastingnessPeriod',28);
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','isPHbalanced','Yes');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','grayCoveragePercentage',100.0);
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','presenceOfHarmfulChemicals','less harmfull chemicals present');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','isAmmoniaPresent','No');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','isPPDPresent','No');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','isHeavyMetalPresent','No');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','isHydrogenPeroxide','No');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','applicableSkinTone','To all');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','formOfhairColor','Gel');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','availablecolorShadeOfhairColor','light');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','IdealFor','Women');

INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','url','url1-https://www.amazon.in/LOreal-Paris-Casting-Creme-Darkest/dp/B006QHB9GK/ref=lp_7477904031_1_1?s=beauty&ie=UTF8&qid=1509966242&sr=1-1');
INSERT INTO [requiredProductTypeProperties] VALUES(17136,'2814-02','url2','url2-https://www.flipkart.com/l-oreal-paris-casting-creme-gloss-hair-color/p/itme5ncf3enez9rh?pid=HRCDFRUJSNHZFZFA&srno=s_1_10&otracker=search&lid=LSTHRCDFRUJSNHZFZFAYMARVR&fm=SEARCH&iid=b5e61c48-f845-4f47-a212-ba124c108723.HRCDFRUJSNHZFZFA.SEARCH&qH=740b11b88269ce17');


INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-weight','range','float');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-dimension1','range','float');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-dimension2','range','float');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-dimension3','range','float');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-color','list','string');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-manufacturerName','list','string');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-mfgDate','list','string');
INSERT INTO [productTypePropertyTypeValidationTypes] VALUES(17136,'17136-2814-price','range','float');
INSERT INTO [productTypePropertyTypeValidationTypes] VALUES(17136,'17136-2184-instructions','container','string' );
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2184-subInstructions','list','string' );
INSERT INTO [productTypePropertyTypeValidationTypes] VALUES(17136,'17136-2814-urlValRule','list','string');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-featuresOfHairColor','container','string');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-hairColorPreventFrom','container','string');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-qualityOfHairColor','container','string');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-yesNoValRule','list','string' );
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-longLastingnessPeriod','range','int' );
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-grayCoveragePercentage','range','float' );
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-presenceOfHarmfulChemicals','container','string' );
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-availablecolorShadeOfhairColor','list','string' );
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-IdealFor','list','string' );
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-formOfhairColor','list','string' );
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-applicableSkinTone','list','string' );

INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-01-weight','range','float');
INSERT INTO [productTypePropertyTypeValidationTypes] VALUES(17136,'17136-2814-01-dimension1','range','float');
INSERT INTO [productTypePropertyTypeValidationTypes] VALUES(17136,'17136-2814-01-dimension2','range','float');
INSERT INTO [productTypePropertyTypeValidationTypes] VALUES(17136,'17136-2814-01-dimension3','range','float');
INSERT INTO [productTypePropertyTypeValidationTypes] VALUES(17136,'17136-2814-01-color','list','string');
INSERT INTO [productTypePropertyTypeValidationTypes] VALUES(17136,'17136-2814-01-manufacturerName','list','string');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-01-mfgDate','list','string');
INSERT INTO [productTypePropertyTypeValidationTypes] VALUES(17136,'17136-2814-01-price','range','float');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-01-url','list','string');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-01-url2','list','string');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-01-featuresOfHairColor','container','string');
INSERT INTO [productTypePropertyTypeValidationTypes] VALUES(17136,'17136-2814-01-hairColorPreventFrom','container','string');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-01-qualityOfHairColor','container','string');
INSERT INTO [productTypePropertyTypeValidationTypes] VALUES(17136,'17136-2814-01-longLastingnessPeriod','range','int' );
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-01-grayCoveragePercentage','range','float' );
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-01-presenceOfHarmfulChemicals','container','string' );
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-01-availablecolorShadeOfhairColor','list','string' );
INSERT INTO [productTypePropertyTypeValidationTypes] VALUES(17136,'17136-2814-01-IdealFor','list','string' );
INSERT INTO [productTypePropertyTypeValidationTypes] VALUES(17136,'17136-2814-01-formOfhairColor','list','string' );
INSERT INTO [productTypePropertyTypeValidationTypes] VALUES(17136,'17136-2814-01-applicableSkinTone','list','string' );

INSERT INTO [productTypePropertyTypeValidationTypes] VALUES(17136,'17136-2814-02-weight','range','float');
INSERT INTO [productTypePropertyTypeValidationTypes] VALUES(17136,'17136-2814-02-dimension1','range','float');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-02-dimension2','range','float');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-02-dimension3','range','float');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-02-color','list','string');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-02-manufacturerName','list','string');
INSERT INTO [productTypePropertyTypeValidationTypes] VALUES(17136,'17136-2814-02-mfgDate','list','string');
INSERT INTO [productTypePropertyTypeValidationTypes] VALUES(17136,'17136-2814-02-price','range','float');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-02-url','list','string');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-02-url2','list','string');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-02-featuresOfHairColor','container','string');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-02-hairColorPreventFrom','container','string');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-02-qualityOfHairColor','container','string');
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-02-longLastingnessPeriod','range','int' );
INSERT INTO [productTypePropertyTypeValidationTypes] VALUES(17136,'17136-2814-02-grayCoveragePercentage','range','float' );
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-02-presenceOfHarmfulChemicals','container','string' );
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-02-availablecolorShadeOfhairColor','list','string' );
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-02-IdealFor','list','string' );
INSERT INTO [productTypePropertyTypeValidationTypes]  VALUES(17136,'17136-2814-02-formOfhairColor','list','string' );
INSERT INTO [productTypePropertyTypeValidationTypes] VALUES(17136,'17136-2814-02-applicableSkinTone','list','string' );

INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'weight','gm','17136-2814-weight');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'dimension1','cm','17136-2814-dimension1');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'dimension2','cm','17136-2814-dimension2');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'dimension3','cm','17136-2814-dimension3');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'color','','17136-2814-color');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'manufacturerName','','17136-2814-manufacturerName');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'mfgDate','','17136-2814-mfgDate');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'price','Rs','17136-2814-price');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'url','','17136-2814-urlValRule');

INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'url2','','17136-2814-urlValRule');

INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'instructions','','17136-2184-instructions');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'subInstruction1','','17136-2184-subInstructions');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'subInstruction2','','17136-2184-subInstructions');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'subInstruction3','','17136-2184-subInstructions');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'subInstruction4','','17136-2184-subInstructions');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'subInstruction5','','17136-2184-subInstructions');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'featuresOfHairColor','','17136-2814-featuresOfHairColor');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'hairColorPreventFrom','','17136-2814-hairColorPreventFrom');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'qualityOfHairColor','','17136-2814-qualityOfHairColor');

INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'isPreventFromhairFall','','17136-2814-yesNoValRule');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'isPreventFromDandruff','','17136-2814-yesNoValRule');
INSERT INTO [productTypePropertyTypeValueTypes]  VALUES(17136,2814,'isProvideUVProtection','','17136-2814-yesNoValRule');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'isAmmoniaPresent','','17136-2814-yesNoValRule');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'isPPDPresent','','17136-2814-yesNoValRule');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'isHeavyMetalPresent','','17136-2814-yesNoValRule');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'isPHbalanced','','17136-2814-yesNoValRule');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'isHydrogenPeroxide','','17136-2814-yesNoValRule');

INSERT INTO [productTypePropertyTypeValueTypes]  VALUES(17136,2814,'longLastingnessPeriod','Weeks','17136-2814-longLastingnessPeriod');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'grayCoveragePercentage','%','17136-2814-grayCoveragePercentage');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'presenceOfHarmfulChemicals','','17136-2814-presenceOfHarmfulChemicals');

INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'applicableSkinTone','','17136-2814-applicableSkinTone');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'formOfhairColor','','17136-2814-formOfhairColor');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'availablecolorShadeOfhairColor','','17136-2814-availablecolorShadeOfhairColor');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,2814,'IdealFor','','17136-2814-IdealFor');

INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-01','weight','gm','17136-2814-01-weight');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-01','dimension1','cm','17136-2814-01-dimension1');
INSERT INTO [productTypePropertyTypeValueTypes]  VALUES(17136,'2814-01','dimension2','cm','17136-2814-01-dimension2');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-01','dimension3','cm','17136-2814-01-dimension3');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-01','color','','17136-2814-01-color');
INSERT INTO [productTypePropertyTypeValueTypes]  VALUES(17136,'2814-01','manufacturerName','','17136-2814-01-manufacturerName');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-01','mfgDate','','17136-2814-01-mfgDate');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-01','price','Rs','17136-2814-01-price');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-01','url','','17136-2814-01-url');
INSERT INTO [productTypePropertyTypeValueTypes]  VALUES(17136,'2814-01','featuresOfHairColor','','17136-2814-01-featuresOfHairColor');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-01','hairColorPreventFrom','','17136-2814-01-hairColorPreventFrom');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-01','qualityOfHairColor','','17136-2814-01-qualityOfHairColor');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-01','longLastingnessPeriod','Weeks','17136-2814-01-longLastingnessPeriod');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-01','grayCoveragePercentage','%','17136-2814-01-grayCoveragePercentage');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-01','presenceOfHarmfulChemicals','','17136-2814-01-presenceOfHarmfulChemicals');

INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-01','isPreventFromhairFall','','17136-2814-yesNoValRule');
INSERT INTO [productTypePropertyTypeValueTypes]  VALUES(17136,'2814-01','isPreventFromDandruff','','17136-2814-yesNoValRule');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-01','isProvideUVProtection','','17136-2814-yesNoValRule');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-01','isPHbalanced','','17136-2814-yesNoValRule');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-01','isAmmoniaPresent','','17136-2814-yesNoValRule');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-01','isPPDPresent','','17136-2814-yesNoValRule');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-01','isHeavyMetalPresent','','17136-2814-yesNoValRule');
INSERT INTO [productTypePropertyTypeValueTypes]  VALUES(17136,'2814-01','isHydrogenPeroxide','','17136-2814-yesNoValRule');

INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-01','formOfhairColor','','17136-2814-01-formOfhairColor');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-01','applicableSkinTone','','17136-2814-01-applicableSkinTone');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-01','availablecolorShadeOfhairColor','','17136-2814-01-availablecolorShadeOfhairColor');
INSERT INTO [productTypePropertyTypeValueTypes]  VALUES(17136,'2814-01','IdealFor','','17136-2814-01-IdealFor');

INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-02','weight','gm','17136-2814-02-weight');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-02','dimension1','cm','17136-2814-02-dimension1');
INSERT INTO [productTypePropertyTypeValueTypes]  VALUES(17136,'2814-02','dimension2','cm','17136-2814-02-dimension2');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-02','dimension3','cm','17136-2814-02-dimension3');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-02','color','','17136-2814-02-color');
INSERT INTO [productTypePropertyTypeValueTypes]  VALUES(17136,'2814-02','manufacturerName','','17136-2814-02-manufacturerName');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-02','mfgDate','','17136-2814-02-mfgDate');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-02','price','Rs','17136-2814-02-price');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-02','url','','17136-2814-02-url');
INSERT INTO [productTypePropertyTypeValueTypes]  VALUES(17136,'2814-02','featuresOfHairColor','','17136-2814-02-featuresOfHairColor');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-02','hairColorPreventFrom','','17136-2814-02-hairColorPreventFrom');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-02','qualityOfHairColor','','17136-2814-02-qualityOfHairColor');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-02','longLastingnessPeriod','Weeks','17136-2814-02-longLastingnessPeriod');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-02','grayCoveragePercentage','%','17136-2814-02-grayCoveragePercentage');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-02','presenceOfHarmfulChemicals','','17136-2814-02-presenceOfHarmfulChemicals');

INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-02','isPreventFromhairFall','','17136-2814-yesNoValRule');
INSERT INTO [productTypePropertyTypeValueTypes]  VALUES(17136,'2814-02','isPreventFromDandruff','','17136-2814-yesNoValRule');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-02','isProvideUVProtection','','17136-2814-yesNoValRule');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-02','isPHbalanced','','17136-2814-yesNoValRule');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-02','isAmmoniaPresent','','17136-2814-yesNoValRule');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-02','isPPDPresent','','17136-2814-yesNoValRule');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-02','isHeavyMetalPresent','','17136-2814-yesNoValRule');
INSERT INTO [productTypePropertyTypeValueTypes]  VALUES(17136,'2814-02','isHydrogenPeroxide','','17136-2814-yesNoValRule');

INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-02','formOfhairColor','','17136-2814-02-formOfhairColor');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-02','applicableSkinTone','','17136-2814-02-applicableSkinTone');
INSERT INTO [productTypePropertyTypeValueTypes] VALUES(17136,'2814-02','availablecolorShadeOfhairColor','','17136-2814-02-availablecolorShadeOfhairColor');
INSERT INTO [productTypePropertyTypeValueTypes]  VALUES(17136,'2814-02','IdealFor','','17136-2814-02-IdealFor');

INSERT INTO [productTypePropertyTypeValidationValues] VALUES(17136,'17136-2814-weight',1,0.00);
INSERT INTO [productTypePropertyTypeValidationValues] VALUES(17136,'17136-2814-weight',2,1000.00);
INSERT INTO [productTypePropertyTypeValidationValues] VALUES(17136,'17136-2814-dimension1',1,0.1);
INSERT INTO [productTypePropertyTypeValidationValues] VALUES(17136,'17136-2814-dimension1',2,20.0);
INSERT INTO [productTypePropertyTypeValidationValues] VALUES(17136,'17136-2814-dimension2',1,0.1);
INSERT INTO [productTypePropertyTypeValidationValues] VALUES(17136,'17136-2814-dimension2',2,20);
INSERT INTO [productTypePropertyTypeValidationValues] VALUES(17136,'17136-2814-dimension3',1,0.1);
INSERT INTO [productTypePropertyTypeValidationValues] VALUES(17136,'17136-2814-dimension3',2,25);
INSERT INTO [productTypePropertyTypeValidationValues] VALUES(17136,'17136-2814-color',1,'Red');
INSERT INTO [productTypePropertyTypeValidationValues] VALUES(17136,'17136-2814-color',2,'Burgundy');
INSERT INTO [productTypePropertyTypeValidationValues] VALUES(17136,'17136-2814-manufacturerName',1,'L,Oreal');
INSERT INTO [productTypePropertyTypeValidationValues] VALUES(17136,'17136-2814-manufacturerName',2,'Goodrej Hair Color');

INSERT INTO [productTypePropertyTypeValidationValues] VALUES(17136,'17136-2814-mfgDate',1,'4 October 2013');
INSERT INTO [productTypePropertyTypeValidationValues] VALUES(17136,'17136-2814-price',1,50.0);
INSERT INTO [productTypePropertyTypeValidationValues] VALUES(17136,'17136-2814-price',2,5000.0);
INSERT INTO [productTypePropertyTypeValidationValues] VALUES(17136,'17136-2184-instructions',1,'Hair Color Instructions');
INSERT INTO [productTypePropertyTypeValidationValues] VALUES(17136,'17136-2184-subInstructions',1,'Keep it away from children reach');
INSERT INTO [productTypePropertyTypeValidationValues] VALUES(17136,'17136-2184-subInstructions',2,'Never apply it on dirty or oilly hair');
INSERT INTO [productTypePropertyTypeValidationValues] VALUES(17136,'17136-2184-subInstructions',3,'Do the allergy test prior');
INSERT INTO [productTypePropertyTypeValidationValues] VALUES(17136,'17136-2184-subInstructions',4,'Use only on recommendation of Doctor & Dermatologist');
INSERT INTO [productTypePropertyTypeValidationValues] VALUES(17136,'17136-2184-subInstructions',5,'This item is non-returnable');
INSERT INTO [productTypePropertyTypeValidationValues] VALUES(17136,'17136-2814-01-url',1,'https://prezi.com/m/beqkwpntdsz5/properties-of-hair-dyes/');
INSERT INTO [productTypePropertyTypeValidationValues]  VALUES(17136,'17136-2814-01-featuresOfHairColor',1,'Shinny Hair');


insert into studentRollNoRecords(rollNo) values(17136);
