
DROP TABLE IF EXISTS leafCountForStudentAssignedProductType;
DROP TABLE IF EXISTS propertyCount;
DROP TABLE IF EXISTS countOfPropertyContainer;
DROP TABLE IF EXISTS countOfContainer;
DROP TABLE IF EXISTS depthOfTree;
DROP TABLE IF EXISTS studentLimitationRecords;
DROP TABLE IF EXISTS tableIdDomain;

CREATE TABLE studentLimitationRecords (
  rollNo varchar(5) NOT NULL,
  applicationLimit integer NOT NULL,
  Constraint pk_studentLimitationRecords primary key(rollNo,applicationLimit),
  Foreign Key(rollNo) References students(rollNo)
  ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE leafCountForStudentAssignedProductType (
  leafCount integer NOT NULL,
  Constraint pk_leafCountForStudentAssignedProductType primary key(leafCount)
);

CREATE TABLE countOfContainer (
  propertyTypeCnt integer NOT NULL,
  Constraint pk_countOfContainer primary key(propertyTypeCnt)
);

CREATE TABLE countOfPropertyContainer (
  propertyContainerLimit integer NOT NULL,
  Constraint pk_countOfPropertyContainer primary key(propertyContainerLimit)
);

CREATE TABLE propertyCount (
  propertyCnt integer NOT NULL,
  Constraint pk_propertyCount primary key(propertyCnt)
);

CREATE TABLE depthOfTree (
  height integer NOT NULL,
  Constraint pk_depthOfTree primary key(height)
);

CREATE TABLE tableIdDomain (
  tableId integer NOT NULL,
  tableName text NOT NULL,
  Constraint pk_tableIdDomain primary key(tableId)
);


.import /home/reshma/InternShip/product-type-db/countOfPropertyContainer.csv countOfPropertyContainer
.import /home/reshma/InternShip/product-type-db/countOfContainer.csv countOfContainer
.import /home/reshma/InternShip/product-type-db/propertyCount.csv propertyCount
.import /home/reshma/InternShip/product-type-db/leafCountForStudentAssignedProductType.csv leafCountForStudentAssignedProductType
.import /home/reshma/InternShip/product-type-db/depthOfTree.csv depthOfTree
.import /home/reshma/InternShip/product-type-db/tableIdDomain.csv tableIdDomain
